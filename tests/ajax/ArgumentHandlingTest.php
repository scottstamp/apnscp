<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, January 2019
 */


	require_once dirname(__DIR__) . '/TestFramework.php';

	class ArgumentHandlingTest extends TestFramework
	{
		public function testPollyfill()
		{
			$auth1 = \TestHelpers::create(array_get(Definitions::get(), 'auth.site.domain'));

			$_GET = [
				'engine' => 'cmd',
				'fn' => 'pman_run',
				session_name() => $auth1->id
			];
			$_POST = [
				'args' => [
					'env',
					2 => [
						'TEST_STRING' => $auth1->username
					]
				]
			];
			if (!defined('AJAX')) {
				define('AJAX', true);
			}
			ob_start();
			include_once(public_path('ajax.php'));
			$data = ob_get_contents();
			ob_end_clean();
			$data = json_decode($data, true);
			$this->assertNotNull($data, 'Reading ajax data');
			$this->assertContains('TEST_STRING', $data['return'], 'TEST_STRING environment variable set');
		}

		public function testPolyfillNamed() {
			$auth1 = \TestHelpers::create(array_get(Definitions::get(), 'auth.site.domain'));

			$_GET = [
				'engine'       => 'cmd',
				'fn'           => 'pman_run',
				session_name() => $auth1->id
			];
			$_POST = [
				'args' => [
					'cmd' => 'env',
					'garbage' => null,
					'env' => [
						'TEST_STRING' => $auth1->username
					]
				]
			];
			if (!defined('AJAX')) {
				define('AJAX', true);
			}

			ob_start();
			include(public_path('ajax.php'));
			$data = ob_get_contents();
			ob_end_clean();

			$data = json_decode($data, true);
			$this->assertNotNull($data, 'Reading ajax data');
			$this->assertContains('TEST_STRING', $data['return'], 'TEST_STRING environment variable set');
		}
	}
