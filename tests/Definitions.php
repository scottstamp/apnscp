<?php declare(strict_types=1);

class Definitions implements ArrayAccess {
	// @var array
	private $defs;
	private static $instance;

	private function __construct()
	{

		$yaml = [];
		foreach (['definitions.yml.dist', 'definitions.yml'] as $f) {
			if (!file_exists($f)) {
				continue;
			}
			$yaml = array_replace_recursive($yaml, (array)\Symfony\Component\Yaml\Yaml::parseFile($f));
		}

		define('AUTH_ROLES', $yaml['auth']);
		define('TEST_VARS', array_except($yaml, 'auth'));

		$this->defs = $yaml;
	}

	public static function get(): self
	{
		if (null === self::$instance) {
			self::$instance = new self;
		}
		return self::$instance;
	}

	public function offsetExists($offset)
	{
		return isset($this->defs[$offset]);
	}

	public function offsetGet($offset)
	{
		return $this->defs[$offset];
	}

	public function offsetSet($offset, $value)
	{
		$this->defs[$offset] = $value;
	}

	public function offsetUnset($offset)
	{
		unset($this->defs[$offset]);
	}


}
