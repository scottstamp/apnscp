<?php
    require_once dirname(__DIR__, 1) . '/TestFramework.php';

    class JobDispatcherTest extends TestFramework
    {
        protected $filename;

        public function testUnqueuedDispatch()
        {
        	if (!\Lararia\JobDaemon::get()->running()) {
        		$this->markTestSkipped('Job daemon not running');
			}
            $job = Lararia\Jobs\Job::create(new class extends Lararia\Jobs\Job
            {
				public $tries = 1;
				protected $filename;

                public function __construct()
                {
                    $this->filename = tempnam(TEMP_DIR, 'job-runner');
                    unlink($this->filename);
                }

                public function handle()
                {
                    return touch($this->filename);
                }

                public function fire()
                {
                    // unqueued job
                }

                public function getFilename()
                {
                    return $this->filename;
                }
            });

            $file = $job->getFilename();
            $job->dispatch();

            for ($i = 0; $i < 120; $i++) {
                if (file_exists($file)) {
                    break;
                }
                sleep(1);
            }
            $this->assertFileExists($file);
            unlink($file);

            return true;
        }

		/**
		 * Process job synchronously, trigger exception
		 *
		 * @return bool
		 */
        public function testExceptionTrace() {
			if (!is_debug()) {
				return $this->markTestSkipped('debug mode not enabled');
			} else if (!\Lararia\JobDaemon::get()->running()) {
				$this->markTestSkipped('Job daemon not running');
			}
			$job = Lararia\Jobs\Job::create(new class(\Auth::profile()) extends Lararia\Jobs\Job
			{
				use ContextableTrait;
				use apnscpFunctionInterceptorTrait;

				public $tries = 1;

				public function __construct($context)
				{
					$this->setContext($context);
				}

				public function fire()
				{
					return $this->test_message_class('error');
				}
			});
			ob_start();
			try {
				$job->dispatch();
			} catch (\Lararia\Jobs\JobException $exception) {
				$this->assertEquals(\Error_Reporter::E_ERROR, $exception->getCode());
				$this->assertContains("Test_Module", $exception->getApiTrace());
				return true;
			} finally {
				ob_end_clean();
			}
			$this->fail('Exception not trigger');
			return false;
		}

        public function testQueuedDispatch()
        {
        	if (!\Lararia\JobDaemon::get()->running()) {
				$this->markTestSkipped('Job runner is not active');
			}
            $msg = 'Testing exception';
            $job = Lararia\Jobs\Job::create(\tests\job\QueuedStubJob::class, $msg);
            $file = $job->getFilename();
            $job->dispatch();
            $job = null;

            for ($i = 0; $i < 120; $i++) {
                if (file_exists($file)) {
                    $this->assertContains($msg, file_get_contents($file));
                    break;
                }
                sleep(1);
            }
            if ($this->assertFileExists($file)) {
                unlink($file);
            }
        }

        public function testContext()
        {
			if (!\Lararia\JobDaemon::get()->running()) {
				$this->markTestSkipped('Job daemon not running');
			}
            $context = \TestHelpers::create(array_get(Definitions::get(), 'auth.site.domain'));
            $this->assertInstanceOf(Auth_Info_User::class, $context);
            $job = Lararia\Jobs\Job::create(new class ($context) extends \Lararia\Jobs\Job {
                use \Lararia\Jobs\Traits\RunAs;
                use apnscpFunctionInterceptorTrait;
				public $tries = 1;

                public function __construct(\Auth_Info_User $user) {
                    $this->setContext($user);
                }

                public function handle()
                {
                    return $this->common_get_admin_username();
                }

                public function fire()
                {}
            });
            $this->assertEquals($job->handle(), $context->username);
            // sending dispatch() precludes from testing directly
            // assume that queueing works fine from {@see testQueuedDispatch()}
        }
}

