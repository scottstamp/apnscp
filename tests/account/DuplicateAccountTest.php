<?php
	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	/**
	 * Class Duplicate
	 *
	 * Test duplicate account handling
	 *
	 */
	class DuplicateAccountTest extends TestFramework
	{
		const EXCEPTION_LEVEL = \Error_Reporter::E_ERROR;
		/**
		 * @covers \Opcenter\Account\Create
		 */
		public function testDuplicate() {
			$acct1 = \Opcenter\Account\Ephemeral::create();
			try {
				\Opcenter\Account\Ephemeral::create([
					'siteinfo' => [
						'domain' => $acct1->getContext()->domain
					]
				]);
			} catch (\apnscpException $e) {
				$this->assertEquals($e->getCode(), \Error_Reporter::E_ERROR);
			} finally {
			}
		}
	}

