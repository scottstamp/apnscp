@extends('apnscp-template')
@section('body')
    @if ($config['domain'] ?? true)
        <h3>Domain: {{ $domain }}</h3>
    @endif
    @yield('notice')
    @if (!empty($config['geoip']))
        <br />
        @include("auth.partials.user-meta")
    @endif
@endsection