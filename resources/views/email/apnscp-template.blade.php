@extends("common-layout")

@section('content')
    <table class="main" width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="content-wrap">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="content-block">
                            @yield('body')
                        </td>
                    </tr>

                </table>
            </td>
        </tr>
    </table>
@endsection

@section("footer")
    @include('common-footer')
@endsection