#!/bin/bash
#
# virtualhosting Brings up virtual IP addresses plus those configured
#                in /etc/sysconfig/virtualhosting
#
# chkconfig: 345 12 99
# description: Virtualhosting is a wrapper to the ip binary \
#              that brings up listed interfaces
# processname: virtualhosting

#SYSTEMCTL_SKIP_REDIRECT=1

# Source function library.
. /etc/rc.d/init.d/functions

# Source networking configuration.

RETVAL=0
prog="virtualhosting"
ETH="$(head -n1 /etc/virtualhosting/interface)"
IPMAPS="/etc/virtualhosting/mappings/ipmap /etc/virtualhosting/mappings/ipmap6"
NBMAPS="/etc/virtualhosting/namebased_ip_addrs /etc/virtualhosting/namebased_ip6_addrs /etc/sysconfig/virtualhosting"

start() {
	# Start daemons.
	for file in $NBMAPS ; do
		 [[ -f "$file" ]] || continue
		 # Necessary for proper read of record unterminated by newline
		 while read -r ADDR || [[ -n "$ADDR" ]] ; do
			echo -n $"Bringing up $ADDR: "
			bind_if "$ADDR" "$ETH"
			echo
		done <"$file"
	done

	for file in $IPMAPS; do
		[[ -f  "$file" ]] || continue
		awk -F= '{ if ($2 ~ "^site" && system("[[ -f /home/virtual/"$3"/info/disabled ]]")) { print $1 } }' "$file" |
			while read -r ADDR || [[ -n "$ADDR" ]] ; do
				echo -n $"Bringing up VH $ADDR: "
				bind_if "$ADDR" "$ETH"
				echo
			 done
	done
	return 0
}

stop() {
	# Stop daemons.
	echo $"Shutting down $prog: "
	for file in $IPMAPS ; do
		[[ -f "$file" ]] || continue
		awk -F= '{ if ($2 ~ "^site") { print $1 } }' "$file" |
			while read -r ADDR || [[ -n "$ADDR" ]]; do
				echo $"Shutting down VH $ADDR: "
				unbind_if "$ADDR" "$ETH"
			done
		done
	echo
	return 0
}

bind_if() {
	local ADDR=$1
	local ETH=$2
	[[ -n $(/sbin/ip addr show to "$ADDR") ]] && return 1
	/sbin/ip addr add "$ADDR" dev "$ETH"
}

unbind_if() {
	local ADDR=$1
	local ETH=$2
	local FULL="$(/sbin/ip -o addr show to "$ADDR" | awk '{print $4}')"
	[[ -z "$FULL" ]] && return 1
	/sbin/ip addr del "$FULL" dev "$ETH"
}

# See how we were called.
case "$1" in
	start)
		start
		;;
	stop)
		#stop
		;;
	restart|reload)
		stop
		start
		RETVAL=$?
		;;
	condrestart)
		if [ -f /var/lock/subsys/$prog ]; then
			stop
			start
			RETVAL=$?
		fi
		;;
	status)
		status $prog
		RETVAL=$?
		;;
	*)
		echo $"Usage: $0 {start|stop|restart|condrestart|status}"
		exit 1
esac

exit $RETVAL
