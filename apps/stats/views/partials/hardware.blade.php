<div class="card">
	<div class="card-header" role="tab" id="headingHardware">
		<h5 class="mb-0">
			<a class="btn-block" data-toggle="collapse" data-parent="#accordion" href="#hardware"
			   aria-expanded="true" aria-controls="hardware">
				Hardware
			</a>
		</h5>
	</div>

	<div id="hardware" class="collapse" role="tabpanel" aria-labelledby="headingHardware">
		<div class="card-block">
			<table width="100%">
				<thead>
				<tr>
					<th class="">
						# Processors
					</th>
					<th class="">
						Model Name
					</th>
					<th class="">
						CPU Speed
					</th>
					<th class="">
						Cache Size
					</th>
					<th class="">
						Bogomips
					</th>
				</tr>
				</thead>
				<tr>
					<td>
						<?=$processor['count']?>
					</td>
					<td>
						<?=$processor['model']?>
					</td>
					<td>
						<?=\Formatter::commafy($processor['speed'])?> MHz
					</td>
					<td>
						<?=\Formatter::commafy($processor['cache'])?> KB
					</td>
					<td>
						<?=\Formatter::commafy($processor['bogomips'])?>
					</td>
				</tr>
				<tr>
					<td class="" colspan="5">
						<h4>PCI Devices</h4>
					</td>
				</tr>
				<tr>
					<td colspan="5">
						<div style="overflow-x:scroll;">
							<pre><?=ltrim($Page->getPCIDevices());?></pre>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>