<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\webmail;

	use Opcenter\Crypto\Letsencrypt;
	use Opcenter\Crypto\NaiveCrypt;
	use Page_Container;

	class Page extends Page_Container
	{
		const DEFAULT_WEBMAIL = [
			'horde'     => '/horde',
			'sqmail'    => '/webmail',
			'roundcube' => '/roundcube'
		];
		private $url;
		private $_links;

		public function __construct()
		{
			parent::__construct();
			$this->add_css('webmail.css');
			$this->add_javascript('webmail.js');
			$this->init_js('lightbox');
			$this->_links = $this->email_webmail_apps();
		}

		public function getWebmailAlias($app, $includeproto = true)
		{
			if (!isset($this->_links[$app])) {
				return null;
			}
			$url = $this->_links[$app];

			$proto = 'http';
			if (isset($_GET['ssl']) && $_GET['ssl']) {
				$proto = 'https';
			}

			return ($includeproto ? $proto . '://' : '') . $url . '.' . \Util_Conf::login_domain();
		}

		public function on_postback($params)
		{
			if (!isset($params['app'])) {
				return;
			}
			if (!$this->hasSSO()) {
				$this->url = $this->getWebmailLink($params['app']);
			} else if ($params['app'] == 'sqmail') {
				$this->_accessSqMail();
			} else if ($params['app'] == 'horde') {
				$this->_accessHorde();
			} else if ($params['app'] == 'roundcube') {
				$this->_accessRoundCube();
			} else {
				return error("unknown webmail application");
			}

			if (!$this->url) {
				if (is_debug()) {
					return error("failed to retrieve cookies for `%s'", $params['app']);
				}
				// can't retrieve info, manual login
				$this->url = $this->getWebmailLink($params['app'], true);
			}
			\Util_HTTP::forwardNoProxy();
			header('Location: ' . $this->url, 302);
			exit;
		}

		public function hasSSO(): bool
		{
			return (bool)\Session::get('password');
		}

		private function _accessSqMail()
		{
			$data = self::_request_page(
				$this->getWebmailLink('sqmail') . '/src/redirect.php',
				[
					'login_username' => $_SESSION['username'] . '@' . $_SESSION['domain'],
					'secretkey'      => $this->getPassword()
				],
				'',
				$this->getWebmailLink('sqmail'),
				$_SERVER['HTTP_USER_AGENT'],
				[
				]
			);

			$cookie = $key = null;
			foreach (explode("\n", $data) as $line) {
				if (preg_match('/SQMSESSID=([^;]+);/', $line, $cookieTmp)) {
					$cookie = $cookieTmp[1];
				} else if (preg_match('/key=([^;]+);/', $line, $keyTmp)) {
					$key = $keyTmp[1];
				}
			}
			if (!$key || !$cookie) {
				return false;
			}
			$this->url = $this->getWebmailLink('sqmail') . '/dummyset.php?SQMSESSID=' .
				$cookie . '&key=' . $key . '&return=src/webmail.php';

			return true;
		}

		private static function _request_page(
			$url,
			$post = null,
			$cookie = [],
			$referer = null,
			$ua = null,
			$curlopt = []
		) {
			if (!$ua) {
				$ua = $_SERVER['HTTP_USER_AGENT'];
			}
			if (!$referer) {
				$referer = $url;
			}
			if (\is_array($cookie)) {
				$cookie = implode("&", array_map(function ($k, $v) {
					return urlencode($k) . '=' . urlencode($v);
				}, $cookie, array_keys($cookie)));
			}
			$curl = new \CurlLayer();
			$curl->toggle_body(true);
			if ($post) {
				$curl->set_form_method('POST');
			} else {
				$curl->set_form_method('GET');
			}
			$routed = \Opcenter\Net\Iface::routed() ?? '127.0.0.1';
			$opts = [
				CURLOPT_POSTFIELDS     => $post,
				CURLOPT_COOKIE         => $cookie,
				CURLOPT_BINARYTRANSFER => 0,
				CURLOPT_HEADER         => 1,
				CURLOPT_REFERER        => $referer,
				CURLOPT_USERAGENT      => $ua,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_COOKIEFILE     => 1,
				CURLOPT_RESOLVE        => [SERVER_NAME . ':443:' . $routed, SERVER_NAME . ':80:' . $routed]
			];
			foreach ($curlopt as $k => $v) {
				$opts[$k] = $v;
			}
			$curl->change_option($opts);
			$data = $curl->try_request($url);

			return $data;
		}

		public function getWebmailLink($app, $includeproto = true)
		{
			static $hasSsl;
			if (null === $hasSsl) {
				$hasSsl = Letsencrypt::bootstrapped();
			}

			if (empty(static::DEFAULT_WEBMAIL[$app])) {
				fatal("unknown webmail app `%s'", $app);
			}
			$proto = $hasSsl ? 'https://' : 'http://';
			$server = false === strpos(SERVER_NAME, '.') ? $_SERVER['SERVER_ADDR'] : SERVER_NAME;

			return ($includeproto ? $proto : '') . $server . static::DEFAULT_WEBMAIL[$app];
		}

		private function _accessHorde()
		{
			// get the Horde cookie
			$cookie = $auth_key = $auth_name = $imp_key = null;

			$data = self::_request_page(
				$this->getWebmailLink('horde') . '/login.php',
				'Horde='
			);

			foreach (explode("\n", $data) as $line) {
				if (preg_match('/Horde=([^;]+);/', $line, $cookieTmp)) {
					$cookie = $cookieTmp[1];
					break;
				}
			}
			if (!$cookie) {
				return error("Unable to retrieve Horde session cookie");
			}

			// now get the auth_key
			$data = self::_request_page(
				$this->getWebmailLink('horde') . '/login.php',
				'Horde=' . $cookie[1] . '&login_button=Log+in'
			);

			foreach (explode("\n", $data) as $line) {
				if (preg_match('/\b((?:horde_)?secret_key|auth_key)=([^;]+);/', $line, $authTmp)) {
					$auth_name = $authTmp[1];
					$auth_key = $authTmp[2];
					break;
				}
			}

			$data = self::_request_page(
				$this->getWebmailLink('horde') . '/login.php',
				[
					'app'               => '',
					'url'               => '',
					'horde_user'        => $_SESSION['username'] . '@' . $_SESSION['domain'],
					'horde_pass'        => $this->getPassword(),
					'new_lang'          => \Preferences::get('language', 'en_US'),
					'login_button'      => 'Log+in',
					'horde_select_view' => 'auto',
					// newer Horde
					'login_post'        => 1,
				],
				'Horde=' . $cookie . '; ' . $auth_name . '=' . $auth_key . ';',
				null,
				$_SERVER['HTTP_USER_AGENT']
			);
			foreach (explode("\n", $data) as $line) {
				if (preg_match('/Horde=([^;]+);/', $line, $cookieTmp)) {
					$cookie = $cookieTmp[1];
					break;
				}
			}

			if (!$cookie) {
				return false;
			}
			$this->url = $this->getWebmailLink('horde') . '/dummyset.php?Horde=' . $cookie .
				'&' . $auth_name . '=' . $auth_key . '&return=services/portal/';

			return true;
		}

		private function _accessRoundCube()
		{
			$cookie = null;
			$data = self::_request_page($this->getWebmailLink('roundcube'), null, null, null, null,
				[
					CURLOPT_HEADER => 1
				]
			);
			$dom = new \DOMDocument();
			silence(function () use ($dom, $data) {
				$dom->loadHTML($data);
			});
			$token = null;
			foreach ($dom->getElementsByTagName('input') as $e) {
				$name = $e->getAttribute("name");
				if ($name === "_token") {
					$token = $e->getAttribute("value");
					break;
				}
			}
			foreach (explode("\n", $data) as $line) {
				if (preg_match('/roundcube_sessid=([^;]+);/', $line, $cookie)) {
					if ($cookie[1] == 'deleted') {
						continue;
					}
					$cookie = $cookie[1];
					break;
				}
			}
			if (!$cookie || !$token) {
				return false;
			}
			$data = self::_request_page(
				$this->getWebmailLink('roundcube') . '/?_task=login',
				[
					'_token'  => $token,
					'_action' => 'login',
					'_task'   => 'login',
					'_url'    => '_task=login',
					'_user'   => $_SESSION['username'] . '@' . $_SESSION['domain'],
					'_pass'   => $this->getPassword(),
				],
				'roundcube_sessid=' . $cookie . ';',
				'',
				$_SERVER['HTTP_USER_AGENT'],
				[
					CURLOPT_HEADER         => 1,
					// following location without sending cookies will invalidate
					CURLOPT_FOLLOWLOCATION => 1,
				]
			);
			// $token is then suffixed with creation timestamp
			$nonce = null;
			foreach (explode("\n", $data) as $line) {
				if (preg_match('/roundcube_sessauth=([^;]+);/', $line, $tmp)) {
					if (strlen($tmp[1]) < 24) {
						continue;
					}
					$nonce = $tmp[1];
				} else if (preg_match('/roundcube_sessid=([^;]+);/', $line, $tmp)) {
					// alternatively parse out the cookie and extract max-age
					if ($tmp[1] == 'deleted' || $tmp[1] === "-del-") {
						continue;
					}
					$cookie = $tmp[1];
				}
			}
			if (!$nonce) {
				return false;
			}
			$this->url = $this->getWebmailLink('roundcube') .
				'/dummyset.php?token=' . $token . '&sessauth=' . $nonce . '&cookiePath=/&sessid=' . $cookie . '&return=' .
				urlencode('./?_task=mail&_token=' . $token);

			return true;
		}

		private function getPassword()
		{
			if (empty($_SESSION['password']) || !($_SESSION['password'] instanceof NaiveCrypt)) {
				return (string)($_SESSION['password'] ?? '');
			}

			return $_SESSION['password']->get();
		}
	}