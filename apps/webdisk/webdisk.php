<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\webdisk;

	use Page_Container;

	class Page extends Page_Container
	{

		public function __construct()
		{
			parent::__construct();
			$this->add_css('webdisk.css');
			$this->add_javascript('webdisk.js');
		}

		public function on_postback($params)
		{

		}

		public function formatUri(): string {
			$server = \Auth_Redirect::CP_LAYOUT ? SERVER_NAME_SHORT : SERVER_NAME;
			return \Auth_Redirect::makeCPFromServer($server);
		}
	}