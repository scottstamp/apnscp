<div class="col-6 col-sm-8 col-md-9 col-lg-10 float-right text-right" id="quick-links">
	@foreach ($Page->favorites(3) as $favorite)
		@php
			$info = Template_Engine::init()->getApplicationFromId($favorite);
			if (!$info->exists()) {
				continue;
			}
			$category = $info->getCategory() ?: $info->getInternal();
		@endphp
		<a class="btn btn-outline-primary hidden-sm-down mb-3 mt-0" href=" {{ $info->getLink() }}">
			<i class="hidden-md-down ui-action ui-action-label ui-menu-category-{{ $category }}"></i>
			{{  $info->name }}
		</a>
	@endforeach
	@if (!\UCard::is('admin'))
		<a href="{{ MISC_KB_BASE }}" class="hidden-xs-down btn btn-outline-primary mb-3 mt-0">
			<i class="ui-action-doclink ui-action"></i>
			Help Center
		</a>
	@else
		<a href="https://docs.apiscp.com" class="hidden-xs-down btn btn-outline-primary mb-3 mt-0">
			<i class="ui-action-doclink ui-action"></i>
			Platform Docs
		</a>
	@endif
	@includeWhen(\UCard::is('site') && cmd('crm_configured'), 'partials.crm.ticket-link')
</div>