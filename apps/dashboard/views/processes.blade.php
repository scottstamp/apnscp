<!-- process management -->
<div class="process-container  hidden-sm-down m-y-3">
	<h2 class="title processes">Processes</h2>
	<div id="processes" class="clearfix">
		<div class="row process-header">
			<div class="col-2">Name</div>
			<div class="col-2 text-right">Memory</div>
			<div class="col-2 text-right">CPU</div>
			<div class="col-2 hidden-sm-down">Age</div>
			<div class="col-2 hidden-sm-down">User</div>
			<div class="col-2 center">Action</div>
		</div>
		@includeWhen(!($procs = $Page->getProcesses()), 'partials.process.inactive')
		<div id="processTable">
			@includeWhen($procs, 'partials.process.process')
		</div>
	</div>
</div>