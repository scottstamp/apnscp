<div class="btn-group {{ $btnGroupClass ?? '' }}">
	<a class="ui-action ui-action-select ui-action-label btn btn-secondary" href="{{ $href }}">
		Select
	</a>
	<button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
	        aria-expanded="false">
		<span class="sr-only">Toggle Dropdown</span>
	</button>
	<div class="dropdown-menu" aria-labelledby="">
		<a class="ui-action-folder-browse ui-action-label dropdown-item ui-action"
		   data-docroot="{{ $pane->getDocumentRoot() }}"
		   data-hostname="{{ $pane->getHostname() }}" href="#">
			Select Folder
		</a>
		<a class="ui-action-visit-site ui-action-label dropdown-item ui-action" href="{{ $pane->getUrl() }}">
			Visit Site
		</a>
		<a class="ui-action-manage-files ui-action-label dropdown-item ui-action"
		   href="{{ HTML_Kit::new_page_url_params('/apps/filemanager', ['f' => $pane->getDocumentRoot()]) }}">
			Manage Files
		</a>
	</div>
</div>