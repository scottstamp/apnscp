<label class="form-control-static">
	Magento Connect
</label>
<fieldset class="form-group">
	@php $key = \Util_Conf::call('magento_get_key'); @endphp
	@if ($key)
		<button class="btn btn-secondary warn" name="delete-magento-key" id="delete-magento-key">
			<i class="ui-action ui-action-delete"></i>
			Delete
		</button>
		&lt;key hidden&gt;
	@else
		<fieldset class="form-group mb-0">
			<label class="my-0">
				<i class="fa fa-lock"></i> Public Key
				<input type="text" class="form-control" name="magento-pubkey" id="magento-key"
				       value=""/>
			</label>
			<label class="my-0">
				<i class="fa fa-user-secret"></i> Private Key
				<input type="text" class="form-control" name="magento-privkey" id="magento-key"
				       value=""/>
			</label>
		</fieldset>
		<div class="alert alert-info" role="alert" id="origin-note">
			<i class="fa fa-sticky-note"></i> Magento 2.x requires an account with
			magentocommerce.com.
			(see Magento: <a class="ui-action ui-action-label ui-action-kb"
			                 href="http://devdocs.magento.com/guides/v2.0/install-gde/prereq/connect-auth.html">Get
				your authentication keys</a>)
		</div>
	@endif
</fieldset>