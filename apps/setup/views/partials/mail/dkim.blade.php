<tr>
	<th>
		<span class="ui-action-copy" data-clipboard-text="{{ $auth->dkimSelector() }}._domainkey">
			<b class="text-uppercase" title="Copy to Clipboard" id="clipboardCopyDkim"
			   data-toggle="tooltip"></b>
			{{_("DKIM selector")}} ({{ _("DNS record") }})
		</span>

	</th>
	<td>
		<code>{{ $auth->dkimSelector() }}</code>
		({{ $auth->dkimSelector() }}._domainkey)
	</td>
</tr>
<tr>
	<th>
		<span class="ui-action-copy" data-clipboard-text="{{ $auth->dkimKey() }}">
			<b class="text-uppercase" title="Copy to Clipboard" id="clipboardCopyDkim"
			   data-toggle="tooltip"></b>
			{{_("DKIM key")}}
		</span>
	</th>
	<td>
		<code class="d-block" aria-describedby="clipboardCopyDkim" class="mr-2">
			{!! wordwrap($auth->dkimKey(), 80, "<br />", true) !!}
		</code>
	</td>
</tr>