<tr>
	<th>
			<span class="ui-action-copy" data-clipboard-text="{{ MAIL_DEFAULT_DMARC  }}">
				<b class="text-uppercase" title="Copy to Clipboard" id="clipboardCopyDmarc"
				   data-toggle="tooltip"></b>
				DMARC
			</span>
	</th>
	<td>
		<code>{{ MAIL_DEFAULT_DMARC }}</code>
	</td>
</tr>