<div class="btn-group">
	<button type="submit" id="uninstallNote"
	        data-prompt="{{ \ArgumentFormatter::format("Are you sure you want to delete all files under %s?", [$app->getDocumentMetaPath()]) }}"
	        class="ajax-wait btn btn-secondary warn ui-action ui-action-label"
	        name="uninstall" value="{{ $app->getDocumentMetaPath() }}">
		<i class="fa ui-action-delete"></i>
		Uninstall
	</button>
	<button type="button" class="btn btn-secondary dropdown-toggle"
	        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		<span class="sr-only">Toggle Dropdown</span>
	</button>
	<div class="dropdown-menu dropdown-menu-right dropdown-menu-form">
		<div class="dropdown-item">
			<label class="custom-control custom-checkbox mb-0">
				<input type="checkbox" class="custom-control-input"
				       @if ($app->getAppRoot() !== "/var/www/html") checked="CHECKED" @endif
				       name="delfiles"/>
				<span class="custom-control-indicator"></span>
				Delete {{ $app->getAppRoot() }}
			</label>
		</div>

		<div class="dropdown-item delete-db">
			<label class="custom-control custom-checkbox mb-0 d-block">
				<input type="checkbox" class="custom-control-input" checked="CHECKED"
				       name="deldb"/>
				<span class="custom-control-indicator"></span>
				Delete database + user
				<br/>
				<small><b>DB: </b><i class="dbname"></i></small>
				<br/>
				<small><b>User: </b><i class="dbuser"></i></small>
			</label>
		</div>
	</div>
</div>