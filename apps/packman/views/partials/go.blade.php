<div class="mb-3">
	<p class="example">
		Example: to install a new Go version
	</p>
	<pre class="command-line" data-host="{{ SERVER_NAME_SHORT }}" data-user="{{ \Session::get('username', 'root') }}">
		<code class="language-bash">
			# Show available versions
			goenv install -l
			# Install v1.9.3
			goenv install 1.9.3
			# Set global version, this must be done once
			goenv global 1.9.3
		</code>
	</pre>
</div>

<div class="mb-3">
	<p class="example">
		Example: to change Go versions after first setting a system version
	</p>
	<pre class="command-line" data-host="{{ SERVER_NAME_SHORT }}" data-user="{{ \Session::get('username', 'root') }}">
		<code class="language-bash">goenv local 1.10.2
		go version
		</code>
	</pre>
</div>