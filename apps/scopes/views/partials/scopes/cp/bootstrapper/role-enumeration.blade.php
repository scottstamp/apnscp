@foreach ($value as $k => $v)
	<div class="row @if ($loop->index % 2) bg-light @endif">
		<div class="col-12 mb-3">
			<label class="form-control-label mb-0 d-flex flex-column flex-md-row justify-content-start justify-content-md-between">
				<span class="mr-3 d-md-inline d-flex">
					{{ $k }}
				</span>
				<span class="ml-md-auto d-md-inline d-flex help text-light text-md-right font-weight-normal ui-action-label small help">
					{{ $scope->getVariableHelp($k) }}
				</span>
			</label>
			@include('partials.scope-internals.parse-type', [
				'setting' => gettype($v),
				'value'   => $v,
				'name'    => $k,
				'attrs'   => ''
			])
		</div>
		<hr/>
	</div>
@endforeach