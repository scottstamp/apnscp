<tr>
	<th class="pt-4">Pool usage</th>
	<td>
		@php
			/** @var \Opcenter\Http\Php\Fpm\PoolStatus $pool */
			$metrics = $pool->getMetrics();
		@endphp
		<table class="table text-center">
			<thead>
			@foreach (array_keys($metrics) as $head)
				<th>{{ ucwords($head) }}</th>
			@endforeach
			</thead>
			<tbody>
			@foreach($metrics as $label => $value)
				<td>
					{{$value}}
					@if ($label === 'traffic')
						req/sec
					@endif
				</td>
			@endforeach
			</tbody>
		</table>
	</td>
</tr>

<tr>
	<th class="pt-4">Cache performance</th>
	<td>
		@php
			$metrics = $pool->getCacheMetrics(\Auth::profile());
		@endphp

		@if (isset($metrics['directives']))
			@include('partials.cache-metrics')
		@else
			<p class="alert alert-warning">
				Cache status unknown.
			</p>
		@endif
	</td>
</tr>