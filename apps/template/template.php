<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\template;

	class Page extends \Page_Container
	{
		public function __construct($doPostback = true)
		{
			parent::__construct($doPostback);
			\Page_Renderer::hide_tutorial();
		}

		public function index() {
			return view('index');
		}

		public function speak($verb) {
			return "Hello $verb!";
		}

		public function showTutorial() {
			\Page_Renderer::show_tutorial();
			return $this->index();
		}
	}