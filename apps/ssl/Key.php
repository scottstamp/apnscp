<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2018
	 */

	namespace apps\ssl;

	class Key implements \ArrayAccess
	{
		// @var string
		protected $key;

		// @var resource
		protected $resource;

		// @var array
		protected $info;

		public function __construct(string $key)
		{
			$this->key = $key;
			$this->resource = openssl_pkey_get_private($key);
			$this->info = openssl_pkey_get_details($this->resource);
		}

		public function __toString()
		{
			return $this->key;
		}

		public function getIterator()
		{
			return $this->info;
		}

		public function offsetExists($offset)
		{
			return isset($this->info[$offset]);
		}

		public function offsetGet($offset)
		{
			return $this->info[$offset];
		}

		public function offsetSet($offset, $value)
		{
			fatal("Cannot modify private key resource");
		}

		public function offsetUnset($offset)
		{
			fatal("Cannot modify private key resource");
		}


	}


