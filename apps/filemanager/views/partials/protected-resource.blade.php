<div class="row text-center">
	<div class="col-12">
		<h3 class="text-danger">
			Protected Area
		</h3>

		<img src="/images/apps/filemanager/protected-resource.svg" title="Protected Resource" style="max-height: 256px;" class="img img-fluid mx-auto my-2" />
	</div>
	<div class="col-12">
		<p class="lead">
			Your settings prevent viewing this location.
		</p>


		<a href="/apps/changeinfo#apps" class="mr-5 btn ui-action ui-action-label ui-action-switch-app">
			Change Settings
		</a>
		<a href="javascript:history.go(-1);" class="ui-action ui-action-restore ui-action-label btn btn-primary">
			Go Back
		</a>
	</div>
</div>
