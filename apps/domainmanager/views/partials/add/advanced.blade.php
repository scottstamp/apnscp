<div class="row form-group">
	<div class="mt-1 col-12 collapse" id="advancedOptions">
		<h4>Advanced Options</h4>
		<label class="custom-checkbox custom-control my-1 align-items-center d-flex" for="enable_email">
			<input class="custom-control-input" type="checkbox" name="enable_email" value="1"
			       id="enable_email"/>
			<span class="custom-control-indicator"></span>
			Enable email for this account
			<em class="ml-2 font-weight-normal">
				see <a class="ui-action ui-action-label ui-action-switch-app" href="/apps/mailertable">Mail Routing</a>
			</em>
		</label>


		@if (count($mailTransports) > 0)
			<div id="clone_domain_row" class="form-group">
					<label class="custom-checkbox custom-control my-1 align-items-center d-flex" for="clone_domain">
						<input class="custom-control-input" type="checkbox" name="clone_domain" value="1"
						       id="clone_domain"/>
						<span class="custom-control-indicator"></span>
						Clone mailbox entries from existing domain
					</label>
					<select name="domain_clone" class="form-control custom-select">
						@foreach ($mailTransports as $domain)
							<option value="{{ $domain }}">
								{{ $domain }}
							</option>
						@endforeach
					</select>
			</div>
		@endif
		@include('partials.add.preload-options')
	</div>
</div>