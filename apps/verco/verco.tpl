<?php
?>
<!-- main object data goes here... -->
<div class="alert alert-info">
	All repositories may be managed from the terminal &mdash;
	<a href="<?=MISC_KB_BASE?>/terminal/accessing-terminal/" class="ui-action ui-action-label ui-action-kb">SSH</a>
	or Dev &gt;
	<a href="/apps/terminal" class="ui-action ui-action-label ui-action-switch-app">Terminal</a>. git, svn, bzr, and hg
	are supported!
</div>