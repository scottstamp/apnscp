<h3 class="mt-3"><span id="counter">{{ \count($whitelist) }}</span>/{{ \cmd('rampart_max_delegations') ?? '∞' }} Active</h3>
<form method="POST" id="active">
	@foreach ($whitelist as $entry)
		<div class="row">
			<div class="col-12 mb-2">
				<button type="submit" data-ip="{{$entry}}" onClick="return confirm('Are you sure you want to delete ' + $(this).val() + '?');" name="remove" value="{{ $entry }}"
				        class="ui-action warn ui-action-label ui-action-delete btn btn-secondary bindable">
					Delete
				</button>
				{{ $entry }}
				(HOST: <span class="host">Unknown</span>)
			</div>
		</div>
	@endforeach
</form>

<div class="row hide" id="item-template">
	<div class="col-12 mb-2">
		<button type="submit" data-ip="" onClick="return confirm('Are you sure you want to delete ' + $(this).val()  + '?');" name="remove"
		        value=""
		        class="ui-action warn ui-action-label ui-action-delete btn btn-secondary bindable">
			Delete
		</button>
		<span class="ip"></span>
		(HOST: <span class="hostname">Unknown</span>)
	</div>
</div>