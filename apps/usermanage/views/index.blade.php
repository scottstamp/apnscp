<div class="d-flex">
	@include('master::partials.shared.search', [
		'filter' => new \apps\usermanage\models\Search
	])
	<div class="ml-auto d-none d-lg-block align-self-end mb-4">
		<a href="/apps/useradd" class="ui-action ui-action-switch-app ui-action-label btn btn-secondary-outline">
			Add User
		</a>
	</div>
</div>

<form method="post">
	<table width="100%" id="users" class="tablesorter table">
		<thead>
		<tr>
			<th class="check_cell">
			</th>
			<th class="left">
				Username
			</th>
			<th class="left hidden-md-down">
				Full Name
			</th>
			<th class="center" id="quota-header">
				Storage
			</th>
			<th class="center status-header status_cell" id="status-header">
				<div class="hidden-sm-down">
					@if (\cmd('email_configured'))
					<div class="status">
						mail
					</div>
					@endif
					<div class="status">
						ftp
					</div>
					@if (DAV_ENABLED)
						<div class="status">
							dav
						</div>
					@endif
					<div class="status">
						web
					</div>
					<div class="status">
						cp
					</div>
					<div class="status">
						@if ($Page->ssh_enabled()) ssh @endif
					</div>
				</div>
			</th>
			<th class="actions center">
				Actions
			</th>
		</tr>
		</thead>
		<tbody>
		@php
			$admin = \Util_Conf::get_svc_config('siteinfo', 'admin_user');
		@endphp
		@foreach ($Page->get_users() as $user => $info)
			@php
				if ($info['quota-total']) {
					$quota_pct = min(100, round($info['quota-used'] / $info['quota-total'] * 100));
				} else {
					$quota_pct = 0;
				}
				if ($quota_pct >= 85)
					$over_quota_cls = 'ui-gauge-crit';
				else if ($quota_pct >= 65) {
					$over_quota_cls = 'ui-gauge-warn';
				} else {
					$over_quota_cls = 'ui-gauge-normal';
				}
			@endphp
			<tr class="entry @if ($quota_pct >= 100) quota-over @endif">
				<td class="check_cell align-middle">
					@if ($user !== $admin)
						<label class="custom-checkbox custom-control align-items-center mb-0 d-flex">
							<input type="checkbox" class="custom-control-input" name="users[]"
							       value="{{ $user }}" id=""/>
							<span class="custom-control-indicator"></span>
							{{ $user }}
						</label>
					@endif
				</td>
				<td class="username">
					{{ $user }}
				</td>
				<td class="gecos hidden-md-down">
					{{ $info['gecos'] }}
				</td>
				<td class="quota-cell monospace">
					<div class="ui-gauge-cluster {{ $over_quota_cls}}">
						<div class="ui-gauge" id="quota-{{$user}}">
							<div class="ui-gauge-used ui-gauge-slice"></div>
							<div class="ui-gauge-free ui-gauge-slice"></div>
							<div class="ui-gauge-slice ui-gauge-cap"></div>
						</div>
						<div class="ui-label-cluster">
							<span class="ui-gauge-label ui-label-percentage">{{$quota_pct}}%</span>
							<span class="ui-gauge-label ui-label-used">{{sprintf("%u", $info['quota-used'])}} MB</span>
							<span class="ui-gauge-label ui-label-total">{{sprintf("%u", $info['quota-total'])}}
								MB</span>
						</div>
					</div>
				</td>
				<td class="status_cell">
					<div class="hidden-sm-down">
						@if (\cmd('email_configured'))
						<div class="status mail">
							<span class="ui-action {{$Page->enable_css_class($user, 'mail')}}"></span>
						</div>
						@endif
						<div class="status ftp">
							<span class="ui-action {{$Page->enable_css_class($user, 'ftp')}}"></span>
						</div>
						@if (DAV_ENABLED)
							<div class="status dav">
								<span class="ui-action {{$Page->enable_css_class($user, 'dav')}}"></span>
							</div>
						@endif
						<div class="status www">
							<span class="ui-action {{$Page->enable_css_class($user, 'www')}}"></span>
						</div>
						<div class="status cp">
							<span class="ui-action {{$Page->enable_css_class($user, 'cp')}}"></span>
						</div>
						<div class="status ssh">
							<span class="ui-action {{$Page->enable_css_class($user, 'ssh')}}"></span>
						</div>
					</div>
				</td>
				<td class="actions">
					<div class="align-items-center d-flex">
						<div class="col-12">
							<div class="btn-group">
								@if ($user !== $admin)
									<button class="mx-auto btn btn-secondary float-left" name="edit[{{$user}}]"
									        value="{{$user}}">
										<i class="ui-action ui-action-edit"></i>
										Edit
									</button>
								@else
									<a class="mx-auto ui-action ui-action-edit ui-action-label float-left btn btn-secondary"
									   href="/apps/changeinfo">Edit</a>
								@endif
								@if ($user !== $admin)
									<button type="button" class="btn btn-secondary dropdown-toggle"
									        data-toggle="dropdown"
									        aria-haspopup="true"
									        aria-expanded="false">
										<span class="sr-only">Toggle Dropdown</span>
									</button>
									<div class="dropdown-menu" aria-labelledby="">
										<button class="ui-action-login ui-action-label rounded-0 dropdown-item ui-action btn btn-block"
										        name="hijack" value="{{ $user }}">
											Login As
										</button>
										<div class="dropdown-divider"></div>
										<button title="Delete {{$user}}"
										        class="mx-auto dropdown-item rounded-0 warn ui-action-delete"
										        name="delete[{{$user}}]" value="{{$user}}">
											Delete
										</button>
									</div>
								@endif
							</div>
						</div>
					</div>
				</td>
			</tr>
		@endforeach
		</tbody>
		<tfoot>
		<tr>
			<td class="center" colspan="6">
				Select multiple users from the <span class="fa fa-check-square-o"></span> column and perform an action:
				<button type="submit" class="btn btn-secondary warn" name="remove_users">
					Remove User
				</button>
			</td>
		</tr>
		</tfoot>
	</table>
</form>