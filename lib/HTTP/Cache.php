<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, March 2021
 */

namespace HTTP;

class Cache {
	use \ContextableTrait;
	use \apnscpFunctionInterceptorTrait;

	public function purge(string $domain): bool
	{
		$domain = $this->web_normalize_hostname($domain);
		if (!$this->web_get_docroot($domain)) {
			return false;
		}

		$ctx = stream_context_create(array(
			'http' =>
				array(
					'timeout'          => 5,
					'method'           => 'PURGE',
					'header'           => [
						"User-agent: " . PANEL_BRAND . " Internal check",
						"Host: ${domain}"
					],
					'protocol_version' => '1.1'
				)
		));

		return (bool)@get_headers('http://' . $this->site_ip_address() . '/*', PHP_MAJOR_VERSION >= 8 ? false : 0, $ctx) ?:
			error("failed to PURGE %s", $domain);
	}
}