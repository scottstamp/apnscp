<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Provisioning;

	use Opcenter\Filesystem;
	use Opcenter\Provisioning\Traits\ApacheModule;
	use Opcenter\SiteConfiguration;

	class Logs
	{
		use ApacheModule;
		const CONFIG_NAME = 'custom_logs';

		public static function createLogrotate(SiteConfiguration $svc): bool
		{
			$config = (new ConfigurationWriter('logs/logrotate-config', $svc))->compile();
			$file = $svc->getAccountRoot() . '/etc/logrotate.conf';

			return file_put_contents($file, $config) > 0 && Filesystem::chogp($file, 'root', 'root', 0644);
		}

		/**
		 * Create logrotate profile for service
		 *
		 * @param SiteConfiguration $svc
		 * @param string            $template
		 * @param string            $cfgname
		 * @return bool
		 */
		public static function createProfile(SiteConfiguration $svc, string $template, string $cfgname): bool
		{
			$path = $svc->getAccountRoot() . "/etc/logrotate.d/${cfgname}";
			$config = (new ConfigurationWriter($template, $svc))->compile();

			return file_put_contents($path, $config) > 0;
		}

		public static function renameProfile(SiteConfiguration $svc, string $old, string $new): bool
		{
			if (!static::exists($svc, $old)) {
				return false;
			}

			if (static::exists($svc, $new)) {
				return false;
			}

			return rename(
				$svc->getAccountRoot() . "/etc/logrotate.d/${old}",
				$svc->getAccountRoot() . "/etc/logrotate.d/${new}"
			);
		}

		/**
		 * Log rotation profile exists
		 *
		 * @param string $cfgname
		 * @return bool
		 */
		public static function exists(SiteConfiguration $svc, string $cfgname): bool
		{
			return file_exists($svc->getAccountRoot() . "/etc/logrotate.d/${cfgname}");
		}

		/**
		 * Unlink logrotation profile
		 *
		 * @param SiteConfiguration $svc
		 * @param string            $cfgname
		 * @return bool
		 */
		public static function deleteProfile(SiteConfiguration $svc, string $cfgname): bool
		{
			$path = $svc->getAccountRoot() . "/etc/logrotate.d/${cfgname}";
			if (!file_exists($path)) {
				return false;
			}

			return unlink($path);
		}

		/**
		 * Create log rotation cronjob
		 *
		 * @param SiteConfiguration $svc
		 * @return bool
		 */
		public static function createCronjob(SiteConfiguration $svc): bool
		{
			$config = (new ConfigurationWriter('logs/logrotate-cron', $svc))->compile();
			$job = $svc->getAccountRoot() . '/etc/cron.daily/logrotate';

			return file_put_contents($job, $config) > 0 && chmod($job, 0755);
		}

		public static function deleteCronjob(SiteConfiguration $svc): bool
		{
			$job = $svc->getAccountRoot() . '/etc/cron.daily/logrotate';
			if (file_exists($job)) {
				unlink($job);
			}

			return true;
		}
	}


