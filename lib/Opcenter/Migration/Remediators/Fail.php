<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, August 2019
 */

namespace Opcenter\Migration\Remediators;

use Opcenter\Migration\Remediator;

class Fail extends Remediator
{
	/**
	 * Item conflicts
	 *
	 * @param string $key
	 * @param        $val
	 * @return bool
	 */
	public function conflict(string $key, $val): bool
	{
		return isset($this->data[$key]);
	}
}
