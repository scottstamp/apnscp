<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, June 2019
 */

namespace Opcenter\Migration;

use Opcenter\Filesystem;

/**
 * Class Locker
 *
 * Relocate a pending task
 *
 * @package Opcenter\Migration
 */
class Locker {

	// @var string $file file of advisory lock
	/**
	 * @var bool delete file on release
	 */
	protected $delete = false;
	private $file;
	// @var string $relocatedPath temporary path of the folder
	private $relocatedPath;

	private function __construct(string $file)
	{
		$this->file = $file;
		for ($i = 0; $i < 10; $i++) {
			$path = \dirname($this->file) . DIRECTORY_SEPARATOR . uniqid('apnscp-migration', true);
			if (!is_dir($path)) {
				if (!Filesystem::mkdir($path, 'root', 'root', 0700)) {
					fatal("Failed to create advisory lock path `%s'", $path);
				}
				$this->relocatedPath = $path . DIRECTORY_SEPARATOR . basename($this->file);
				if (!\rename($this->file, $this->relocatedPath)) {
					fatal("Failed to relocate `%s' to `%s'", $this->file, $this->relocatedPath);
				}
				// kill open file handles
				Filesystem\Fuser::bindTo()->kill($this->relocatedPath);
				break;
			}
		}
	}

	public function __destruct()
	{
		$this->unlock();
	}

	/**
	 * Get locked asset
	 *
	 * @return string|null
	 */
	public function getFile(): ?string {
		return $this->relocatedPath;
	}

	/**
	 * Delete archive on lock release
	 *
	 * @param bool $mode
	 */
	public function setDeleteOnRelease(bool $mode): void {
		$this->delete = $mode;
	}

	/**
	 * Create advisory lock
	 *
	 * @param string $file
	 * @return Locker
	 */
	public static function lock(string $file): self
	{
		if (!file_exists($file)) {
			fatal("Cannot create advisory lock - path `%s' does not exist", $file);
		}

		return new static($file);
	}

	/**
	 * Release advisory lock
	 *
	 * @return bool
	 */
	public function unlock(): bool
	{
		if (!$this->relocatedPath) {
			return true;
		} else if (!file_exists($this->relocatedPath)) {
			debug("Advisory lock on `%s' is missing, cannot revert permissions", $this->file);
			return false;
		}
		$relocPath = $this->relocatedPath;
		$this->relocatedPath = null;
		if (file_exists($this->file)) {
			warn("Failed to release advisory lock on `%s', backup located in `%s'", $this->file, $this->relocatedPath);
			return false;
		}

		if (! (rename($relocPath, $this->file) && rmdir(\dirname($relocPath))) ) {
			warn("Failed to remove advisory lock directory `%s'", $relocPath);
		} else if ($this->delete && file_exists($this->file)) {
			return $this->removeBackup() ? info("Backup completed successfully. Removing `%s'", $this->file) :
				warn("Failed to remove backup `%s'. File missing?", $this->file);
		}
		return true;
	}

	/**
	 * Remove backup from filesystem
	 *
	 * @return bool
	 */
	private function removeBackup(): bool
	{
		if (!file_exists($this->file)) {
			return false;
		}
		if (is_link($this->file) || is_file($this->file)) {
			return unlink($this->file);
		}
		$ret = \Util_Process_Safe::exec('rm -rf %s', $this->file);

		return $ret['success'];
	}
}