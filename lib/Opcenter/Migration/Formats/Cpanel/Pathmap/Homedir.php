<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2019
	 */

	namespace Opcenter\Migration\Formats\Cpanel\Pathmap;


	use Event\Cardinal;
	use Event\Events;
	use Opcenter\Mail\Services\Webmail;
	use Opcenter\Migration\Contracts\BackupStreamInterface;
	use Opcenter\Migration\Formats\Cpanel\ConfigurationBuilder;
	use Opcenter\Migration\Formats\Cpanel\Shadow;
	use Opcenter\Migration\Formats\Cpanel\User;
	use Opcenter\Migration\Helpers\PathTranslate;
	use Opcenter\Migration\Import;
	use Opcenter\Migration\Notes\MailTransports;
	use Opcenter\Migration\Notes\User as UserNote;
	use Opcenter\Migration\Remediator;
	use Opcenter\Net\Ip4;
	use Opcenter\Net\Ip6;
	use Opcenter\Service\Validators\Rampart\Whitelist;
	use Opcenter\SiteConfiguration;

	class Homedir extends ConfigurationBuilder
	{
		const DEPENDENCY_MAP = [
			Meta::class,
		];
		// @var array list of directories to relocate/remove
		const REMAPS = [
			'public_html'    => '/var/www/html',
			'mail'           => '~/Mail',
			'www'            => null,
			'etc'            => null,
			'public_ftp'     => null,
			'tmp'            => null,
			'cpanel3-skel'   => null,
			'cpbackup-exclude.conf' => null,
			'cpanelbranding' => null,
			'.cpanel-logs'   => null,
			'.rvskin'        => null,
			'.rvglobalsoft'  => null,
			'.cpanel'        => null
		];

		/**
		 * @var Remediator
		 */
		protected $remediator;

		/**
		 * @var array list of DAV accounts
		 */
		private $davAccounts = [];

		private $emailMap;

		public function __construct(Import $task, BackupStreamInterface $archiveStream)
		{
			parent::__construct($task, $archiveStream);
			$this->emailMap = $this->getEmailUserMap();
		}


		public function parse(): bool
		{
			// add last IP to Rampart
			$this->setupDelegatedWhitelist();
			// accounts always have email
			$this->bill->set('mail', 'provider', 'builtin');
			Cardinal::preempt(
				[\Opcenter\Account\Import::HOOK_ID, Events::SUCCESS],
				function ($event, SiteConfiguration $s) {
					$this->remediator = Remediator::create($this->task->getRemediator($s)->getStrategy(), $s->getAuthContext());
					$this->mapUsers($s);

					$afi = $s->getSiteFunctionInterceptor();
					$home = $afi->user_get_home();
					$this->stream->externalExtract(
						'/homedir',
						$s->getAccountRoot() . $home,
						$s->getAuthContext()->user_id,
						$s->getAuthContext()->group_id
					);
					foreach (static::REMAPS as $local => $target) {
						$src = "${home}/${local}";
						if ($target === null) {
							if (!$afi->file_delete($src, true)) {
								warn('Failed to delete %s', $local);
							}
							continue;
						}

						if ($target[0] === '~') {
							$target = $home . substr($target, 1);
						}

						$this->transmigratePath($afi, $src, $target);
					}
					// remap /home/<USER>/public_html to /var/www/html
					$translator = new PathTranslate();
					$translator->setLogPath($s->getAccountRoot() . '/changed.log');
					$oldUser = $this->bill->getNote(UserNote::class)->getOriginalUser();
					$this->convertPath($s, "/home/${oldUser}/public_html", '/var/www/html');
					// CloudLinux workaround:
					// Fixup public_html/ which on CL distributions may have "other" revoked.
					// Apache runs as "nobody" in the group thus obviating other permissions.
					// "other" for apnscp is the web server as all other accounts would be jailed
					// and group is the account quota
					$path = $s->getAccountRoot() . '/var/www/html';
					// set o+x
					chmod($path, fileperms($path)|0001);

					$maps = $this->getEmailUserMap();
					foreach (array_keys($maps) as $hostname) {
						if (!preg_match(\Regex::DOMAIN, $hostname)) {
							warn("Skipping garbled mail domain `%s'", $hostname);
						}

						if ($s->getSiteFunctionInterceptor()->file_exists("~/Mail/${hostname}")) {
							info('Removing duplicate Maildir in ~/Mail/%s', $hostname);
							$s->getSiteFunctionInterceptor()->file_delete("~/Mail/${hostname}", true);
						}

						foreach (array_get($maps[$hostname], 'accounts', []) as $user => $data) {
							$fuser = str_replace('.', '_', $user);
							$fdomain = str_replace('.', '_', $hostname);
							// cleanup dangling symlinks in user home
							if ($s->getSiteFunctionInterceptor()->file_stat("~/Mail/.${fuser}@${fdomain}")) {
								$s->getSiteFunctionInterceptor()->file_delete("~/Mail/.${fuser}@${fdomain}");
							}
						}
					}
				}
			);

			return true;
		}

		public function mapUsers(SiteConfiguration $s): void
		{
			$etcPath = $this->getDirectoryEnumerator()->getPath() . DIRECTORY_SEPARATOR . 'etc';
			if (!file_exists($etcPath)) {
				// no users or email
				return;
			}

			// webdav is a special file to control DAV access
			if (file_exists($path = implode(DIRECTORY_SEPARATOR, [$etcPath, 'etc', 'webdav', 'passwd']))) {
				$pwd = User::bindTo($path)->getpwnam(null);
				foreach ($pwd as $user => $attr) {
					$this->davAccounts[$user] = $attr;
				}
			}

			foreach (new \DirectoryIterator($etcPath) as $file) {
				if ($file->getFilename() === '.' || $file->getFilename() === '..') {
					continue;
				}
				$path = $file->getPathname();
				if (!is_dir($path)) {
					continue;
				}

				$domain = \basename($path);
				if ($domain === 'webdav') {
					// addressed earlier
					continue;
				}
				if (!preg_match(\Regex::HTTP_HOST, $domain)) {
					warn('%s is not a domain - skipping', $domain);
					continue;
				}
				if ($this->bill->getNote(MailTransports::class)->revoked($domain)) {
					if ($s->getSiteFunctionInterceptor()->email_transport_exists($domain)) {
						$s->getSiteFunctionInterceptor()->email_remove_virtual_transport($domain);
					}
				} else {
					$this->tryAddTransport($domain, $s->getSiteFunctionInterceptor());
				}

				if (!file_exists($path . DIRECTORY_SEPARATOR . 'passwd')) {
					warn('passwd missing in %s, skipping', $file);
					continue;
				}
				if (!file_exists($path . DIRECTORY_SEPARATOR . 'shadow')) {
					warn('shadow missing in %s, skipping', $file);
					continue;
				}

				$shadow = Shadow::bindTo($path)->getspnam(null);
				$passwd = User::bindTo($path)->getpwnam(null);
				foreach ($passwd as $user => $pwd) {
					if (!isset($shadow[$user]['shadow'])) {
						fatal("Shadow missing from `%s' ???", $user);
					}
					if (false === strpos($pwd['home'], $domain)) {
						$parts = explode(DIRECTORY_SEPARATOR, $pwd['home']);
						$key = (\array_search('mail', $parts, true) ?: 2) + 1;
						if ($parts[$key] !== $domain) {
							$parts[$key] = $domain;
							$newhome = implode(DIRECTORY_SEPARATOR, $parts);
							$pwd['home'] = $newhome;
							warn('Invalid HOME encountered in PWD. Converting %(old)s to %(new)s',
								['old' => $pwd['home'], 'new' => $newhome]);
						}
					}
					$this->addUser($s, $user, $domain, $shadow[$user]['shadow'], $pwd['uid'], $pwd['home'], $pwd['gecos'], rtrim($pwd['shell'], '/'));
				}
			}
		}

		private function getEmailUserMap(): array
		{
			$emailList = implode(DIRECTORY_SEPARATOR, [
				$this->getDirectoryEnumerator()->getPath(),
				'.cpanel',
				'email_accounts.json'
			]);
			if (!file_exists($emailList)) {
				return [];
			}
			return (array)json_decode(file_get_contents($emailList), true);
		}

		/**
		 * Create user on site
		 *
		 * @param int|string        $user
		 * @param string            $domain
		 * @param string            $passwd
		 * @param int               $uid
		 * @param string            $home
		 * @param string            $gecos
		 * @param string            $shell
		 * @param SiteConfiguration $s
		 */
		private function addUser(SiteConfiguration $s, $user, string $domain, string $passwd, int $uid, string $home, string $gecos = '', string $shell = '')
		{
			$newuser = '';
			$dav = DAV_ENABLED && \array_key_exists("${user}@${domain}", $this->davAccounts);
			if ($s->getSiteFunctionInterceptor()->email_transport_exists($domain)) {
				// has a mail transport
				try {
					if (\is_int($user)) {
						// user is parsed from 123@domain.com
						$newuser = $this->remediator->forceResolve(
							(string)$user,
							$domain,
							$user . '-' . substr($domain, 0, strpos($domain, '.'))
						);
						warn(
							"User `%(olduser)s' is numeric, which violates POSIX standards. Force conversion to `%(newuser)s'",
							['olduser' => $user, 'newuser' => $newuser]
						);
					} else {
						$newuser = $this->remediator->store($user, $domain);
					}
				} catch (\ValueError $e) {
					$conflictor = $this->remediator->getConflict($user);
					fatal('Conflict discovered using strategy %s: %s@%s conflicts with ambiguous user %s on %s',
						$this->remediator->getStrategy(), $user, $domain, $user, $conflictor);
				}
			}


			if (!$s->getSiteFunctionInterceptor()->user_exists($newuser)) {
				$domainData = array_get($this->emailMap, $domain, []);
				$data = array_get($domainData, "accounts.${user}", []);
				// old, new uid
				$enabled = !array_get($data, 'suspended_login', false);

				$marker = null;
				if (strncmp($passwd, '*LOCKED*', 8) === 0) {
					$marker = '*LOCKED*';
				} else if (strncmp($passwd, '!!', 2) === 0) {
					$marker = '!!';
				} else if (strncmp($passwd, 'no-login', 8) === 0) {
					$marker = 'no-login';
				}

				if ($marker) {
					$passwd = substr($passwd, \strlen($marker));
					$enabled = false;
					warn('cPanel goobledygook to disable login for user, stripping %s and forcing disablement', $marker);
				}

				if (!\Opcenter\Auth\Shadow::valid_crypted($passwd)) {
					if (false === ($pos = strpos($passwd, '$', 1))) {
						fatal("Old/incompatible password detected for user %s. Please update this user's password before migrating.", $user);
					}
					fatal('Unknown or unsupported shadow type: %s', substr($passwd, 1, $pos));
				}
				$success = $s->getSiteFunctionInterceptor()->user_add($newuser, $passwd, $gecos, (float)array_get($data, 'diskquota', 0)/1024/1024,
					[
						'password' => 'crypted',
						'ftp'      => $enabled,
						'imap'     => $enabled,
						'smtp'     => $enabled,
						'cp'       => $enabled,
						'dav'      => $dav
					]
				);
				if (!$success) {
					fatal("Failed to create user `%s'", $newuser);
				}
				info('Created user %s, (old/new uid: %d/%d)',
					$newuser,
					$uid,
					$s->getSiteFunctionInterceptor()->user_get_uid_from_username($newuser)
				);
			}
			$pwd = $s->getSiteFunctionInterceptor()->user_getpwnam($newuser);

			$this->createEmail($s, (string)$user, $domain, $pwd['uid']);

			/**
			 * @XXX cPanel does things screwy by merging mail for all users into a single spool.
			 *      Pluck the home directories out for each and remove from account admin home
			 *      shell contains the prefixed home for the user; gecos AFAIK is empty
			 */
			$adminHome = '/home/' . $this->bill->getNote(UserNote::class)->getOriginalUser();
			$mailBase = $adminHome;
			if (0 === strncmp($shell, '/home', 5)) {
				$mailBase = $shell;
			}
			if (0 !== strpos($home, $mailBase)) {
				warn("Irregular path detected for user `%s'. Expected home base `%s', found `%s' in passwd!",
					$user,
					$home,
					$mailBase
				);
			}
			$maildir = substr($home, \strlen($mailBase));
			if (!file_exists($this->getDirectoryEnumerator()->getPath() . $maildir)) {
				// certain passwd entries are invalid-
				// aaron:x:1055:1053::/home/admin/mail/domain.net/mail/domain.ca/aaron:/home/admin
				// where mail is located in /home/admin/mail/domain.ca/aaron
				$newMaildir = implode(DIRECTORY_SEPARATOR, [
					'',
					'mail',
					$domain,
					$user
				]);
				$chkPath = $this->getDirectoryEnumerator()->getPath() . $newMaildir;
				if (file_exists($chkPath)) {
					$oldMaildir = $maildir;
					$maildir = $newMaildir;
					warn("Garbage home path found in passwd. Using `%(newpath)s' instead of `%(oldpath)s for %(user)s",
					[
						'newpath' => $maildir,
						'oldpath' => $oldMaildir,
						'user'    => $user
					]);
				}
			}
			if (false !== strpos($maildir, '..')) {
				fatal("Corrupt path detected: `%s' for user `%s'", $maildir, $user);
			} else if (false === strpos($maildir, 'mail/')) {
				warn("Failed to locate mail/ in `%s'. Processing anyway for `%s'.", $maildir, $user);
			}
			$this->stream->externalExtract(
				"/homedir$maildir",
				$s->getAccountRoot() . $pwd['home'] . '/Mail',
				$pwd['uid'],
				$s->getAuthContext()->group_id
			);

			if ($this->task->getOption('unsafe')) {
				$this->extractWebmail($s, $user, $domain, $newuser);
			}
		}

		/**
		 * Extract webmail configuration
		 *
		 * @param SiteConfiguration $s
		 * @param string            $user
		 * @param string            $domain
		 * @param string            $newuser
		 */
		private function extractWebmail(SiteConfiguration $s, string $user, string $domain, string $newuser): void
		{
			// SquirrelMail
			$sqmailBase = Webmail::instantiateContexted($s->getAuthContext())->getStorageDirectory('sqmail');
			foreach (['abook', 'pref'] as $ext) {
				$srcpath = $this->getDirectoryEnumerator()->getPath() . "/.sqmaildata/${user}@${domain}.${ext}";
				if (file_exists($srcpath)) {
					$dstpath = "${sqmailBase}/${newuser}@${domain}.${ext}";
					file_put_contents($dstpath, file_get_contents($srcpath))  !== false &&
						chown($dstpath, APACHE_UID) && chgrp($dstpath, APACHE_GID) && chmod($dstpath, 0600);
				}
			}
		}

		private function tryAddTransport(string $hostname, \apnscpFunctionInterceptor $afi): bool
		{
			$parts = $afi->web_split_host($hostname);
			if ($afi->email_transport_exists($hostname)) {
				return true;
			}
			if (!\in_array($parts['domain'], $afi->aliases_list_aliases(), true) &&
				$parts['domain'] !== $afi->common_get_service_value('siteinfo', 'domain'))
			{
				warn("domain `%s' not owned by site - ignoring transport", $parts['domain']);
				return false;
			}
			if (!$afi->email_add_virtual_transport($parts['domain'], $parts['subdomain'])) {
				fatal('Failed to add transport to %s', $hostname);
			}

			return true;
		}

		private function setupDelegatedWhitelist(): bool
		{
			if (!class_exists(Whitelist::class)) {
				return false;
			}
			if (RAMPART_DELEGATED_WHITELIST === 0) {
				// disabled
				return false;
				return false;
			}
			$path = \dirname($this->getDirectoryEnumerator()->getPathname()) . DIRECTORY_SEPARATOR . '.lastlogin';
			if (!file_exists($path)) {
				return false;
			}
			$now = new \DateTime();
			$check = array_cardinal(array_map(static function($line) use ($now) {
				$ip = strtok($line, ' ');
				if (!Ip4::valid($ip) && !Ip6::valid($ip)) {
					return '';
				}
				$date = ltrim((string)strtok('#'));
				if (!$date) {
					// ambiguous or bad line
					return '';
				}
				// ignore entries older than 12 months
				if ((new \DateTime($date))->diff($now)->days > 365) {
					return '';
				}
				return $ip;
			}, file($path, FILE_IGNORE_NEW_LINES|FILE_SKIP_EMPTY_LINES)));
			$max = RAMPART_DELEGATED_WHITELIST === -1 ? null : (int)RAMPART_DELEGATED_WHITELIST;
			$ips = \array_slice($check, 0, $max);
			if ($ips) {
				info('Added IPs to delegated whitelist: %s', implode(', ', $ips));
				$this->bill->set('rampart', 'whitelist', $ips);
			}
			return true;
		}

		/**
		 * Create email inbox
		 *
		 * @param SiteConfiguration $s
		 * @param string            $user
		 * @param string            $domain
		 * @param int               $uid
		 * @return bool
		 * @throws \PostgreSQLError
		 */
		private function createEmail(SiteConfiguration $s, string $user, string $domain, int $uid): bool
		{
			if (!$s->getSiteFunctionInterceptor()->email_transport_exists($domain)) {
				return false;
			}

			// update email map
			if ($s->getSiteFunctionInterceptor()->email_address_exists($user, $domain)) {
				if (!$this->task->getOption('create')) {
					return info('Found duplicate address %(user)@%(host)s in no-create mode - skipping',
						['user' => $user, 'host' => $domain]
					);
				}
				$s->getSiteFunctionInterceptor()->email_delete_mailbox($user, $domain, \Email_Module::MAILBOX_USER);
			}

			return $s->getSiteFunctionInterceptor()->email_add_mailbox($user, $domain, $uid);
		}
	}

