<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2019
	 */

	namespace Opcenter\Migration\Formats\Cpanel\Pathmap;


	use Event\Cardinal;
	use Event\Events;
	use Opcenter\Account\Import;
	use Opcenter\Migration\Formats\Cpanel\ConfigurationBuilder;
	use Opcenter\Migration\Helpers\PathTranslate;
	use Opcenter\Migration\Notes\User;
	use Opcenter\SiteConfiguration;
	use Symfony\Component\Yaml\Yaml;

	/**
	 * Class Userdata
	 *
	 * - addon_domains, sub_domains have flexible docroots
	 * - main_domain must be /home/USER/public_html => /var/www/html
	 * - parked_domain inherit main_domain docroot
	 *
	 * @package Opcenter\Migration\Formats\Cpanel\Pathmap
	 */
	class Userdata extends ConfigurationBuilder
	{
		/**
		 * Parse addon domain configuration
		 *
		 * @TODO add restriction checks for addon domains
		 *
		 * @return bool
		 */
		public function parse(): bool
		{
			$map = $this->getMap();

			$aliases = array_keys($map['addon_domains']) + append_config($map['parked_domains']);
			$this->bill->set('aliases', 'aliases', $aliases);
			Cardinal::register([Import::HOOK_ID, Events::SUCCESS],
				function ($event, SiteConfiguration $p) {
					$map = $this->getMap();
					$afi = $p->getSiteFunctionInterceptor();
					$blanked = [
						\Web_Module::MAIN_DOC_ROOT => 1
					];
					foreach ($map['addon_domains'] as $domain => $path) {
						if ($afi->aliases_domain_exists($domain)) {
							$afi->aliases_remove_domain($domain);
						}
						$path = $this->transformPath($path);
						$newPath = $this->assignDestinationRoot($path) ?? "/var/www/${domain}";

						if (!isset($blanked[$path])) {
							$this->transmigrationWrapper($p, $path, $newPath);
							$blanked[$path] = true;
						}
						// cover cases where addon aliased to main domain but not parked (???)
						$afi->aliases_add_domain($domain, $newPath);
					}

					foreach ($map['parked_domains'] as $domain) {
						if ($afi->aliases_domain_exists($domain)) {
							$afi->aliases_remove_domain($domain);
						}

						$afi->aliases_add_domain($domain, \Web_Module::MAIN_DOC_ROOT);
					}

					foreach ($map['sub_domains'] as $subdomain => $path) {
						$path = $this->transformPath($path);
						$newPath = $this->assignDestinationRoot($path) ?? "/var/www/${subdomain}";

						if (!isset($blanked[$path])) {
							$this->transmigrationWrapper($p, $path, $newPath);
							$blanked[$path] = true;
						}

						if ($afi->web_subdomain_exists($subdomain)) {
							$afi->web_remove_subdomain($subdomain);
						}
						// cover cases where subdomain aliased to main domain
						$afi->web_add_subdomain($subdomain, $newPath);
					}
					$afi->aliases_synchronize_changes();
				});

			return true;
		}

		/**
		 * Open path after transmigration
		 *
		 * @param SiteConfiguration $s
		 * @param string            $src
		 * @param string            $dest
		 */
		private function transmigrationWrapper(SiteConfiguration $s, string $src, string $dest) {
			$this->transmigratePath($s->getSiteFunctionInterceptor(), $src, $dest);
			$this->convertPath($s, $src, $dest);
			$path = $s->getAccountRoot() . $dest;
			// set o+x
			chmod($path, fileperms($path) | 0001);
		}

		private function getMap(): array
		{
			if (!file_exists($path = $this->getDirectoryEnumerator()->getPathname())) {
				return [];
			}

			$yaml = $this->getDomainConfiguration('main');
			// verify the primary domain points to /home/USERNAME/public_html
			$this->assertPrimaryDomainConsistency($yaml['main_domain']);
			$new = [];
			foreach ($yaml['sub_domains'] as $subdomain) {
				$cfg = $this->getDomainConfiguration($subdomain);
				if (!$cfg) {
					continue;
				}

				$new[$subdomain] = $cfg['documentroot'];
			}
			$yaml['sub_domains'] = $new;
			foreach ($yaml['addon_domains'] as $addon => $referenceSubdomain) {
				$path = $yaml['sub_domains'][$referenceSubdomain] ?? "/var/www/${addon}";
				$yaml['addon_domains'][$addon] = $path;
			}
			return $yaml;
		}

		/**
		 * Get domain configuration
		 *
		 * @param string $domain
		 * @return array
		 */
		private function getDomainConfiguration(string $domain): array
		{
			$path = \dirname($this->getDirectoryEnumerator()->getPathname()) . DIRECTORY_SEPARATOR . $domain;
			if (!file_exists($path)) {
				warn("Bug? Missing hostname configuration for `%s' in %s", $domain, $path);
				return [];
			}
			return (array)Yaml::parseFile($path);
		}

		private function assertPrimaryDomainConsistency(string $domain): void
		{
			$cfg = $this->getDomainConfiguration($domain);
			if ( !($docroot = array_get($cfg, 'documentroot')) ) {
				fatal("Failed assertion: document root for `%s' does not exist in configuration", $domain);
			}
			$adminHome = '/home/' . $this->bill->getNote(User::class)->getOriginalUser() . '/public_html';
			if ($adminHome !== $docroot) {
				fatal("Docroot for `%s' is `%s'. Expected `%s'.", $domain, $docroot, $adminHome);
			}
		}

		/**
		 * Transform source docroot path
		 *
		 * @param string $path
		 * @return string
		 */
		private function transformPath(string $path): string
		{
			$old = $this->bill->getNote(User::class)->getOriginalUser();
			$new = $this->bill->getNote(User::class)->getNewUser();
			if ($old !== $new && 0 === strpos($path, "/home/${old}/")) {
				$path = "/home/${new}/" . substr($path, \strlen("/home/${old}/"));
			}
			if (0 === strpos($path, "/home/${new}/public_html")) {
				/**
				 * /home/XXX/public_html gets remapped to /var/www/html in Homedir::class
				 * which preempts all other components to populate restored filesystem
				 */
				$path = '/var/www/html' . substr($path, \strlen("/home/${new}/public_html"));
			}

			return $path;
		}

		/**
		 * Suggest a document root from old path
		 *
		 * Reorganize addon domains beneath /var/www
		 *
		 * @param string $path
		 * @return string|null
		 */
		private function assignDestinationRoot(string $path): ?string
		{
			$old = $this->bill->getNote(User::class)->getOriginalUser();
			$new = $this->bill->getNote(User::class)->getNewUser();
			if ($old !== $new && 0 === strpos($path, "/home/${old}/")) {
				$path = "/home/${new}/" . substr($path, \strlen("/home/${old}/"));
			}

			if (0 === strpos($path, "/home/${new}/public_html")) {
				/**
				 * /home/XXX/public_html gets remapped to /var/www/html in Homedir::class
				 * which preempts all other components to populate restored filesystem
				 */
				$path = '/var/www/' . substr($path, \strlen("/home/${new}/public_html"));
			} else if (0 === strpos($path, "/home/${new}/")) {
				$path = '/var/www/' . substr($path, \strlen("/home/${new}/"));
			} else if (0 !== strpos($path, \Web_Module::MAIN_DOC_ROOT)) {
				// ????
				warn("Path `%s' does not exist in user home - autogenerating path", $path);

				return null;
			}

			// translate global subdomains
			return str_replace('*', '_', $path);
		}

	}

