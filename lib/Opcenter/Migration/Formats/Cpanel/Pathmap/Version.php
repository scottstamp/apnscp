<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2019
	 */

	namespace Opcenter\Migration\Formats\Cpanel\Pathmap;


	use Opcenter\Migration\Formats\Cpanel\ConfigurationBuilder;

	class Version extends ConfigurationBuilder
	{
		const ACCEPTABLE_BACKUP_VERSIONS = [4];
		const DEPENDENCY_MAP = [];
		public function parse(): bool
		{
			$path = $this->getReader()->getPathname();
			if (!file_exists($path)) {
				return error('version file not found, archive corrupted?');
			}
			if (!preg_match('/^archive version: (\d+)$/m', file_get_contents($path), $matches)) {
				return error('Corrupt or missing archive version in backup');
			}
			if (!\in_array($matches[1], static::ACCEPTABLE_BACKUP_VERSIONS, false)) {
				return error("Archive version `%s' is not supported", $matches[1]);
			}

			$note = new \Opcenter\Migration\Notes\Version($matches[1]);
			$this->bill->addNote($note);
			return true;
		}
	}

