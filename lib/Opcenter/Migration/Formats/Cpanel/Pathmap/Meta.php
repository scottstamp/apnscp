<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2019
	 */

	namespace Opcenter\Migration\Formats\Cpanel\Pathmap;


	use Opcenter\Migration\Formats\Cpanel\ConfigurationBuilder;
	use Opcenter\Migration\Note;
	use Opcenter\Migration\Notes\NetInfo;
	use Opcenter\Migration\Notes\User;

	class Meta extends ConfigurationBuilder
	{
		public function parse(): bool
		{
			$this->setHostname();
			$this->setHomedir();

			return true;
		}

		private function setHostname(): bool {
			$path = $this->getDirectoryEnumerator()->getPath() . DIRECTORY_SEPARATOR . 'hostname';
			if (!file_exists($path) || !($hostname = trim(file_get_contents($path)))) {
				return false;
			}
			if (!preg_match(\Regex::DOMAIN, $hostname)) {
				fatal("Invalid hostname `%s' detected in meta/hostname", $hostname);
			}
			$note = $this->bill->getNote(Note::symbolic(NetInfo::class));
			$note->setHostname($hostname);
			return true;
		}

		private function setHomedir(): bool {
			$path = $this->getDirectoryEnumerator()->getPath() . DIRECTORY_SEPARATOR . 'homedir_paths';
			$home = '/home/' . $this->bill->getNote(User::class)->getOriginalUser();
			if (file_exists($path) && ($dir = trim(file_get_contents($path)))) {
				$home = $dir;
			}
			$this->bill->addNote(new \Opcenter\Migration\Notes\Homedir($home));
			return true;
		}
	}

