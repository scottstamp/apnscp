<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2019
	 */

	namespace Opcenter\Migration\Notes;

	use Opcenter\Migration\Note;

	class MailTransports extends Note
	{
		protected $lookup = [];

		public function revokeTransport(string $domain): self
		{
			$this->lookup[$domain] = 1;

			return $this;
		}

		public function revoked(string $domain): bool
		{
			return \array_key_exists($domain, $this->lookup);
		}

		public function get(): array
		{
			return array_keys($this->lookup);
		}
	}