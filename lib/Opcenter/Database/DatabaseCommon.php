<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace Opcenter\Database;

	use Opcenter\Map;

	abstract class DatabaseCommon
	{
		const PREFIX_MAP = 'sql.prefixmap';
		const USER_MAP = 'sql.usermap';

		protected static $svc = null;

		/**
		 * Database type includes restricting by host
		 *
		 * @return bool
		 */
		public static function hasHosts(): bool
		{
			return false;
		}

		public static function uninstallPostHook(string $user, string $host): bool
		{
			return true;
		}

		public static function deleteMainUser(string $user, string $host = null)
		{
			return static::deleteUser($user, $host);
		}

		abstract public static function deleteUser(string $user, string $host = null);

		/**
		 * Suggest a database prefix
		 *
		 * @param string      $base
		 * @param string|null $check key to soft validate against
		 * @return null|string
		 * @throws \Exception
		 */
		public static function suggestPrefix(string $base, string $check = null): ?string
		{
			$base = preg_replace('/[\W_]/', '', $base);
			if ($base[-1] !== '_') {
				$base .= '_';
			}
			$baselen = \strlen($base);
			// mysql has a
			$n = min($baselen, 16, MySQL::fieldLength('user') - 3);

			for ($i = 4; $i < $n; $i++) {
				if (!isset($base[$i])) {
					$base[$i] .= \chr(random_int(\ord('a'), \ord('z')));
				}
				$prefix = substr($base, 0, $i);
				if (!self::prefixExists($prefix . '_', $check)) {
					return $prefix . '_';
				}
			}

			return null;
		}

		/**
		 * Database prefix in use
		 *
		 * @param string      $prefix
		 * @param string|null $check
		 * @return bool
		 */
		public static function prefixExists(string $prefix, string $check = null): bool
		{
			if (!file_exists(Map::home() . '/' . static::PREFIX_MAP)) {
				return false;
			}

			$key = Map::read(static::PREFIX_MAP)[$prefix];

			return $key && (null === $check || $check !== $key);
		}

		/**
		 * @param string $what
		 * @param string $old
		 * @param string $new
		 * @return string
		 */
		public static function convertPrefix(string $what, ?string $old, string $new): string
		{
			if ($new[-1] !== '_') {
				warn("Prefix `%s' not delimited by _", $new);
			}
			if ($old === null || 0 !== strpos($what, $old)) {
				return $what;
			}

			return $new . substr($what, \strlen($old));
		}

		/**
		 * Canonical name of database type
		 *
		 * @param string $name
		 * @return string
		 */
		public static function canonicalizeBrand(string $name): string
		{
			switch ($name) {
				case 'mysql':
				case 'Mysql':
					return 'MySQL';
				case 'pgsql':
				case 'Postgresql':
					return 'PostgreSQL';
				default:
					return $name;
			}

		}

		public static function reserve(string $prefix, string $site): bool
		{

		}
	}