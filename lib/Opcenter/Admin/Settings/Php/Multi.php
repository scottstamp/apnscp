<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, January 2020
	 */

	namespace Opcenter\Admin\Settings\Php;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Http\Php\Fpm\MultiPhp;
	use Opcenter\Versioning;

	class Multi implements SettingsInterface
	{
		public function set($val): bool
		{
			$sysver = (string)(new Config())->offsetGet('system_php_version');
			$versions = $this->get();

			if (!\is_array($val) || isset($val[0])) {
				// [7.3,7.2]
				$iterator = array_fill_keys((array)$val, 'native');
			} else {
				// 7.3: type
				$iterator = (array)$val;
			}

			$cfg = new Bootstrapper\Config();

			$persisted = (array)($cfg['php_multiphp'] ?? []);
			$systemVersion = (new Version())->get();
			foreach ($iterator as $version => $type) {
				if (!$version) {
					return error("Missing version specified in multiPHP setting - is version specified correctly?");
				}
				if ($type === 'system') {
					if (Versioning::asMinor((string)$sysver) === Versioning::asMinor((string)$version)) {
						// no-op
						continue;
					}

					return error(
						'Cannot declare %(fakever)s system version. System version %(realver)s already exists',
						['fakever' => $version, 'realver' => $sysver]
					);
				}
				if ($version === $systemVersion) {
					return error(
						"Version `%s' exists as system version. Cannot redeclare this as a multiPHP build.",
						$version
					);
				}
				$version = str_replace(',', '.', (string)$version);
				$version = false === strpos($version, '.') ? Versioning::asMinor($version) : $version;
				if ($type === 'disabled') {
					$type = false;
				}

				if ($type === false || $type === 'package') {
					// removed, no direct cleanup yet
					if (false !== ($pos = array_search($version, $persisted, false))) {
						if (in_array($version, MultiPhp::listNative(), true) && !MultiPhp::removeNative($version)) {
							continue;
						}
						unset($persisted[$pos]);
					}
					continue;
				}

				if ($type === 'package') {
					warn("Package PHP versions must be installed manually. See docs/admin/PHP-FPM.md. Skipping `%s'.",
						$version);
					continue;
				}

				if (!in_array($version, $persisted, true)) {
					$persisted[] = $version;
				}

				if ($type === ($hasVer = array_get($versions, $version))) {
					// version no-op
					continue;
				}

				if ($hasVer === 'package') {
					warn("Converting `%s' from package to native build requires manual reconfiguration", $version);
				}

				Version::versionCheck((string)$version);
				Bootstrapper::run('php/build-from-source', 'php/install-pecl-module', ['multiphp_build' => true, 'php_version' => (string)$version]);
			}

			$cfg['php_multiphp'] = array_values($persisted);
			$cfg->sync();

			return true;
		}

		public function get()
		{
			return MultiPhp::list();
		}

		public function getHelp(): string
		{
			return 'Add native PHP builds';
		}

		public function getValues()
		{
			return 'string|array';
		}

		public function getDefault()
		{
			return [(string)(new Config())->offsetGet('system_php_version') => 'system'];
		}
	}