<?php declare(strict_types=1);

	namespace Opcenter\Admin\Settings\VirusScanner;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Net\IpCommon;

	class RemoteScan implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val == $this->get()) {
				// no need to set the value again
				return true;
			}
			$port = null;
			if (false !== ($pos = strrpos((string)$val, ':'))) {
				$port = (int)substr($val, $pos+1);
				$val = substr($val, 0, $pos);
			}
			if (!$val) {
				$val = (bool)$val;
			} else if (preg_match(\Regex::DOMAIN, $val)) {
				if (!\Net_Gethost::gethostbyname_t($val)) {
					warn("Cannot resolve `%s' to an IP address", $val);
				}
			} else if (!IpCommon::valid($val)) {
				return error("Address `%s' is invalid", $val);
			}

			$cfg = new Bootstrapper\Config();
			$cfg['clamav_client_only'] = (bool)$val;
			$cfg['clamav_clamd_tcp_addr'] = $val ?: false;
			if ($port) {
				$cfg['clamav_clamd_tcp_port'] = $port;
			} else {
				unset($cfg['clamav_clamd_tcp_port']);
			}
			unset($cfg);

			if ($val && !(new Enabled())->get()) {
				warn("Virus-scanning support disabled, toggling on");
				return (new Enabled())->set(true);
			}

			Bootstrapper::run('clamav/setup', 'apache/modsecurity');

			return true;
		}

		public function get()
		{
			$cfg = new Bootstrapper\Config();
			if (!$cfg['clamav_client_only'] || !$cfg['clamav_enabled']) {
				return false;
			}
			$host = $cfg['clamav_clamd_tcp_addr'] ?? '127.0.0.1';
			if (isset($cfg['clamav_clamd_tcp_port'])) {
				$host .= ':' . $cfg['clamav_clamd_tcp_port'];
			}

			return $host;
		}

		public function getHelp(): string
		{
			return 'Remote scanner address';
		}

		public function getValues()
		{
			return 'string';
		}

		public function getDefault()
		{
			return false;
		}

	}
