<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2018
	 */

	namespace Opcenter\Argos\Backends;

	use Opcenter\Argos\Backend;

	class Slack extends Backend
	{
		protected $token;
		protected $recipient;

		public function getAuthentication()
		{
			return ['token' => $this['token'], 'recipient' => $this['recipient']];
		}

		public function setAuthentication(...$vars): bool
		{
			if (isset($vars[1])) {
				// config_set argos.auth slacky TOKEN RECIPIENT
				$vars = ['token' => $vars[0], 'recipient' => $vars[1]];
			} else {
				// config_set argos.auth slacky '[token:TOKEN,recipient:RECIPIENT]'
				$vars = $vars[0];
			}
			if (!isset($vars['token'])) {
				return error("Missing authentication item `token'");
			}
			if (!isset($vars['recipient'])) {
				return error("Missing authentication item `recipient'");
			}

			$this['token'] = $vars['token'];
			$this['recipient'] = $vars['recipient'];

			return true;
		}
	}