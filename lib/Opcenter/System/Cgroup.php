<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, February 2018
	 */

	declare(strict_types=1);

	namespace Opcenter\System;

	use Opcenter\Filesystem;
	use Opcenter\Filesystem\Mount;
	use Opcenter\System\Cgroup\Attributes\Blkio\Weight;
	use Opcenter\System\Cgroup\Controller;
	use Opcenter\System\Cgroup\Controllers\Blkio;
	use Opcenter\System\Cgroup\Group;
	use Util_Process;

	class Cgroup
	{
		const CGROUP_HOME = CGROUP_HOME;
		const CGROUP_SERVER_CONFIG = '/etc/cgconfig.conf';
		const CGROUP_SITE_CONFIG = '/etc/cgrules.conf';
		const CGROUP_CONTROLLERS = CGROUP_CONTROLLERS;

		/**
		 * Get CPU usage for controller
		 *
		 * @param string $group group name
		 * @return array
		 */
		public static function cpu_usage(?string $group): array
		{
			$stats = array(
				'used'     => null,
				'system'   => null,
				'free'     => null,
				'limit'    => null,
				'user'     => null,
				'procs'    => [],
				'maxprocs' => null
			);
			$path = self::CGROUP_HOME . '/' . 'cpuacct/' . $group;
			if (!file_exists($path) || !is_readable($path . '/cgroup.procs')) {
				return $stats;
			}

			// nano to seconds
			$stats['used'] = (int)file_get_contents($path . '/cpuacct.usage')/1e9;
			$tmp = file($path . '/cpuacct.stat', FILE_IGNORE_NEW_LINES);
			$stats['user'] = (float)substr($tmp[0], strpos($tmp[0], ' ') + 1) / CPU_CLK_TCK;
			$stats['system'] = (float)substr($tmp[1], strpos($tmp[1], ' ') + 1) / CPU_CLK_TCK;
			// CPU counters are delta counters, which requires knowing the previous val
			$stats['free'] = null;
			$stats['procs'] = array_map(static function ($a) {
				return (int)$a;
			},
				file($path . '/cgroup.procs', FILE_IGNORE_NEW_LINES)
			);

			return $stats;
		}

		public static function io_usage(?string $group): array
		{
			static $prefix;

			$controller = Controller::make(new Group($group), 'blkio');
			$path = $controller->getPath();
			$io = [
				'bw-read' => 0,
				'bw-write' => 0,
				'bw-rlimit' => 0,
				'bw-wlimit' => 0,
				'iops-read' => 0,
				'iops-write' => 0,
				'iops-rlimit' => 0,
				'iops-wlimit' => 0
			];
			if (!file_exists($path)) {
				return $io;
			}

			if (!isset($prefix)) {
				// @XXX assume all filesystems are mounted using
				// same scheduler and mountpoint
				// @TODO update for multimount platforms

				$namespace = version_compare(posix_uname()['release'], '4.0', '>=') ? '.throttle' : '';
				$elevator = Mount::getScheduler(Mount::getBlockFromPath(FILESYSTEM_VIRTBASE));
				$prefix = $path . '/blkio' . ($elevator === 'bfq' ? '.bfq' : $namespace);
			}

			$ioStats = array_build(file($prefix . '.io_serviced', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES), static function ($k, $v) {
				strtok($v, ' ');
				if (false === strpos($v, ':')) {
					return null;
				}
				$statKey = strtolower(strtok(' '));
				$statVal = strtok(' ');
				return ['iops-' . $statKey, (int)$statVal];
			});
			$ioStats['iops-rlimit'] = (int)strstr(file_get_contents($path . '/blkio.throttle.read_iops_device'), ' ');
			$ioStats['iops-wlimit'] = (int)strstr(file_get_contents($path . '/blkio.throttle.write_iops_device'), ' ');

			$bwStats = array_build(file($prefix . '.io_service_bytes', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES),
				static function ($k, $v) {
					strtok($v, ' ');
					if (false === strpos($v, ':')) {
						return null;
					}
					$statKey = strtolower(strtok(' '));
					$statVal = strtok(' ');

					return ['bw-' . $statKey, (int)$statVal];

				});
			$bwStats['bw-rlimit'] = (int)strstr(file_get_contents($path . '/blkio.throttle.read_bps_device'), ' ');
			$bwStats['bw-wlimit'] = (int)strstr(file_get_contents($path . '/blkio.throttle.write_bps_device'), ' ');

			return $bwStats + $ioStats;
		}

		public static function pid_usage(?string $group): array
		{
			$controller = Controller::make(new Group($group), 'pids');
			$path = $controller->getPath();
			$pids = [
				'max'     => -1,
				'current' => 0
			];
			if (!file_exists($path)) {
				return $pids;
			}

			return [
				'current' => file_exists($path . '/pids.current') ? \count(file($path . '/pids.current',
					FILE_IGNORE_NEW_LINES)) : 0,
				// technically it's pid_max from /proc/sys/kernel/pid_max
				'max'     => file_exists($path . '/pids.max') ? (int)file_get_contents($path . '/pids.max') : 4096
			];
		}

		/**
		 * Get memory usage from controller
		 *
		 * @deprecated
		 * @param string $group group name
		 * @return array
		 */
		public static function get_memory(?string $group): array
		{
			deprecated_func('Use memory_usage');
			return static::memory_usage($group);
		}

		/**
		 * Get memory usage from controller
		 *
		 * @param string $group group name
		 * @return array
		 */
		public static function memory_usage(?string $group): array
		{
			$stats = array(
				'used'     => 0,
				'peak'     => 0,
				'free'     => null,
				'limit'    => null,
				'procs'    => array(),
				'detailed' => array(),
				'oom'      => null
			);
			$path = self::CGROUP_HOME . '/' . 'memory/' . $group;
			if (!file_exists($path) || !is_readable($path . '/cgroup.procs')) {
				return $stats;
			}
			$stats['used'] = (int)file_get_contents($path . '/memory.usage_in_bytes');
			$stats['peak'] = (int)file_get_contents($path . '/memory.max_usage_in_bytes');
			$stats['limit'] = (int)file_get_contents($path . '/memory.limit_in_bytes');
			$stats['oom'] = (int)file_get_contents($path . '/memory.failcnt');
			$stats['procs'] = array_map(static function ($a) {
				return (int)$a;
			}, file($path . '/cgroup.procs', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES));
			/**
			 * Odd issue on root controller. Normally used < peak < limit, except
			 * that peak < used < limit... cap
			 */
			if (!$group) {
				$stats['peak'] = max($stats['peak'], $stats['used']);
			}

			return $stats;
		}

		/**
		 * Create a new group within a controller
		 *
		 * @param Group $group
		 * @return bool
		 */
		public static function create(Group $group): bool
		{
			foreach ($group->getControllers() as $controller) {
				$cgpath = self::CGROUP_HOME . '/' . $controller;
				if (!file_exists($cgpath)) {
					return error("cgroup path `%s' doesn't exist",
						$cgpath);
				}
				$cgpath .= '/' . $group;
				if (file_exists($cgpath) && !$controller->exists()) {
					// botched creation
					rmdir($cgpath);
				}
				if (!self::addConfiguration($group)) {
					warn('failed to populate cgroup configuration!');
				}
			}

			return true;
		}

		/**
		 * Add cgroup configuration
		 *
		 * @param Group $group
		 * @return bool|void
		 */
		protected static function addConfiguration(Group $group)
		{
			$config = self::CGROUP_SERVER_CONFIG;
			if (!file_exists($config)) {
				if (!is_dir(\dirname($config)) && !mkdir($concurrentDirectory = \dirname($config)) && !is_dir($concurrentDirectory)) {
					fatal('Directory "%s" was not created', $concurrentDirectory);
				}
				touch($config);
			}

			$fp = fopen($config, 'r+');
			$blocked = false;
			for ($i = 0; $i < 10; $i++) {
				if (flock($fp, LOCK_EX | LOCK_NB, $blocked)) {
					break;
				}
				sleep(2);
			}
			if ($blocked) {
				fclose($fp);

				return error("failed to lock %s for `%s'", self::CGROUP_SERVER_CONFIG, $group);
			}
			$search = 'group ' . $group . ' ';
			$found = false;
			$cgconf = $group->build();
			$lines = [];
			while (false !== ($line = fgets($fp))) {
				if (0 === strpos($line, $search)) {
					if (trim($line) === $cgconf) {
						$found = true;
						break;
					}
					continue;
				}
				$lines[] = rtrim($line, "\n");
			}
			if (!$found) {
				$lines[] = $cgconf;
				ftruncate($fp, 0);
				rewind($fp);
				fwrite($fp, implode("\n", $lines) . "\n");
			}
			flock($fp, LOCK_UN);

			return fclose($fp);
		}

		/**
		 * Delete a group from a controller
		 * s
		 *
		 * @param Group      $group
		 * @param Controller $controller
		 * @return bool
		 */
		public static function delete(Group $group, Controller $controller): bool
		{
			$cgpath = self::CGROUP_HOME . '/' . $controller;
			if (!file_exists($cgpath)) {
				return error("cgroup controller `%s' doesn't exist",
					$cgpath);
			}
			self::removeConfiguration($group);
			if (!static::exists($controller, $group)) {
				return true;
			}

			$ret = Util_Process::exec('cgdelete -r %(controller)s:%(group)s',
				['controller' => $controller, 'group' => $group]);
			self::removeConfiguration($group);

			return $ret['success'];
		}

		/**
		 * Remove group from cgroup configuration
		 *
		 * @param Group      $group      group name
		 * @param Controller $controller optional group (not implemented)
		 * @return bool
		 */
		public static function removeConfiguration(Group $group, Controller $controller = null)
		{
			$config = self::CGROUP_SERVER_CONFIG;
			if (!file_exists($config)) {
				return false;
			}
			$new = array();
			$fp = fopen($config, 'r+');
			if (!flock($fp, LOCK_EX)) {
				fclose($fp);

				return error("failed to remove cg configuration `%s'", $group);
			}
			$search = 'group ' . $group . ' ';
			while (false !== ($line = fgets($fp))) {
				if (0 === strpos($line, $search)) {
					continue;
				}
				$new[] = rtrim($line);
			}
			ftruncate($fp, 0);
			rewind($fp);
			fwrite($fp, implode("\n", $new) . "\n");
			flock($fp, LOCK_UN);

			return fclose($fp);
		}

		/**
		 * Group exists within controller
		 *
		 * @param string $controller
		 * @param string $group
		 * @return bool
		 */
		public static function exists(Controller $controller, Group $group = null): bool
		{
			$path = self::CGROUP_HOME . DIRECTORY_SEPARATOR .
				$controller . DIRECTORY_SEPARATOR . (string)$group . '/tasks';

			return file_exists($path);
		}

		/**
		 * Charge a process
		 *
		 * @param Group    $group
		 * @param int|null $pid
		 * @return bool
		 */
		public static function charge(Group $group, int $pid = null): bool
		{
			$ret = true;
			foreach (static::getControllers() as $controller) {
				$path = CGROUP_HOME . "/${controller}/${group}/tasks";
				if (!file_exists($path)) {
					continue;
				}
				$ret &= file_put_contents($path, $pid ?? posix_getpid(), FILE_APPEND) > 0;
			}

			return (bool)$ret;
		}

		/**
		 * Get all configured controllers
		 *
		 * @return array
		 */
		public static function getControllers(): array
		{
			return self::CGROUP_CONTROLLERS;
		}

		/**
		 * Get controller class name from cgroup service class
		 *
		 * @param string $svc
		 * @return string|null
		 */
		public static function resolveParameterController(string $svc): ?string
		{
			static $hash = [];
			if (isset($hash[$svc])) {
				return $hash[$svc];
			}

			$found = null;
			foreach (self::getControllers() as $controller) {
				$controller = \Opcenter\System\Cgroup\Controller::make(new Group(null), $controller);
				if ($controller->hasParameter($svc)) {
					$found = get_class($controller);
					break;
				}
			}

			return $hash[$svc] = $found;
		}

		/**
		 * Mount all cgroup controllers into shared slice
		 *
		 * @return bool
		 */
		public static function mountAll(): bool
		{
			foreach (self::getControllers() as $controller) {
				if ($controller === 'cpu') {
					// special case merged controller
					$controller = 'cpu,cpuacct';
				} else if ($controller === 'cpuacct') {
					continue;
				}
				// Ideally applied against /sys/fs/cgroup with /.socket/cgroup
				// a slave to /sys/fs/cgroup. File descriptor woes in 3.10.x kernels
				// in C7 prevent filesystem from flushing fully on delete requiring
				// a second cgroup mount into /.socket and working exclusively off that layer
				$path = FILESYSTEM_SHARED . "/cgroup/${controller}";
				if (Mount::mounted($path)) {
					continue;
				}

				if (!Mount::mount($path, 'cgroup',
					[
						'noexec'    => true,
						'nosuid'    => true,
						'nodev'     => true,
						'rw'        => true,
						$controller => true
					])) {
					warn("Failed to mount cgroup controller `%s'", $controller);

					return false;
				}
			}

			return true;
		}

		/**
		 * Unmount all cgroup controllers from shared slice
		 *
		 * @return bool
		 */
		public static function unmountAll(): bool
		{
			if (null === ($pattern = static::controllerPathPattern())) {
				return true;
			}

			return Mount::unmount(FILESYSTEM_SHARED . "/cgroup/${pattern}");
		}

		/**
		 * Get active controllers filtered, merging same-path controllers
		 *
		 * @return array
		 */
		public static function activeControllers(): array
		{
			return array_filter(
				array_merge(array_diff(self::getControllers(), ['cpu', 'cpuacct']), ['cpu,cpuacct']),
				static function ($c) {
					return Mount::mounted(self::CGROUP_HOME . "/${c}");
				}
			);
		}

		/**
		 * Get glob-style pattern for all controllers
		 *
		 * @return string|null
		 */
		public static function controllerPathPattern(): ?string
		{
			if (!($controllers = static::activeControllers())) {
				return null;
			}

			if (\count($controllers) === 1) {
				return implode('', $controllers);
			}

			return '{' . implode(',', str_replace(',', '\\,', $controllers)) . '}';
		}

		public static function version(): ?int
		{
			if (!is_dir(self::CGROUP_HOME)) {
				return null;
			}

			$mount = Mount::getFilesystemType(self::CGROUP_HOME);
			if (null === $mount) {
				return null;
			}

			return $mount === 'cgroup2' ? 2 : 1;
		}
	}