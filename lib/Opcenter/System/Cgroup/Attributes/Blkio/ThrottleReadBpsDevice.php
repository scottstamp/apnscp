<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, August 2019
 */

	namespace Opcenter\System\Cgroup\Attributes\Blkio;

	use Opcenter\Filesystem\Mount;
	use Opcenter\System\Cgroup\Attributes\BaseAttribute;

	class ThrottleReadBpsDevice extends BaseAttribute
	{
		use \ContextableTrait;

		protected $path = FILESYSTEM_VIRTBASE;
		static $pathMap = [];

		public function getValue()
		{
			static $pathMap = [];
			$path = $this->getAuthContext()->domain_shadow_path() ?: FILESYSTEM_VIRTBASE;
			$majmin = $this->majminFromPath($path);
			$val = (int)parent::getValue();
			if (static::class === ThrottleReadBpsDevice::class || static::class === ThrottleWriteBpsDevice::class) {
				$val *= 1024*1024;
			}
			return $majmin . ' ' . $val;
		}

		public function __toString(): string
		{
			$str = parent::__toString();
			if (false === ($pos = strpos($str, '_'))) {
				return $str;
			}
			return substr_replace($str, '.', $pos, 1);
		}

		/**
		 * Get major:minor device from path
		 *
		 * @param string $path
		 * @return string
		 */
		protected function majminFromPath(string $path): string
		{
			// @XXX fails if device mount-point changes, add option to clean?
			if (!isset(static::$pathMap[$path])) {
				$block = '/dev/' . Mount::getBlockFromDevice(implode(':', Mount::getDeviceFromPath($path)));
				static::$pathMap[$path] = implode(':', Mount::getDeviceId($block));
			}
			return static::$pathMap[$path];
		}


	}