<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, December 2019
 */

namespace Opcenter\Http\Php\Fpm;

use GuzzleHttp\Exception\RequestException;
use HTTP\SelfReferential;
use Opcenter\Contracts\VirtualizedContextable;

class CacheInspector implements  VirtualizedContextable
{
	use \ContextableTrait {
		instantiateContexted as instantiateContextedReal;
	}

	protected $hostname;
	/**
	 * @var string
	 */
	protected $file;
	/**
	 * @var string
	 */
	protected $ip;

	private function __construct(string $ip, string $hostname)
	{
		$this->hostname = $hostname;
		$this->ip = $ip;
	}

	public function __destruct()
	{
		$this->clean();
	}

	protected function clean(): void
	{
		if (!$this->file || !file_exists($this->file)) {
			return;
		}

		unlink($this->file);
	}

	public static function instantiateContexted(\Auth_Info_User $context, ...$ctor): self
	{
		return static::instantiateContextedReal($context, ...$ctor);
	}

	public function readFromPath(string $path): ?array
	{
		$path = realpath($this->getAuthContext()->domain_fs_path($path));
		if (0 !== strpos($path, $this->getAuthContext()->domain_fs_path())) {
			fatal('Bogus path detected');
		}
		$this->file = tempnam($path, 'cp-perf-check');

		file_put_contents($this->file, file_get_contents(resource_path('storehouse/php/opcache-stat-dump.php')));
		if (is_link($this->file . '.php') || !rename($this->file, $this->file . '.php') || !chmod($this->file .= '.php', 0644)) {
			fatal('Failed to rename cache inspector file!');
		}

		try {
			$contents = SelfReferential::instantiateContexted($this->getAuthContext(),
				[$this->hostname, $this->ip])->get(basename($this->file))->getBody()->getContents();
		} catch (RequestException $e) {
			return null;
		} finally {
			$this->clean();
		}

		return (array)\Util_PHP::unserialize($contents);
	}



}