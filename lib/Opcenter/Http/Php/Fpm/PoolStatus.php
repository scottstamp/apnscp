<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, November 2019
 */

namespace Opcenter\Http\Php\Fpm;

use Opcenter\Http\Php\Fpm;

class PoolStatus implements \IteratorAggregate {
	// @var array
	protected $status;
	// @var CacheInspector cache inspector
	protected $metrics;
	// @var string pool name
	private $poolName;

	public function __construct(array $data)
	{
		if (!isset($data['Id'])) {
			fatal('Pool status passed lacks Id field from systemd');
		}
		$this->status = $data;
		$this->poolName = array_get($this->status, 'Id', '');
		if (0 === strpos($this->poolName, Fpm::SERVICE_NAMESPACE)) {
			$this->poolName = basename(substr($this->poolName, \strlen(Fpm::SERVICE_NAMESPACE)), '.service');
		}
	}

	public function getIterator()
	{
		return $this->status;
	}

	/**
	 * Pool is running
	 *
	 * @return bool
	 */
	public function running(): bool
	{
		return array_get($this->status, 'ActiveState') === 'active';
	}

	public function getUser(): int
	{
		return (int)array_get($this->status, 'User');
	}

	/**
	 * Get pool PID
	 *
	 * @return int|null
	 */
	public function getPid(): ?int
	{
		if (!isset($this->status['MainPID'])) {
			return null;
		}
		return (int)$this->status['MainPID'];
	}


	/**
	 * Get pool metrics
	 *
	 * @return array
	 */
	public function getMetrics(): array
	{
		$status = [];
		if (!isset($this->status['StatusText'])) {
			return $status;
		}
		$query = $this->status['StatusText'];
		$type = strtok($query, ':');
		do {
			$val = strtok(',');
			switch ($type = trim(strtolower($type))) {
				case 'processes active';
					$type = 'active';
				case 'idle':
				case 'requests':
				case 'slow':
					$val = (int)$val;
					break;
				case 'traffic':
					if (substr($val, -4) !== '/sec') {
						debug('Odd traffic unit encountered: %s - treating as req/sec', $val);
					}
					$val = (float)$val;
					break;
				default:
					debug('Unknown status type %s: %s, ignoring', $type, $val);
					continue 2;
			}
			$status[$type] = $val;
		} while (false !== ($type = strtok(':')));

		return $status;
	}

	/**
	 * Get cache metrics
	 *
	 * @param \Auth_Info_User $ctx
	 * @return array
	 */
	public function getCacheMetrics(\Auth_Info_User $ctx): CacheSettings
	{
		if (null === $this->metrics) {
			$afi = \apnscpFunctionInterceptor::factory($ctx);
			$this->metrics = new CacheSettings((array)$afi->php_pool_cache_status($this->getName()));
		}

		return $this->metrics;
	}

	/**
	 * Get pool start
	 *
	 * @return int|null null on inactive
	 */
	public function getStart(): ?int
	{
		if (!$this->running()) {
			return null;
		}
		return strtotime($this->status['ActiveEnterTimestamp']);
	}

	/**
	 * Get pool name
	 *
	 * @return string
	 */
	public function getName(): string
	{
		return $this->poolName;
	}
}