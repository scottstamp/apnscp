<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2018
	 */


	namespace Opcenter\Account;

	use Opcenter\Auth\Password;

	/**
	 * Class Ephemeral
	 *
	 * Temporary accounts that vanish out of scope
	 *
	 * @package Opcenter\Account
	 */
	class Ephemeral
	{
		protected $site;
		protected $domain;
		protected $params;
		protected $context;
		protected $afi;
		protected $buffer = [];

		protected function __construct(array $params)
		{
			/**
			 * builtin for faster setup/teardown
			 */
			$configs = [
				'siteinfo.admin_user' => self::random('user'),
				'siteinfo.domain'     => self::random('domain'),
				'dns.provider'        => 'null',
				'mail.provider'       => 'null',
				'ssl.enabled'         => 0
			];
			foreach ($configs as $k => $v) {
				if (array_has($params, $k)) {
					continue;
				}
				array_set($params, $k, $v);
			}

			$this->domain = array_pull($params, 'siteinfo.domain');
			$this->params = $params;
		}

		protected static function random(string $what): string
		{
			switch ($what) {
				case 'domain':
					return Password::generate(16, 'a-z') . '.test';
				case 'user':
				case 'username':
					return Password::generate(16, 'a-z');
				default:
					fatal("Unknown what `%s'", $what);
			}
		}

		/**
		 * Create a new account
		 *
		 * @param array $params dot notation
		 * @return Ephemeral|null
		 */
		public static function create(array $params = []): ?self
		{
			if (!\is_array(current($params))) {
				$tmp = [];
				// passed as dotted notation
				foreach ($params as $k => $v) {
					array_set($tmp, $k, array_pull ($params, $k));
				}
				$params = $tmp;
			}

			return (new static($params))->exec();
		}

		protected function exec(): ?self
		{
			$oldBuffer = \Error_Reporter::get_buffer();
			$account = new Create($this->domain, $this->params);
			$ex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_FATAL, true);
			try {
				if (!$res = $account->exec()) {
					return null;
				}
			} catch (\apnscpException $e) {
				throw $e;
			} finally {
				\Error_Reporter::exception_upgrade($ex);
			}
			register_shutdown_function(function () {
				// account falling out of scope prematurely?
				$this->destroy();
			});
			$this->context = \Auth::context(null, $account->getDomain());
			$this->context->getAccount()->reset($this->context);
			$this->site = $this->context->site;
			$this->buffer = \Error_Reporter::flush_buffer();
			// empty creation log
			\Error_Reporter::set_buffer($oldBuffer);

			return $this;
		}

		/**
		 * Create new invocation proxy
		 *
		 * @return \Module_Skeleton
		 */
		public function getApnscpFunctionInterceptor(): \apnscpFunctionInterceptor
		{
			if (null === $this->afi) {
				$this->afi = \apnscpFunctionInterceptor::factory($this->getContext());
			}

			return $this->afi;
		}

		public function getContext(): \Auth_Info_User
		{
			if (!\apnscpSession::init()->exists($this->context->id)) {
				// regenerate if session ID gets collected
				$this->context = \Auth::context(null, $this->site);
			}

			return $this->context;
		}

		public function getBuffer(): array
		{
			return $this->buffer;
		}

		public function __destruct()
		{
			$this->destroy();
		}

		public function destroy()
		{
			if (!$this->site) {
				return true;
			}
			if (!(new Delete($this->site, ['force' => false]))->exec()) {
				return false;
			}

			$this->site = null;
			return true;
		}
	}