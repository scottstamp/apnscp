<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */

	namespace Opcenter\Account;

	use Event\Cardinal;
	use Event\Contracts\Publisher;
	use Event\Events;
	use Opcenter\Service\ConfigurationContext;
	use Opcenter\SiteConfiguration;

	class Suspend extends DomainOperation implements Publisher
	{
		const HOOK_ID = 'vdsuspend';

		use \FilesystemPathTrait;

		// @var string domain name
		protected $domain;
		// @var string "site" + id
		protected $site;
		// @var int
		protected $site_id;
		// @var bool creation status, used to release site id on failure
		protected $status = false;
		// @var array runtime configuration
		protected array $options;

		/**
		 *
		 * @param string $site
		 * @param array  $options suspension options
		 */
		public function __construct(string $site, array $options = [])
		{
			// avoid firing callbacks on previously registered iterations
			Cardinal::purge();
			$this->site = $site;
			$this->site_id = (int)substr($site, 4);
			$this->domain = \Auth::get_domain_from_site_id($this->site_id);
			$this->options = $options;
			cli_set_process_title(basename($_SERVER['argv'][0]) . " {$this->domain}");
		}

		/**
		 * Run account creation
		 */
		public function exec(): bool
		{
			$instance = new SiteConfiguration($this->site, [], $this->options);
			foreach (array_reverse($instance->getServices()) as $svc) {
				$ctx = new ConfigurationContext($svc, $instance);
				if (!$ctx->getServiceValue($svc, 'enabled')) {
					continue;
				}
				// reverse service values to ensure "version" then "enabled" are final to trigger
				$vars = array_reverse(array_keys($ctx->toArray()));
				foreach ($vars as $var) {
					if (null === ($checker = $ctx->getValidatorClass($var))) {
						// nothing to do
						continue;
					}
					/**
					 * @var $cls \Opcenter\Service\ServiceValidator
					 */
					$cls = new $checker($ctx, $this->site);
					if ($cls instanceof \Opcenter\Service\Contracts\ServiceToggle && !$cls->suspend($instance)) {
						return error("failed to suspend service `%s' on svc var `%s'", $svc, $var);
					}
				}
			}
			$this->runUserHooks($instance);

			Cardinal::fire([SiteConfiguration::HOOK_ID, Events::SUCCESS], $instance);
			\apnscpSession::invalidate_by_site_id($this->site_id);
			\apnscpFunctionInterceptor::factory(\Auth::context(null))->call('admin_kill_site', [$this->site]);

			return true;
		}

		public function getDomain(): string
		{
			return $this->domain;
		}

		public function getEventArgs()
		{
			return [
				'site'    => $this->site,
				'domain'  => $this->domain,
				'options' => []
			];
		}
	}