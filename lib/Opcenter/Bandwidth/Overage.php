<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, October 2019
 */

namespace Opcenter\Bandwidth;

/**
 * Class Overage
 *
 * Per-account overage representation
 *
 * @package Opcenter\Bandwidth
 */
class Overage {
	use \AccountInfoTrait;
	use \apnscpFunctionInterceptorTrait;

	protected $site;
	protected $siteId;
	protected $beginTs;
	protected $endTs;
	/**
	 * @var Site bw handler
	 */
	protected $handler;
	protected $overage;
	protected $threshold;
	protected $inBytes;
	protected $outBytes;

	public function __construct(array $record)
	{
		$this->siteId = $record['site_id'];
		$this->handler = new Site($this->siteId);
		$this->site = 'site' . $this->siteId;
		$this->beginTs = $record['begin_ts'];
		$this->endTs = $this->handler->getCycleEnd();
		$this->overage = $record['over'];
		$this->threshold = $record['threshold'];
		$this->inBytes = $record['in'];
		$this->outBytes = $record['out'];
		$this->setContext(\Auth::context(null, $this->site));
	}

	/**
	 * Get begin as epoch
	 *
	 * @return int unix timestamp (UTC)
	 */
	public function getBegin(): int
	{
		return $this->beginTs;
	}

	/**
	 * Get end as epoch
	 *
	 * @return int unix timestamp (UTC)
	 */
	public function getEnd(): int
	{
		return $this->endTs;
	}

	/**
	 * Get transfer over allowance
	 *
	 * @return int transfer in bytes
	 */
	public function getOverage(): int
	{
		return $this->overage;
	}

	/**
	 * Triggers stopgap suspension
	 *
	 * @see [quota] => bandwidth_stopgap
	 * @return bool
	 */
	public function triggersSuspension(): bool
	{
		if (!BANDWIDTH_STOPGAP) {
			return false;
		}
		return $this->getTotal() > $this->getThreshold() * (BANDWIDTH_STOPGAP / 100);
	}

	/**
	 * Get stopgap threshold
	 *
	 * @return int stopgap amount in bytes
	 */
	public function getStopgapPercentage(): int
	{
		return BANDWIDTH_STOPGAP;
	}

	/**
	 * Consumption triggers notice
	 *
	 * @return bool
	 */
	public function triggersNotice(): bool
	{
		if (!BANDWIDTH_NOTIFY) {
			return false;
		}
		return $this->getTotal() >= $this->getThreshold()*(BANDWIDTH_NOTIFY/100);
	}

	/**
	 * Get bandwidth threshold
	 *
	 * @return int threshold in bytes
	 */
	public function getThreshold(): int
	{
		return $this->threshold;
	}

	/**
	 * Get total consumption
	 *
	 * @return int
	 */
	public function getTotal(): int
	{
		return $this->getInBytes() + $this->getOutBytes();
	}

	/**
	 * Get ingress traffic
	 *
	 * @return int traffic in bytes
	 */
	public function getInBytes(): int
	{
		return $this->inBytes;
	}

	/**
	 * Get egress traffic
	 *
	 * @return int traffic in bytes
	 */
	public function getOutBytes(): int
	{
		return $this->outBytes;
	}

	/**
	 * Get bandwidth handler
	 *
	 * @return Site
	 */
	public function getHandler(): Site
	{
		return $this->handler;
	}

	/**
	 * Get site ID
	 *
	 * @return int
	 */
	public function getSiteId(): int
	{
		return $this->siteId;
	}
}
