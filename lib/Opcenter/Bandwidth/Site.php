<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, September 2019
 */

namespace Opcenter\Bandwidth;

use Carbon\Carbon;
use Opcenter\Account\Activate;
use Opcenter\Account\Suspend;

class Site {
	use \FilesystemPathTrait;
	// @var string marker to indicate site suspended for bandwidth violation
	public const SUSPEND_MARKER = 'bwsuspend';
	// @var string marker to skip bandwidth overages
	public const AMNESTY_MARKER ='bwskip';

	// @var int site ID
	protected $siteId;

	public function __construct(int $site_id)
	{
		if (!\Auth::get_domain_from_site_id($site_id)) {
			fatal('Unknown site ID %d', $site_id);
		}
		$this->siteId = $site_id;
		$this->site = "site${site_id}";
	}

	/**
	 * Get all rollovers
	 *
	 * @return array
	 * @throws \PostgreSQLError
	 */
	public function rollovers(): array
	{
		$query = \PostgreSQL::initialize()->query(
			'SELECT
					EXTRACT(epoch FROM bandwidth_spans.begindate::TIMESTAMPTZ) as begin,
					EXTRACT(epoch FROM bandwidth_spans.enddate::TIMESTAMPTZ)   as end
				FROM
					bandwidth_spans
				WHERE
					bandwidth_spans.site_id = ' . $this->siteId . '
				ORDER BY
					begin ASC');
		$periods = array();
		while (($period = $query->fetch_object()) !== null) {
			$periods[] = array(
				'begin' => (int)$period->begin,
				'end'   => (int)$period->end
			);
		}
		return $periods;
	}

	/**
	 * Get rollover end date of current span
	 *
	 * @return int|null no rollover present
	 * @throws \PostgreSQLError
	 */
	public function getCycleEnd(): ?int
	{
		$db = \PostgreSQL::initialize();
		// @XXX bandwidth is always TZ sensitive as it runs at 12:00 AM local time
		// when working with epochs, UTC, convert first to system TZ
		$query = 'SELECT				
					rollover,
					EXTRACT(epoch FROM begindate::TIMESTAMPTZ)::integer as begin
				FROM
					bandwidth_spans JOIN bandwidth USING (site_id)
				WHERE
					bandwidth_spans.site_id = ' . $this->siteId . ' AND
					enddate IS NULL';
		$rs = $db->query($query);
		if (!$rs->num_rows()) {
			return null;
		}
		// incidentally "+ INTERVAL '1 month'" adds 30 days disrupting February calculations
		$tv = $rs->fetch_object();
		return (new Carbon())->setTimestamp((int)$tv->begin)->day($tv->rollover)->addMonthNoOverflow(1)->getTimestamp();
	}

	/**
	 * Get bandwidth cycle begin date
	 *
	 * @return int cycle begin or time() on error
	 * @throws \PostgreSQLError
	 */
	public function getCycleBegin(): int
	{
		$db = \PostgreSQL::initialize();
		$query = 'SELECT
					EXTRACT(epoch FROM begindate::TIMESTAMPTZ)::integer as begin
				FROM
					bandwidth_spans
				WHERE
					bandwidth_spans.site_id = ' . $this->siteId . ' AND
					enddate IS NULL';
		$rs = $db->query($query);
		if (!$rs->num_rows()) {
			return time();
		}

		$tv = $rs->fetch_object();
		return (int)$tv->begin;
	}

	/**
	 * End rollover for site
	 *
	 * @param int|null $close
	 * @return bool
	 * @throws \PostgreSQLError
	 */
	public function closeRollover(int $close = null): bool
	{
		$db = \PostgreSQL::initialize();
		if (!$close) {
			$close = $this->getCycleEnd();
		}
		$close = (new \DateTime())->setTimestamp($close);

		if (!$close) {
			fatal('Failed to construct date from provided $close argument');
		}

		$query = 'UPDATE bandwidth_spans SET enddate = TO_TIMESTAMP(' .
			$close->setTime(0, 0)->getTimestamp() . ')::DATE WHERE site_id = ' .
			$this->siteId . ' AND enddate IS NULL';
		if (0 === $db->query($query)->affected_rows()) {
			return error('Failed to close rollover period for %d (%s)', $this->siteId, \Auth::get_domain_from_site_id($this->siteId));
		}

		if ($this->suspended()) {
			$this->unsuspend();
		}

		if ($this->amnestied()) {
			$this->revokeAmnesty();
		}

		return true;

	}

	/**
	 * Create new rollover
	 *
	 * @param int $begin same as close date
	 * @return bool
	 */
	public function createRollover(int $begin): bool
	{
		$db = \PostgreSQL::initialize();
		if (!$begin) {
			$begin = time();
		}
		if (null !== $this->getCycleEnd()) {
			return error("Rollover isn't closed yet for %d (%s)",
				$this->siteId,
				\Auth::get_domain_from_site_id($this->siteId)
			);
		}
		$begin = (new \DateTime())->setTimestamp($begin);
		if (!$begin) {
			fatal('Failed to construct date from provided $begin argument');
		}

		$query = 'INSERT INTO bandwidth_spans (site_id, begindate, enddate) 
			VALUES(' .
				$this->siteId . ', 
				TO_TIMESTAMP(' . $begin->setTime(0, 0)->getTimestamp() . ')::DATE,
				NULL)';

		if (0 === $db->query($query)->affected_rows()) {
			return error('Failed to create rollover period for %d (%s)', $this->siteId,
				\Auth::get_domain_from_site_id($this->siteId));
		}

		return true;
	}

	/**
	 * Suspend a site for bandwidth overage
	 *
	 * @return bool
	 */
	public function suspend(): bool
	{
		if ($this->suspended()) {
			return warn('Site %d (%s) already suspended, ignoring request',
				$this->siteId,
				\Auth::get_domain_from_site_id($this->siteId)
			);
		} else if ($this->amnestied()) {
			return false;
		}

		touch($this->domain_info_path(self::SUSPEND_MARKER));
		return (new Suspend($this->site))->exec();
	}

	/**
	 * Remove bandwidth-based suspension
	 *
	 * @return bool
	 */
	public function unsuspend(): bool
	{
		if (!file_exists($this->domain_info_path(self::SUSPEND_MARKER))) {
			return false;
		}
		unlink($this->domain_info_path(self::SUSPEND_MARKER));
		return (new Activate($this->site))->exec();
	}

	/**
	 * Site is suspended for bandwidth overages
	 *
	 * @return bool
	 */
	public function suspended(): bool
	{
		return file_exists($this->domain_info_path(self::SUSPEND_MARKER));
	}

	/**
	 * Grant bandwidth amensty
	 *
	 * @return bool
	 */
	public function amnesty(): bool
	{
		if ($this->suspended()) {
			info('Account in suspended state - unsuspending');
			$this->unsuspend();
		}
		return touch($this->domain_info_path(self::AMNESTY_MARKER));
	}

	/**
	 * Revoke bandwidth amnesty
	 *
	 * @return bool
	 */

	public function revokeAmnesty(): bool
	{
		if (!$this->amnestied()) {
			return false;
		}
		unlink($this->domain_info_path(self::AMNESTY_MARKER));

		return true;
	}

	/**
	 * Account is amnestied for bandwidth overages
	 *
	 * @return bool
	 */
	public function amnestied(): bool
	{
		return file_exists($this->domain_info_path(self::AMNESTY_MARKER));
	}

	/**
	 * Get amnesty end date
	 *
	 * @return int
	 * @throws \PostgreSQLError
	 */
	public function amnestyEnd(): int
	{
		return $this->getCycleEnd();
	}

	/**
	 * Requested site requires rollover
	 *
	 * @return bool
	 */
	public function requiresRollover(): bool
	{
		$query = \PostgreSQL::initialize()->query(
			'SELECT
					1
				FROM
					bandwidth_spans
				WHERE
					bandwidth_spans.site_id = ' . $this->siteId . '
					AND
					enddate IS NULL
					AND
					NOW() >= begindate::TIMESTAMPTZ + INTERVAL \'1 month\''
		);
		return $query->num_rows() > 0;
	}

	/**
	 * Get bandwidth consumed by period
	 *
	 * @param $grouping string group bandwidth by 'month' or 'day'
	 * @return array
	 */
	public function getByComposite(string $grouping = 'month'): array
	{
		if ($grouping === 'month') {
			$query = "SELECT DATE_PART('epoch',bs.begindate) as begin,
				DATE_PART('epoch',bs.enddate) as end,
				sum(bl.in_bytes) as in_bytes,
				sum(bl.out_bytes) as out_bytes
				FROM bandwidth_spans AS bs CROSS JOIN site_totals_daily_view AS bl
				WHERE bl.site_id=" . $this->siteId . '
				AND bl.ts_bucket >= bs.begindate::timestamp
				AND bl.ts_bucket <= bs.enddate
				GROUP BY bs.enddate, bs.begindate
				ORDER BY bs.enddate;';
		} else if ($grouping === 'day') {
			$query = "SELECT DATE_PART('epoch',bl.ts_bucket::DATE) as begin,
				DATE_PART('epoch',bl.ts_bucket::DATE) as end,
				sum(bl.in_bytes) as in_bytes,
				sum(bl.out_bytes) as out_bytes
				FROM site_totals_daily_view AS bl
				WHERE bl.site_id=" . $this->siteId . "
				AND bl.ts_bucket >= CURRENT_TIMESTAMP::DATE - INTERVAL '90 DAY'
				GROUP BY (bl.ts_bucket::DATE)
				ORDER BY bl.ts_bucket::DATE;";
		} else {
			fatal("Unknown grouping method `%s'", $grouping);
		}

		$query = \PostgreSQL::initialize()->query($query);
		$bandwidth = array();
		while (($bw = $query->fetch_object()) !== null) {
			$bandwidth[] = array(
				'begin'     => (int)$bw->begin,
				'end'       => (int)$bw->end,
				'in_bytes'  => (double)$bw->in_bytes,
				'out_bytes' => (double)$bw->out_bytes
			);
		}

		return $bandwidth;
	}

	/**
	 * Get bandwidth usage by span
	 *
	 * @param int      $begin
	 * @param int|null $end
	 * @return array
	 * @throws \PostgreSQLError
	 */
	public function getByRange(int $begin, int $end = null): array
	{
		$pgdb = \PostgreSQL::initialize();
		$endRestriction = '';
		if ($end) {
			$endRestriction = ' bandwidth_log.ts < TO_TIMESTAMP('
				. $pgdb->escape_string($end) . ') AND ';
		}
		$query = $pgdb->query(
			'SELECT
                    name,
					sum(in_bytes)  AS in_sum,
					sum(out_bytes) AS out_sum,
					name AS svc_name,
					info AS ext_info
				 FROM
					bandwidth_log
				 JOIN
					bandwidth_services
					USING (svc_id)
				 LEFT JOIN
				 	bandwidth_extendedinfo
				 	USING (ext_id)
				 WHERE
					 bandwidth_log.site_id = ' . $this->siteId . '
					AND
					 bandwidth_log.ts >= TO_TIMESTAMP(' . $pgdb->escape_string($begin) . ')
					AND ' . $endRestriction . '
					 bandwidth_services.svc_id = bandwidth_log.svc_id
					GROUP BY
						name, info');
		$services = array();
		while (null !== ($service = $query->fetch_object())) {
			$services[] = array(
				'in'       => (double)$service->in_sum,
				'out'      => (double)$service->out_sum,
				'svc_name' => $service->svc_name,
				'ext_info' => $service->ext_info
			);
		}

		return $services;
	}

	/**
	 * Update extendedinfo tag for bandwidth on changes
	 *
	 * @param string $old
	 * @param string $new
	 * @return bool
	 */
	public function renameExtendedInfo(string $old, string $new): bool
	{
		$db = \PostgreSQL::initialize();
		$db->query("UPDATE bandwidth_extendedinfo SET info = '" .
			pg_escape_string($new) . "' WHERE site_id = " . $this->siteId .
			" AND info = '" . pg_escape_string($old) . "'");

		return $db->affected_rows() > 0;
	}

	/**
	 * Collapse bandwidth data
	 *
	 * @param int    $begin
	 * @param int    $end
	 * @param string $specificity
	 * @param bool   $verbose preserve per-service metadata
	 * @return bool
	 */
	public function zip(int $begin, int $end, string $specificity = '30 minutes', bool $verbose = false): bool
	{
		$stub = \PostgreSQL::pdo();
		$groupAs = [
			'site_id',
			'time',
			'svc_id'
		];
		if ($verbose) {
			$groupAs[] = 'ext_id';
		}
		$stmt = $stub->prepare('WITH zip AS (
			DELETE FROM bandwidth_log
			WHERE
				site_id = :site_id AND
				ts >= TO_TIMESTAMP(:begin) AND ts < TO_TIMESTAMP(:end)
			RETURNING *)
			INSERT INTO bandwidth_log (site_id, in_bytes, out_bytes, ts)
			SELECT site_id,
				SUM(in_bytes)::BIGINT AS in_bytes,
				SUM(out_bytes)::BIGINT AS out_bytes,
				TIME_BUCKET(:specificity, ts) AS time FROM zip
			GROUP BY (' . implode(',', $groupAs) . ') ORDER BY time');
		$rs = $stmt->execute([
			':site_id' => $this->siteId,
			':specificity' => $specificity,
			':begin' => $begin,
			':end' => $end
		]);
		if (!$rs) {
			return error('Failed to collapse span for %d (%s) using spec %s: (%s) %s',
				$this->siteId,
				\Auth::get_domain_from_site_id($this->siteId),
				$specificity,
				$stmt->errorCode(),
				$stmt->errorInfo()[2]
			);
		}

		info('Zipped site %d (%s; specificity: %s) range [%d, %d)',
			$this->siteId,
			\Auth::get_domain_from_site_id($this->siteId),
			$specificity,
			$begin,
			$end
		);

		return true;
	}

}

