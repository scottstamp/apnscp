<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Ipinfo;

	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\Service\Validators\Common\MustBeEnabled;
	use Opcenter\SiteConfiguration;

	class Enabled extends \Opcenter\Service\Validators\Common\Enabled implements ServiceInstall
	{

		public const DESCRIPTION = 'Assign account unique IPv4 address from pool';

		public function valid(&$value): bool
		{
			$other = static::class === self::class ? 'ipinfo6' : 'ipinfo';
			if (!$value && !$this->ctx->getServiceValue($other, 'enabled')) {
				return error('IPv4 or IPv6 must be enabled to add domain');
			}

			if ($this->ctx->isEdit() && $value !== $this->ctx->getOldServiceValue(null, 'enabled')) {
				$this->forceValidation();
			}

			return parent::valid($value);
		}


		public function populate(SiteConfiguration $svc): bool
		{
			return true;
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			return true;
		}
	}