<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Common;

	use Opcenter\Service\Contracts\DefaultNullable;
	use Opcenter\Service\ServiceValidator;

	abstract class ProviderKey extends ServiceValidator implements DefaultNullable
	{

		public function valid(&$value): bool
		{
			return true;
		}



	}