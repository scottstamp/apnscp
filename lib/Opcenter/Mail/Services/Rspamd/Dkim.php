<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, January 2021
 */

namespace Opcenter\Mail\Services\Rspamd;

use Illuminate\Support\Str;
use Lararia\Jobs\SimpleCommandJob;
use Opcenter\Admin\Bootstrapper;
use Opcenter\Admin\Bootstrapper\Config;
use Opcenter\Contracts\VirtualizedContextable;
use Opcenter\Crypto\Ssl;
use Opcenter\Dns\Bulk;
use Opcenter\Dns\Record;
use Opcenter\Mail\Services\Rspamd;

/**
 * Class Dkim
 *
 * DKIM signing
 *
 * @package Opcenter\Mail\Services
 */
class Dkim implements VirtualizedContextable {
	use \ContextableTrait;

	// default selector
	const SELECTOR = 'dkim';

	protected function __construct() { }

	/**
	 * DKIM signing available to account
	 *
	 * @return bool
	 */
	public function enabled(): bool
	{
		return Rspamd::present() && MAIL_DKIM_SIGNING;
	}

	/**
	 * Get public DKIM key
	 *
	 * @return string|null
	 */
	public function publicKey(): ?string
	{
		if (!$this->enabled()) {
			return null;
		}

		if ($olduid = posix_geteuid()) {
			if (!posix_seteuid(0)) {
				warn("seteuid failed");
			}
			defer($_, static function() use ($olduid) {
				posix_seteuid($olduid);
			});
		}

		$path = static::globalKeyPath($this->selector());
		if (!file_exists($path)) {
			return null;
		}

		$key = file_get_contents($path);

		if (!($res = openssl_pkey_get_private($key))) {
			return null;
		}
		$pub = openssl_pkey_get_details($res)['key'];
		if (!$pub) {
			error("Invalid or corrupted DKIM key detected");
			return null;
		}
		$der = Ssl::pem2Der($pub);
		return base64_encode($der);
	}

	/**
	 * Get active selector
	 *
	 * @return string|null
	 */
	public function selector(): ?string
	{
		if (posix_getuid() !== 0) {
			fatal("%s must be called from CLI", __METHOD__);
		}

		if (!Configuration::configurationExists('dkim_signing.conf')) {
			return null;
		}

		if ($olduid = posix_geteuid()) {
			// when called from within a job
			// @see Dkim::changeSelector()
			if (!posix_seteuid(0)) {
				warn("seteuid failed");
			}
			defer($_, static function () use ($olduid) {
				posix_seteuid($olduid);
			});
		}
		return array_get(new Configuration('dkim_signing.conf', 'r'), 'selector', self::SELECTOR);
	}

	/**
	 * Expire set selector
	 *
	 * @param string $selector
	 * @return bool
	 */
	public function expireSelector(string $selector): bool
	{
		if ($selector === $this->selector()) {
			return Bootstrapper::running() ?
				error("Cannot expire active selector %s. Bootstrapper is running. Wait until it finishes.", $selector) :
				error("Cannot expire active selector %s", $selector);
		}

		if (!ctype_alnum($selector)) {
			return error("Selector must be alphanumeric");
		}

		$record = Str::replaceFirst($this->selector(), $selector, $this->selectorRecord());
		// bulk change DNS
		$r = new Record('_bogus-domain.com', [
			'name'      => $record,
			'rr'        => 'TXT',
			'parameter' => ''
		]);
		(new Bulk())->remove($r, function (\apnscpFunctionInterceptor $afi, Record $r) {
			return $afi->email_transport_exists($r->getZone());
		});

		return true;
	}

	/**
	 * Change DKIM selector
	 *
	 * @param string $selector
	 * @return bool
	 */
	public function changeSelector(string $selector): bool
	{
		if ($selector === $this->selector()) {
			return Bootstrapper::running() ?
				error("Selector %s already in use. Bootstrapper is running. Wait until it finishes.", $selector) :
				error("Selector %s already in use", $selector);
		}

		if (!ctype_alnum($selector)) {
			return error("Selector must be alphanumeric");
		}

		if (\strlen($selector) > 16 || !$selector) {
			return error("Selector length must be between 1 and 16 characters");
		}

		if ($this->getAuthContext()->level & PRIVILEGE_SITE) {
			return error("Per-site not supported yet");
		}

		// bulk change DNS


		$cfg = new Config();
		$cfg['rspamd_dkim_selector'] = $selector;
		$cfg->sync();
		$job = Bootstrapper::job('mail/rspamd');

		// runs in sequence
		$nextJob = (new SimpleCommandJob($this->getAuthContext(), function () {
			$record = $this->selectorRecord();
			$key = $this->dkimRecord();
			$r = new Record('_bogus-domain.com', [
				'name'      => $record,
				'rr'        => 'TXT',
				'parameter' => $key
			]);
			(new Bulk())->add($r, function (\apnscpFunctionInterceptor $afi, Record $r) {
				return $afi->email_transport_exists($r->getZone());
			});
		}));
		$job->dispatch([$nextJob]);
		return true;
	}

	/**
	 * Get DKIM key path
	 *
	 * @param string $selector
	 * @return string
	 */
	public static function globalKeyPath($selector = self::SELECTOR): string
	{
		return Rspamd::RSPAMD_HOME. '/dkim/global.' . $selector . '.key';
	}


	/**
	 * Roll DKIM key
	 *
	 * @param string|null $into key to roll into
	 * @return bool
	 */
	public function rollKey(string $into = null): bool
	{
		if (!static::singleKey()) {
			return error("Multi-site keys not supported");
		}

		if (!$into) {
			$selector = $this->selector();
			$len = strcspn($selector, '0123456789');
			$digit = (int)substr($selector, $len) + 1;
			$into = substr($selector, 0, $len) . $digit;
		}

		return $this->changeSelector($into);
	}

	/**
	 * DKIM uses single key
	 *
	 * @return bool
	 */
	public static function singleKey(): bool
	{
		return MAIL_DKIM_SIGNING === true;
	}

	/**
	 * DKIM selector name
	 *
	 * @return string
	 */
	public function selectorRecord(): string
	{
		return $this->selector() . '._domainkey';
	}

	/**
	 * DKIM DNS label
	 *
	 * @return string|null
	 */
	public function dkimRecord(): ?string
	{
		if (!($key = $this->publicKey())) {
			return null;
		}

		return "v=DKIM1; k=rsa; p=" . $this->publicKey();
	}
}
