<?php
	declare(strict_types=1);
	/**
	 *  +------------------------------------------------------------+
	 *  | apnscp                                                     |
	 *  +------------------------------------------------------------+
	 *  | Copyright (c) Apis Networks                                |
	 *  +------------------------------------------------------------+
	 *  | Licensed under Artistic License 2.0                        |
	 *  +------------------------------------------------------------+
	 *  | Author: Matt Saladna (msaladna@apisnetworks.com)           |
	 *  +------------------------------------------------------------+
	 */

	/**
	 * Version control, CVS and subversion
	 *
	 * @package core
	 */
	class Verco_Module extends Module_Skeleton
	{

		protected $exportedFunctions = [
			'*' => PRIVILEGE_SITE
		];


		/**
		 * bool svn_enabled
		 * Checks to see if Subversion is enabled for an account
		 *
		 * @return bool
		 */
		public function svn_enabled(): bool
		{
			return $this->ssh_enabled() && file_exists($this->domain_fs_path() . '/usr/bin/svn');
		}


	}
