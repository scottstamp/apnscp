<?php declare(strict_types=1);

	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, August 2018
	 */
	class Config_Module extends Scope_Module
	{
		public function set(string $name, ...$val): bool
		{
			debug('Forwarding to scope:set');
			return parent::set($name, ...$val);
		}

		public function list(string $filter = ''): array
		{
			debug('Forwarding to scope:list');
			return parent::list($filter);
		}

		public function info(string $name): ?array
		{
			debug('Forwarding to scope:info');
			return parent::info($name);
		}

		public function get(string $name, ...$val)
		{
			debug('Forwarding to scope:get');
			return parent::get($name, ...$val);
		}


		public function _housekeeping()
		{

		}
	}
