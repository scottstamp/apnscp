<?php declare(strict_types=1);

	use Module\Support\Webapps\App\Type\Adhoc\Manifest;
	use Module\Support\Webapps\App\Type\Unknown\Handler as Unknown;
	use Module\Support\Webapps\App\Loader;

	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2020
	 */
	class Webapp_Module extends \Module\Support\Webapps
	{
		public function __construct()
		{
			parent::__construct();
			$this->exportedFunctions['prune'] = PRIVILEGE_SITE;
		}

		public function install(string $hostname, string $path = '', array $opts = array()): bool
		{
			return error('Unsupported universal function %s', __METHOD__);
		}

		public function uninstall(string $hostname, string $path = '', $delete = 'all'): bool
		{
			return $this->redirect(__FUNCTION__, $hostname, $path, ...array_slice(func_get_args(), 2));
		}

		public function plugin_status(string $hostname, string $path = '', string $plugin = null)
		{
			return $this->redirect(__FUNCTION__, $hostname, $path, ...array_slice(func_get_args(), 2));
		}

		public function install_plugin(string $hostname, string $path, string $plugin, string $version = ''): bool
		{
			return $this->redirect(__FUNCTION__, $hostname, $path, ...array_slice(func_get_args(), 2));
		}

		public function uninstall_plugin(string $hostname, string $path, string $plugin, bool $force = false): bool
		{
			return $this->redirect(__FUNCTION__, $hostname, $path, ...array_slice(func_get_args(), 2));
		}

		public function disable_all_plugins(string $hostname, string $path = ''): bool
		{
			return $this->redirect(__FUNCTION__, $hostname, $path, ...array_slice(func_get_args(), 2));
		}

		public function db_config(string $hostname, string $path = '')
		{
			$oldex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_FATAL);
			try {
				return $this->redirect(__FUNCTION__, $hostname, $path, ...array_slice(func_get_args(), 2));
			} catch (\apnscpException $e) {
				$credentials = $this->loadManifest($hostname, $path)['database'] ?? [];

				return empty($credentials['db']) ? [] : $credentials;
			} finally {
				\Error_Reporter::exception_upgrade($oldex);
			}
		}

		public function get_versions(): array
		{
			return [];
		}

		public function change_admin(string $hostname, string $path, array $fields): bool
		{
			return $this->redirect(__FUNCTION__, $hostname, $path, ...array_slice(func_get_args(), 2));
		}

		public function get_admin(string $hostname, string $path = ''): ?string
		{
			return $this->redirect(__FUNCTION__, $hostname, $path, ...array_slice(func_get_args(), 2));
		}

		public function get_version(string $hostname, string $path = ''): ?string
		{
			return $this->redirect(__FUNCTION__, $hostname, $path, ...array_slice(func_get_args(), 2));
		}

		public function update_all(string $hostname, string $path = '', string $version = null): bool
		{
			return $this->redirect(__FUNCTION__, $hostname, $path, ...array_slice(func_get_args(), 2));
		}

		public function update(string $hostname, string $path = '', string $version = null): bool
		{
			return $this->redirect(__FUNCTION__, $hostname, $path, ...array_slice(func_get_args(), 2));
		}

		public function update_plugins(string $hostname, string $path = '', array $plugins = array()): bool
		{
			return $this->redirect(__FUNCTION__, $hostname, $path, ...array_slice(func_get_args(), 2));
		}

		public function update_themes(string $hostname, string $path = '', array $themes = array()): bool
		{
			return $this->redirect(__FUNCTION__, $hostname, $path, ...array_slice(func_get_args(), 2));
		}

		public function fortify(string $hostname, string $path = '', string $mode = 'max', $args = []): bool
		{
			if (null === ($handler = $this->autoloadDriver($hostname, $path))) {
				return parent::fortify($hostname, $path, $mode, $args);
			}

			return $this->{"${handler}_" . __FUNCTION__}($hostname, $path, $mode, $args);
		}

		public function unfortify(string $hostname, string $path = ''): bool
		{
			if (null === ($handler = $this->autoloadDriver($hostname, $path))) {
				return parent::unfortify($hostname, $path);
			}

			return $this->{"${handler}_" . __FUNCTION__}($hostname, $path);
		}

		public function has_fortification(string $hostname, string $path = '', string $mode = null): bool
		{
			if (null === ($handler = $this->autoloadDriver($hostname, $path))) {
				$modes = $this->loadManifest($hostname, $path)['fortification'] ?? [];

				return ($mode === null) ? !empty($modes) : isset($modes[$mode]);
			}

			return $this->{"${handler}_" . __FUNCTION__}($hostname, $path, $mode);
		}

		public function fortification_modes(string $hostname, string $path = ''): array
		{
			if (null === ($handler = $this->autoloadDriver($hostname, $path))) {
				return array_keys($this->loadManifest($hostname, $path)['fortification'] ?? []);
			}

			return (array)$this->{"${handler}_" . __FUNCTION__}($hostname, $path);
		}

		/**
		 * @inheritDoc
		 */
		public function valid(string $hostname, string $path = ''): bool
		{
			return $this->discover($hostname, $path) !== null;
		}

		/**
		 * Redirect call to corresponding API
		 *
		 * @param string $method
		 * @param string $hostname
		 * @param string $path
		 * @param mixed  ...$args
		 * @return mixed
		 */
		protected function redirect(string $method, string $hostname, string $path, ...$args)
		{
			if (!$handler = $this->autoloadDriver($hostname, $path)) {
				fatal('Unknown or unsupported app located in %s/%s', $hostname, $path);
			}

			return $this->{"${handler}_${method}"}($hostname, $path, ...$args);

		}

		/**
		 * Autoload API driver
		 *
		 * @param string $hostname
		 * @param string $path
		 * @param bool   $force
		 * @return string|null
		 */
		protected function autoloadDriver(string $hostname, string $path = '', bool $force = false): ?string
		{
			static $typeModule = [];

			/** @var Unknown $app */
			$app = Loader::fromHostname(null, $hostname, $path, $this->getAuthContext());
			if (!($docroot = $app->getDocumentMetaPath())) {
				// bad subdomain
				return null;
			}

			if (!$force && ($type = $app->getClassMapping()) !== 'webapp') {
				return $type;
			}

			if ($force) {
				foreach (Loader::getKnownApps() as $type) {
					if (Loader::isApp($docroot, $type, $this->getAuthContext())) {
						return Loader::fromHostname($type, $hostname, $path, $this->getAuthContext())->getClassMapping();
					}
				}
			}

			return null;
		}

		/**
		 * Autoload Web App type
		 *
		 * @param string $hostname
		 * @param string $path
		 * @param bool   $force
		 * @return string|null
		 */
		protected function autoloadType(string $hostname, string $path = '', bool $force = false): ?string
		{
			$app = Loader::fromHostname(null, $hostname, $path, $this->getAuthContext());
			if (!($docroot = $app->getDocumentMetaPath())) {
				// bad subdomain
				return null;
			}
			if (!$force && ($type = $app->getModuleName()) !== 'webapp') {
				return $type;
			}
			foreach (Loader::getKnownApps() as $type) {
				if (Loader::isApp($docroot, $type, $this->getAuthContext())) {
					return $type;
				}
			}

			return null;
		}

		/**
		 * Discover available app
		 *
		 * @param string $hostname
		 * @param string $path
		 * @return string|null
		 */
		public function discover(string $hostname, string $path = ''): ?string
		{
			if (null !== ($type = $this->autoloadType($hostname, $path, true))) {
				success("detected `%s'; updating records", $type);
			}

			$app = Loader::fromHostname($type, $hostname, $path, $this->getAuthContext());
			$meta = [
				'version' => $app->getVersion(true) ?: null,
				'type'    => $type,
				'path'    => $path
			];
			$app->initializeMeta($meta);
			$app->getPane()->freshen(true);

			return $type;
		}

		/**
		 * Sign ad hoc manifest
		 *
		 * @param string $hostname
		 * @param string $path
		 * @return bool
		 */
		public function manifest_sign(string $hostname, string $path = ''): bool
		{
			return $this->loadManifest($hostname, $path)->sign();
		}

		/**
		 * Ad hoc manifest signed
		 *
		 * @param string $hostname
		 * @param string $path
		 * @return bool
		 */
		public function manifest_signed(string $hostname, string $path = ''): bool
		{
			return $this->loadManifest($hostname, $path)->verifySignature();
		}

		/**
		 * Create new manifest
		 *
		 * @param string $hostname
		 * @param string $path
		 * @return bool
		 */
		public function manifest_create(string $hostname, string $path = ''): bool
		{
			return $this->loadManifest($hostname, $path)->create();
		}

		protected function loadManifest(string $hostname, string $path): Manifest
		{
			$app = Loader::fromHostname('adhoc', $hostname, $path, $this->getAuthContext());

			return Manifest::instantiateContexted($this->getAuthContext(), [$app]);
		}

		/**
		 * App is blacklisted
		 *
		 * @param string $app
		 * @return bool
		 */
		public static function blacklisted(string $app): bool
		{
			if ($app === 'webapp') {
				return false;
			}

			return parent::blacklisted($app); // TODO: Change the autogenerated stub
		}

		/**
		 * @inheritDoc
		 */
		public function reconfigure(string $hostname, string $path, $param, $value = null): bool
		{
			if (static::class !== self::class || !($module = $this->autoloadDriver($hostname, $path))) {
				return parent::reconfigure($hostname, $path, $param, $value);
			}

			return $this->{$module . '_reconfigure'}($hostname, $path, $param, $value);
		}

		/**
		 * @inheritDoc
		 */
		public function reconfigurables(string $hostname, string $path = ''): array
		{
			if (static::class !== self::class || !($module = $this->autoloadDriver($hostname, $path))) {
				// forwarded
				return parent::reconfigurables($hostname, $path);
			}

			return $this->{$module . '_reconfigurables'}($hostname, $path);
		}

		/**
		 * Remove orphaned webapp metadata
		 *
		 * @return void
		 */
		public function prune()
		{
			\Module\Support\Webapps\Finder::prune([$this->site]);
		}

		/**
		 * @inheritDoc
		 */
		public function get_reconfigurable(string $hostname, string $path, $setting)
		{
			if (static::class !== self::class || !($module = $this->autoloadDriver($hostname, $path))) {
				// forwarded
				return parent::get_reconfigurable($hostname, $path, $setting);
			}

			return $this->{$module . '_get_reconfigurable'}($hostname, $path, $setting);
		}

		/**
		 * @inheritDoc
		 */
		public function snapshot(string $hostname, string $path = '', string $comment = 'snapshot'): bool
		{
			return $this->redirect(__FUNCTION__, $hostname, $path, ...array_slice(func_get_args(), 2));
		}

		/**
		 * @inheritDoc
		 */
		public function rollback(string $hostname, string $path = '', string $commit = null): bool
		{
			return $this->redirect(__FUNCTION__, $hostname, $path, ...array_slice(func_get_args(), 2));
		}


		public function _housekeeping()
		{
			\Module\Support\Webapps\PathManager::flush();
			\Module\Support\Webapps\PathManager::applicationViewPaths();
		}
	}