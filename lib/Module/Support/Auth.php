<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace Module\Support;

	use Crm_Module;
	use Mail;
	use Mail_Mime;
	use Module_Skeleton;
	use Util_Geoip;

	/**
	 * Support structure for auth module
	 */
	abstract class Auth extends Module_Skeleton
	{

		/**
		 * Remap siteinfo to auth on 7.5+ platforms
		 *
		 * @param null|string $platform_version optional platform version to compare against
		 * @return string
		 */
		public static function getAuthService($platform_version = PLATFORM_VERSION): string
		{
			return version_compare($platform_version, '7.5', '>=') ?
				'auth' : 'siteinfo';
		}

		/**
		 * Send security notice
		 *
		 * @param       $what
		 * @param array $args
		 * @return bool|mixed|void
		 */
		protected function sendNotice($what, $args)
		{
			if (\Auth::client_ip() === '127.0.0.1') {
				// originated locally from server
				return true;
			}
			$key = 'notify.' . $what . 'change';
			$donotify = \Preferences::get($key, true);
			if (!$donotify) {
				return true;
			}

			$hostname = 'UNKNOWN';
			$location = null;
			if ($tmp = $this->dns_gethostbyaddr_t($args['ip'], 3000)) {
				$hostname = $tmp;
			}
			$location = Util_Geoip::instantiateContexted($this->getAuthContext())->locate($args['ip']);
			$gmap = 'https://www.google.com/maps/preview/@' . $location['latitude'] . ',' . $location['longitude'] . ',8z';

			$vars = array_replace_recursive([
				'what'      => $what,
				'latitude'  => $location['latitude'],
				'longitude' => $location['longitude'],
				'city'      => $location['city'],
				'state'     => $location['state'],
				'country'   => $location['country'],
				'gmapurl'   => $gmap,
				'ip'        => $args['ip'],
				'hostname'  => $hostname,
				'username'  => $this->username,
				'domain'    => $this->domain,
				'config'    => [
					'geoip' => true
				],
			], $args);
			if (!$vars['email']) {
				return warn("no email configured - ignoring alert notification for %s change for `%s'", $what,
					$vars['username']);
			}
			$blade = \BladeLite::factory('views/email');
			$body = $blade->make('auth.change', $vars)->render();

			$opts = array(
				'html_charset' => 'utf-8',
				'text_charset' => 'utf-8'
			);
			$from = Crm_Module::FROM_NAME . ' <' . Crm_Module::FROM_ADDRESS . '>';
			$headers = array(
				'Sender' => $from,
				'From'   => $from
			);
			$mime = new Mail_Mime($opts);

			$mime->setHTMLBody($body);
			$mime->setTXTBody(strip_tags($body));
			$headers = $mime->txtHeaders($headers);
			$msg = $mime->get();

			return Mail::send(
				$vars['email'],
				ucwords($what) . ' Changed - ' . $vars['domain'],
				$msg,
				$headers
			);
		}

		/**
		 * Recreate TokyoCabinet map
		 */
		public static function rebuildMap()
		{
			if (file_exists(\Opcenter\Map::DOMAIN_MAP) && filesize(\Opcenter\Map::DOMAIN_MAP) > 0) {
				return true;
			}

			// backwards compatible for older platforms that don't explicitly use TC
			$tcd = \Opcenter\Map::load(\Opcenter\Map::DOMAIN_MAP, 'cd');

			return $tcd->copy(\Opcenter\Map::DOMAIN_TXT_MAP);
		}
	}