<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, January 2019
 */

namespace Module\Support\Webapps\MetaManager;

use Illuminate\Contracts\Support\Arrayable;
use Module\Support\Webapps\MetaManager;
use Symfony\Component\Yaml\Yaml;

class Meta implements \ArrayAccess, Arrayable {
	// @var string docroot
	protected $docroot;
	// @var array webapp meta
	protected $meta;
	// @var Options quick access to "options" meta
	protected $options;
	/**
	 * @var MetaManager
	 */
	protected $manager;

	public function __construct(MetaManager $manager, string $docroot, array &$meta = [])
	{
		$this->docroot = $docroot;
		$this->meta = &$meta;
		$this->manager = $manager;
		if (!isset($this->meta['options'])) {
			$this->meta['options'] = [];
		} else if ($this->meta['options'] instanceof Options) {
			$this->meta['options'] = $this->meta['options']->toArray();
		} else if ($this->meta['options'] instanceof \stdClass) {
			$this->meta['options'] = (array)$this->meta['options'];
		}

		$this->__wakeup();
		\assert($this->meta['options'] === $this->options && $this->options instanceof Options, 'Reference check constructor');
	}

	public function __destruct()
	{
		if (isset($this->meta['options'])) {
			\assert($this->meta['options'] === $this->options, 'Reference check destructor');
			unset($this->options);
			if (!\is_array($this->meta['options'])) {
				$this->meta['options'] = $this->meta['options']->toArray();
			}
		}
	}

	public function __wakeup()
	{
		if (!\is_array($this->meta['options'])) {
			$this->meta['options'] = $this->meta['options']->toArray();
		}

		$this->meta['options'] = new Options($this->meta['options']);
		// NB hold this reference in case "options" field gets overwritten
		$this->options = &$this->meta['options'];
	}

	public function dirty() {
		return $this->dirty;
	}

	public function toArray()
	{
		return $this->meta;
	}

	/**
	 * Unset dirty flag
	 */
	public function sync(): self
	{
		$this->manager->sync();
		return $this;
	}

	/**
	 * Perform bulk set overriding existing meta
	 *
	 * @param array $meta
	 * @return self
	 */
	public function set(array $meta): self
	{
		$this->manager->dirty = true;
		if (isset($meta['options'])) {
			$options = array_pull($meta, 'options');
			$this->options = new Options($options);
			$meta['options'] = &$this->options;
		}

		$this->meta = $meta;

		return $this;
	}

	public function merge(array $meta): self
	{
		$this->manager->dirty = true;
		if (isset($meta['options'])) {
			$this->options->merge(array_pull($meta, 'options'));
		}
		$this->meta = array_merge_recursive($this->meta, $meta);

		return $this;
	}

	public function replace(array $meta): self
	{
		$this->manager->dirty = true;
		if (isset($meta['options'])) {
			$this->options->replace(array_pull($meta, 'options'));
		}
		$this->meta = array_replace_recursive($this->meta, $meta);
		return $this;
	}

	/**
	 * Get a read-only copy of options
	 *
	 * @return Options
	 */
	public function getOptions(): Options
	{
		if (\is_array($this->options)) {
			// ???
			$this->meta['options'] = new Options($this->meta['options']);
		}
		return $this->options;
	}

	/**
	 * Set option value
	 *
	 * @param array|string|null $name
	 * @param mixed|null   $val
	 */
	public function setOption($name, $val = null): self
	{
		$this->manager->dirty = true;
		if (null === $name) {
			$this->meta['options'] = new Options();
			$this->options = &$this->meta['options'];

			return $this;
		}
		if (!\is_array($name)) {
			$name = [$name => $val];
		}

		foreach ($name as $k => $v) {
			if ($v === null) {
				unset($this->options[$k]);
			} else {
				$this->options[$k] = $v;
			}
		}

		return $this;
	}

	public function getDocumentRoot(): string
	{
		return $this->docroot;
	}

	public function offsetExists($offset)
	{
		return \array_key_exists($offset, $this->meta);
	}

	public function offsetGet($offset)
	{
		if ($offset === 'options') {
			return $this->getOptions();
		}
		return $this->meta[$offset];
	}

	public function offsetSet($offset, $value)
	{
		if ($offset === 'options') {
			fatal('Call setOption()');
		}
		$this->manager->dirty = true;

		$this->meta[$offset] = $value;
	}

	public function offsetUnset($offset)
	{
		$this->manager->dirty = true;
		if ($offset === 'options') {
			$this->options = new Options();
			return;
		}
		unset($this->meta[$offset]);
	}
}