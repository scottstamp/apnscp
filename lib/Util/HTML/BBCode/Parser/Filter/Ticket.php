<?php
	/* vim: set expandtab tabstop=4 shiftwidth=4: */
//

	/**
	 * @package  HTML_BBCodeParser
	 * @author   Matt Saladna <matt@apisnetworks.com>
	 * @desc     mashup of Basic and Extended filters
	 */


	require_once 'Util/HTML/BBCode/Parser/Filter.php';


	class Util_HTML_BBCode_Parser_Filter_Ticket extends Util_HTML_BBCode_Parser_Filter
	{

		/**
		 * An array of tags parsed by the engine
		 *
		 * @access   private
		 * @var      array
		 */
		var $_definedTags = array(
			'b'     => array(
				'htmlopen'   => 'strong',
				'htmlclose'  => 'strong',
				'allowed'    => 'all',
				'attributes' => array()
			),
			'i'     => array(
				'htmlopen'   => 'em',
				'htmlclose'  => 'em',
				'allowed'    => 'all',
				'attributes' => array()
			),
			'u'     => array(
				'htmlopen'   => 'span style="text-decoration:underline;"',
				'htmlclose'  => 'span',
				'allowed'    => 'all',
				'attributes' => array()
			),
			's'     => array(
				'htmlopen'   => 'del',
				'htmlclose'  => 'del',
				'allowed'    => 'all',
				'attributes' => array()
			),
			'sub'   => array(
				'htmlopen'   => 'sub',
				'htmlclose'  => 'sub',
				'allowed'    => 'all',
				'attributes' => array()
			),
			'sup'   => array(
				'htmlopen'   => 'sup',
				'htmlclose'  => 'sup',
				'allowed'    => 'all',
				'attributes' => array()
			),
			'quote' => array(
				'htmlopen'   => 'q',
				'htmlclose'  => 'q',
				'allowed'    => 'all',
				'attributes' => array('quote' => 'cite=%2$s%1$s%2$s')
			),
			'code'  => array(
				'htmlopen'   => 'code',
				'htmlclose'  => 'code',
				'allowed'    => 'all',
				'attributes' => array()
			),
			'div'   => array(
				'htmlopen'   => 'div',
				'htmlclose'  => 'div',
				'allowed'    => 'all',
				'attributes' => array()
			)
		);

	}


?>