<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2018
	 */

	namespace CLI\Yum\Synchronizer;

	use CLI\Yum\Synchronizer\CacheManager\Package;

	/**
	 * Class VirtualPackageManager
	 *
	 * Maintain internal cache of installed RPMs
	 */
	class SynchronizerCache implements \Iterator
	{
		public const CACHE_VERSION = 20181108;
		public const CACHE = 'cache/synchronizer.installed.cache';
		/**
		 * CacheManager instance
		 *
		 * @var
		 */
		private static $instance;

		// @var bool CM cloned
		private $cache = [];

		// @var bool CM needs write
		private $cloned = false;
		private $dirty = false;

		private function __construct()
		{
			if ($this->requiresRebuild()) {
				info('Rebuilding installed package cache');
				$this->generate();
			}
			$this->load();
		}

		/**
		 * Cache requires a complete rebuild
		 *
		 * @return bool
		 */
		private function requiresRebuild(): bool
		{
			$cacheFile = $this->getCachePath();
			if (!file_exists($cacheFile)) {
				return true;
			}
			$this->load();

			return $this->getMeta('version') !== static::CACHE_VERSION;
		}

		/**
		 * Get cache file path
		 *
		 * @return string
		 */
		private function getCachePath(): string
		{
			return storage_path(static::CACHE);
		}

		/**
		 * Load cache
		 */
		private function load(): void
		{
			$cacheFile = $this->getCachePath();
			if (file_exists($cacheFile)) {
				// @TODO restrict classes once this implementation is sorted
				$this->cache = \Util_PHP::unserialize(file_get_contents($cacheFile), true);
			}
		}

		/**
		 * Get cache meta
		 *
		 * @param string $key
		 * @return mixed
		 */
		public function getMeta(string $key)
		{
			return array_get($this->cache, "_meta.${key}", null);
		}

		/**
		 * Rebuild installed packages cache
		 *
		 * @return bool
		 */
		public function generate(): bool
		{
			$db = \PostgreSQL::initialize();
			$db->query('SELECT package_name FROM site_packages');
			$this->cache = [
				'_meta'    => [
					'version' => self::CACHE_VERSION
				],
				'packages' => [],
			];

			$db = \PostgreSQL::initialize()->getHandler();

			$res = pg_query($db, 'SELECT package_name, version, release, service FROM site_packages');
			while (false !== ($rs = pg_fetch_object($res))) {
				$package = $rs->package_name;
				if (!Utils::exists($package)) {
					pg_query($db,
						"DELETE FROM site_packages WHERE package_name = '" . pg_escape_string($package) . "'");
					info("removed orphaned package `%s' from `%s'", $package, $rs->service);
					continue;
				}
				$meta = Utils::getMetaFromRPM($package);
				if ($meta['version'] !== $rs->version) {
					warn("`%s' mismatch: system version `%s', fst version `%s' - assuming files from system version. " .
						"Run '%s resync' to refresh filesystem template.",
						$package,
						$meta['version'],
						$rs->version,
						$_SERVER['argv'][0]
					);
				}

				$this->addPackage(
					(new Package(
						$package,
						$meta['version'],
						$rs->release ?? '0' // backwards compat for bugged older versions
					))->addFiles(Utils::getFilesFromRPM($package))
				);
			}

			return $this->save();
		}

		/**
		 * Add package to cache
		 *
		 * @param Package $package
		 */
		public function addPackage(Package $package): void
		{
			$this->dirty = true;
			$name = $package->name();
			if (!isset($this->cache['packages'][$name])) {
				$this->cache['packages'][$name] = [];
			}
			$this->cache['packages'][$name][$package->versionFull()] = $package;
		}

		/**
		 * Save cache
		 *
		 * @return bool
		 */
		private function save(): bool
		{
			if (file_put_contents($this->getCachePath(), \serialize($this->cache), LOCK_EX) > 0) {
				$this->dirty = false;

				return true;
			}

			return false;
		}

		/**
		 * Get CacheManager instance
		 *
		 * @return SynchronizerCache
		 */
		public static function get(): self
		{
			if (null === self::$instance) {
				self::$instance = new static;
			}

			return self::$instance;
		}

		public function __destruct()
		{
			if (!$this->dirty) {
				return;
			}
			if ($this->cloned) {
				warn('Cache is dirty, but cannot save cloned copy');

				return;
			}
			$this->save();
		}

		/**
		 * Get installed package version/meta
		 *
		 * @param string $package
		 * @return string
		 */
		public function installed(string $package): ?string
		{
			if (!$meta = Utils::getMetaFromPackage($package)) {
				return null;
			}

			return $meta['version'] . '-' . $meta['release'];
		}

		public function clone(): self
		{
			return clone $this;
		}

		public function __clone()
		{
			$this->cloned = true;
		}

		/**
		 * Package exists in cache
		 *
		 * @param string      $package
		 * @param string|null $fullVersion version + release
		 * @return bool
		 */
		public function exists(string $package, string $fullVersion = null): bool
		{
			if ($fullVersion) {
				return isset($this->cache['packages'][$package][$fullVersion]);
			}

			return isset($this->cache['packages'][$package]);
		}

		/**
		 * Package is installed
		 *
		 * @param string $name
		 * @return bool
		 */
		public function packageInstalled(string $name): bool
		{
			return Utils::packageInstalled($name);
		}

		/**
		 * Get installed package data
		 *
		 * @param string      $package
		 * @param string|null $version
		 * @param string|null $release
		 * @return Package|null
		 */
		public function getPackage(string $package, string $version = null, string $release = null): ?Package
		{
			$fullVersion = null;
			if ($version !== null && $release !== null) {
				$fullVersion = $version . '-' . $release;
			} else if ($version || $release) {
				error('When specifying a package version or release, both version and release must be provided');

				return null;
			}
			if (!$fullVersion && null === ($fullVersion = Utils::getFullVersionFromPackage($package))) {
				error("Package `%s' is not installed in FST", $fullVersion);
				return null;
			}
			if (null === ($obj = array_get(array_get($this->cache, "packages.${package}", []), $fullVersion, null))) {
				return $obj;
			}

			return $obj;
		}

		/**
		 * Remove package from synchronizer cache
		 *
		 * @param string $package
		 */
		public function removePackage(string $package): void
		{
			$this->dirty = true;
			unset($this->cache['packages'][$package]);
		}

		public function next()
		{
			next($this->cache['packages']);
		}

		public function key()
		{
			return key($this->cache['packages']);
		}

		public function valid()
		{
			return $this->current() !== false;
		}

		public function current()
		{
			if (false === ($cur = current($this->cache['packages']))) {
				return false;
			}

			if (\count($cur) > 2) {
				$meta = Utils::getMetaFromPackage($this->key);
				$fullVersion = $meta['version'] . '-' . $meta['release'];
				if (!isset($cur[$fullVersion])) {
					warn("Package `%s' meta (ver %s, rel %s) missing from synchronizer cache - using most recent",
						$cur, $meta['version'], $meta['release']
					);
				}

				return $cur[$fullVersion] ?? current($cur);
			}

			return current($cur);
		}

		public function rewind()
		{
			reset($this->cache['packages']);
		}


	}