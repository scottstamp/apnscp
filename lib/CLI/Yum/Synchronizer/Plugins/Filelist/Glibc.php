<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2020
	 */

	namespace CLI\Yum\Synchronizer\Plugins\Filelist;

	use CLI\Yum\Synchronizer\Plugins\Filelist;

	/**
	 * Class Rspamd
	 *
	 * @package CLI\Yum\Synchronizer\Plugins\Filelist
	 *
	 * Override filelist
	 *
	 */
	class Glibc extends Filelist
	{
		/**
		 * Get updated filelist
		 *
		 * @param array $files
		 * @return array
		 */
		public function filterFiles(array $files): array
		{
			return array_filter($files, static function ($v) {
				return $v !== '/etc/nsswitch.conf';
			});
		}
	}