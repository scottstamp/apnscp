<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2017
	 */

	namespace CLI\Yum\Synchronizer\Plugins\Trigger;

	use CLI\Yum\Synchronizer\Plugins\AlternativesTrigger;

	/**
	 * Class Binutils
	 *
	 * @package CLI\Yum\Synchronizer\Plugins\Trigger
	 *
	 * @todo    support other packages
	 *
	 */
	class Binutils extends AlternativesTrigger
	{
		/**
		 * @var array
		 */
		protected $alternatives = [
			[
				'name'     => 'ld',
				'src'      => '/usr/bin/ld',
				'dest'     => '/usr/bin/ld.bfd',
				'priority' => 30
			],
			[
				'name'     => 'ld',
				'src'      => '/usr/bin/ld',
				'dest'     => '/usr/bin/ld.gold',
				'priority' => 50
			],

		];
	}