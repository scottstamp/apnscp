<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2018
	 */


	namespace CLI\Yum\Synchronizer\CacheManager;

	class Package implements \Serializable, \ArrayAccess
	{
		/**
		 * @var string package name
		 */
		protected $name;

		/**
		 * @var string version
		 */
		protected $version;

		/**
		 * @var string release
		 */
		protected $release;

		/**
		 * @var array files
		 */
		protected $files = [];

		/**
		 * @var PackageFiles[]
		 */
		/**
		 *
		 * Package constructor.
		 *
		 * @param string $name
		 * @param string $version
		 * @param string $release
		 */

		public function __construct(string $name, string $version, string $release)
		{
			$this->name = $name;
			$this->version = $version;
			$this->release = $release;
		}

		public function __debugInfo()
		{
			return [
				'name'    => $this->name,
				'version' => $this->version,
				'release' => $this->release,
				'files'   => array_keys(array_filter($this->files)),
			];
		}

		/**
		 * Add files
		 *
		 * @param array $files
		 * @return Package
		 */
		public function addFiles(array $files): self
		{
			$this->files += array_fill_keys($files, 1);

			return $this;
		}

		/**
		 * Soft delete files
		 *
		 * @param array $files
		 * @return Package
		 */
		public function removeFiles(array $files): self
		{
			$this->files += array_fill_keys($files, 0);

			return $this;
		}

		/**
		 * Remove soft-deleted files permanently from package
		 */
		public function prune(): void
		{
			$this->files = array_filter($this->files);
		}

		public function serialize(): string
		{
			return \serialize(
				[
					'files'   => $this->files,
					'name'    => $this->name,
					'version' => $this->version,
					'release' => $this->release
				]
			);
		}

		public function unserialize($serialized): void
		{
			$data = \Util_PHP::unserialize($serialized);
			[$this->files, $this->name, $this->version, $this->release] = [
				$data['files'],
				$data['name'],
				$data['version'],
				$data['release']
			];
		}

		public function versionFull(): string
		{
			return $this->version() . '-' . $this->release();
		}

		public function version(): string
		{
			return $this->version;
		}

		public function release(): string
		{
			return $this->release;
		}

		public function has(string $file): bool
		{
			return isset($this[$file]);
		}

		/**
		 * Get package name
		 *
		 * @return string
		 */
		public function name(): string
		{
			return $this->name;
		}

		/**
		 * Calculate files in this package not in $comp
		 *
		 * @param Package $comp
		 * @return array
		 */
		public function diff(Package $comp): array
		{
			// bugged?
			return array_diff($this->files(), $comp->files());
		}

		/**
		 * Get files in package
		 *
		 * @return array
		 */
		public function files(): array
		{
			return array_keys(array_filter($this->files));
		}

		/**
		 * Calculate files in this package and in $comp
		 *
		 * @param Package $comp
		 * @return array
		 */
		public function union(Package $comp): array
		{
			return array_intersect($this->files(), $comp->files());
		}

		public function offsetExists($index): bool
		{
			return ($this->files[$index] ?? 0) === 1;
		}

		public function offsetGet($index): bool
		{
			return $this->files[$index];
		}

		public function offsetSet($index, $newval): void
		{
			$this->files[$index] = $newval;
		}

		public function offsetUnset($index): void
		{
			$this->files[$index] = 0;
		}
	}