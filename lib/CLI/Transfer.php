<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	/**
	 * Migration procedure:
	 * 1. Dump site configuration
	 * 2. Add site on remote
	 * 3. Reduce DNS TTL window to 60 seconds
	 * 4. Add DNS TXT record - TS of change
	 * 5. Create users
	 * 6. Set crypted passwords
	 * 7. Wait 24 hours
	 */
	class CLI_Transfer
	{
		const STATUS_EMAIL = MIGRATION_STATUS_EMAIL;
		const PLACEHOLDER_FILE = \Web_Module::MAIN_DOC_ROOT . '/index.html';
		// prepped migration TTL for DNS
		const MIGRATION_RECORD_NAME = '__acct_migration';
		/**
		 * @var Auth_Info_User
		 */
		protected $_auth;

		/**
		 * @var apnscpFunctionInterceptor
		 */
		private $_afi;
		// Site configuration
		private $_cfg;
		// Remote SOAP API client
		private $_api;
		// Unique password
		private $_onetimepw;
		// Migration stage
		private $_stage;
		// Force xfer ahead of schedule
		private $_force;
		// Domain
		private $_domain;
		// platform ver
		private $_destPlatformVersion;

		private $_createAccount = true;

		// account creation overrides
		private $overrides = [];

		private $_buffer = array();

		private $_xferfiles = array(
			'/home',
			'/var/www',
			'/usr/local',
			'/etc/usertemplate',
			'/etc/logrotate.d',
			'/etc/maildroprc',
		);

		// domains that do not use our hosting nameservers for DNS
		private $_nslessdomains = array();

		// optional manual stage sync via do=what,args flag
		private $_syncStages = array();

		public function __construct(Auth_Info_User $auth)
		{
			$this->_auth = $auth;
			$this->_afi = \apnscpFunctionInterceptor::factory($auth);
			$this->_cfg = $this->get_config();
			if (!is_array($this->_cfg)) {
				fatal('cannot import config for site');
			}
		}

		public function get_config()
		{
			$cfg = $this->common_get_services();
			if (!array_get($cfg, 'aliases.aliases')) {
				array_set($cfg, 'aliases.enabled', 0);
			}

			return $cfg;
		}

		public function __call($cmd, $args)
		{
			return call_user_func_array(array($this->_afi, $cmd), $args);
		}

		public function setOverrides(array $params)
		{
			$this->overrides = $params;
		}

		public function process()
		{
			$start = microtime(true);
			$cfg = $this->get_config();
			$domain = $this->_domain = $cfg['siteinfo']['domain'];
			if ($this->_sync('local', 'auth_is_inactive') && !$this->_force) {
				$this->log('FATAL: %s is in suspended state, not transferring!', $domain);

				return false;
			}
			$this->_stage = $this->get_migration_stage();

			if (!is_int($this->_stage)) {
				$this->log('%s: account not ready for next phase - exiting', $domain);

				return false;
			}
			if ($this->_isDebug()) {
				$this->_stage = 0;
			}

			$this->log('beginning migration: %s (stage %d)', $domain, $this->_stage);

			if ($this->_stage == 2) {
				$this->log('ignoring migration task (completed)');

				return false;
			}

			// add site
			$target = $this->getTargetServer();
			if ($target == SERVER_NAME || $target == SERVER_NAME_SHORT) {
				fatal('destination is same as source!');
			}

			if ($this->_createAccount && $this->_stage == 0) {
				if (!$this->_create_site()) {
					return false;
				}
			}

			$this->_create_api_client();
			$this->sanity();
			if (count($this->_syncStages) > 0) {
				return $this->_processStages();
			}

			$this->_sync_users();
			$this->_sync_passwords();

			if ($this->sql_enabled('mysql')) {
				$this->_sync_sql_databases('mysql');
				$this->_sync_sql_users('mysql');
				$this->_sync_sql_schema('mysql');
			}
			if ($this->sql_enabled('pgsql')) {
				if (version_compare($this->_get_dest_platform(), '7.5', '<')) {
					$this->_remote_cmd('/usr/local/sbin/addpgsql-nodb.sh ' .
						$this->_cfg['siteinfo']['domain'] . ' ' .
						escapeshellarg(\Opcenter\Auth\Password::generate()));
				}
				$this->_sync_sql_databases('pgsql');
				$this->_sync_sql_users('pgsql');
				$this->_sync_sql_schema('pgsql');
			}

			$this->_sync_addon_domains();
			$this->_sync_subdomains();

			$this->_sync_email_domains();
			$this->_sync_mailing_lists();

			$this->_sync_files($this->buildFileList());
			$this->_sync_mailboxes();
			$this->_sync_crons();
			$this->_sync_vmount();
			$this->_fix_apache_ownership();
			$this->_sync_http_custom_config();
			$this->_sync_dns();
			$this->_sync_ssl();
			$this->_sync_letsencrypt_ssl();
			$destver = $this->_get_dest_platform();

			// aufs retired in Sol
			if ($destver >= 4.5 && $destver < 6) {
				$remotepath = $this->_sync('remote', 'common_get_base_path');
				$this->_remote_cmd('auplink ' . $remotepath . ' flush');
			} else if ($destver >= 6) {
				$site = 'site' . $this->_getSiteId('remote');
				$this->_remote_cmd(\Opcenter\Service\ServiceLayer::MOUNT_CMD . " reload ${site}");
			}

			$this->_stage++;
			$this->_sync_update_stage();
			// schedule phase 2 in 24 hours
			if ($this->_stage === 1) {
				$cmd = PHP_BINARY . ' ' . $_SERVER['SCRIPT_FILENAME'] . ' ' . 'site' . $this->_getSiteId('local');
				// at interprets "tomorrow" as +24 hours
				// PHP interprets "tomorrow' as midnight following day
				$proc = new Util_Process_Schedule('+24 hours');
				$ret = $proc->run($cmd);
				if (!$ret['success']) {
					fatal('failed to schedule process: %s', $ret['error']);
				}
			} else {
				if (!$cfg['ipinfo']['namebased']) {
					$myip = (array)$this->_sync('local', 'common_get_ip_address');
					foreach ($myip as $ip) {
						dlog("releasing old IP `%s'", $ip);
						$this->_free_ip($ip);
					}

				}

				$this->_delete_api_key();

			}
			$dx = microtime(true) - $start;
			$this->log('completed xfer ' . $cfg['siteinfo']['domain'] . ' (%02d:%02d:%02d)',
				$dx / 3600, $dx / 60 % 60, $dx % 60);

			return true;
		}

		private function _sync($which, $cmd, ...$args)
		{
			static $called, $originalEx;
			if (is_null($called)) {
				$called = 0;
			}
			if ($which == 'local') {
				$afi = $this->_afi;
			} else {
				$afi = $this->_api;
			}
			try {
				$resp = call_user_func_array(array($afi, $cmd), $args);
			} catch (Exception $e) {
				if ($called == 0) {
					/**
					 * first call, bugfix workaround: store first ex
					 * sometimes these can be overwritten by different
					 * failure points on repeat calls
					 */
					$originalEx = $e;
				}
				if ($called < 5) {
					$called++;
					$this->log('NOTICE: ' . $cmd . ' failed... retrying...');

					return call_user_func_array(array($this, '_sync'),
						func_get_args());
				}
				$this->log('FAILURE: %s', $cmd);
				$this->log("%s \nLine %s \n%s", $originalEx->getMessage(),
					$originalEx->getLine(),
					$originalEx->getTraceAsString()
				);
				if ($originalEx->getMessage() != $e->getMessage()) {
					$this->log("FIRST != LAST: %s \nLine %s \n%s", $e->getMessage(),
						$e->getLine(),
						$e->getTraceAsString()
					);
				}
				throw $e;
				exit(1);
			}
			$called = 0;

			return $resp;

		}

		public function log($msg, ...$args)
		{
			if (count($args) > 0) {
				$msg = vsprintf($msg, $args);
			}
			/*if (strlen($msg) > 128)
				$msg = substr($msg,0,128) . ' ... ';*/
			$this->_buffer[] = $msg;
			if (!is_debug()) {
				print $msg . "\n";
			}
			dlog($msg);
		}

		/**
		 * Get current migration stage for a domain or active session
		 *
		 * @param $domain
		 * @return null|int
		 */
		public function get_migration_stage($domain = null): ?int
		{
			if (null !== $this->_stage && null === $domain) {
				return $this->_stage;
			}

			if (is_null($domain)) {
				$domain = $this->_domain;
			}

			if (!$this->dns_configured()) {
				return error('DNS is not configured - cannot determine migration stage. Set manually with --stage=N');
			}
			$dns = $this->dns_get_records(self::MIGRATION_RECORD_NAME, 'txt', $domain);
			if (!$dns || !count($dns)) {
				return 0;
			}
			// parameter contains the timestamp of last step
			// timestamp value of 0 indicates completion
			$timestamp = (int)$dns[0]['parameter'];
			if ($timestamp < 1) {
				return 2;
			}

			if ($this->_force || CLI_Transfer_Util::canMigrate($timestamp)) {
				return 1;
			}

			return null;
		}

		private function _isDebug()
		{
			return is_debug() || $this->_cfg['siteinfo']['domain'] == 'debug.com';
		}

		public function getTargetServer()
		{
			static $server;
			if (null === $server) {
				$file = $this->_auth->domain_info_path('/.server.migration');
				if (file_exists($file)) {
					$server = trim(file_get_contents($file));
				} else {
					fatal('No target migration server set! Use -s <server> to define one');
				}
			}

			return $server;
		}

		public function suspend(): bool
		{
			return array_get($this->_cmd('SuspendDomain site' . $this->_getSiteId('local')), 'success', false);
		}

		private function _create_site()
		{
			$target = $this->getTargetServer();
			$cfg = $this->_cfg;
			$platform = array(
				'src'  => $this->_get_src_platform(),
				'dest' => $this->_get_dest_platform()
			);
			$this->log('creating site on ' . $target);

			$fixup = CLI_Transfer_Fixup::use_fixup('apis');
			$fixup->setLogger(array($this, 'log'));
			if (version_compare(platform_version(), '7.5', '<')) {
				$cfg['siteinfo']['tpasswd'] = \Opcenter\Auth\Password::generate();
			}

			// rollover is created automatically on site migration
			// honor this value, not old rollover
			if (isset($cfg['bandwidth']['rollover'])) {
				unset($cfg['bandwidth']['rollover']);
			}

			if ($this->overrides) {
				info('Overrides detected - replacing values');
				foreach ($this->overrides as $svc => $vars) {
					if (!isset($cfg[$svc])) {
						$cfg[$svc] = [];
					}
					foreach ($vars as $k => $v) {
						info('Overriding [%s] %s => %s', $svc, $k, $v);
						$cfg[$svc][$k] = $v;
					}
				}
			}

			if (!$cfg['ipinfo']['namebased']) {
				$ip = $this->_allocate_ip();
				if (version_compare($this->_get_dest_platform(), '7.5', '<')) {
					$this->_remote_cmd('/usr/local/sbin/add_ptr.sh ' .
						$cfg['siteinfo']['domain'] . ' ' . $ip);
				}
				$this->log('allocated IP ' . $ip);
				$cfg['ipinfo']['ipaddrs'] = array($ip);
				// don't know how to query yet
				unset($cfg['ipinfo6']['ipaddrs']);
				$cfg['openssl']['enabled'] = 1;
			} else {
				// let server decide on namebased IP
				unset($cfg['ipinfo']['nbaddrs'], $cfg['ipinfo6']['nbaddrs']);
			}
			// let server update proxied IP addresses
			unset($cfg['ipinfo']['proxyaddr'], $cfg['ipinfo']['proxy6addr']);

			$cfg = $fixup->fix($cfg, $platform);

			$cmd = $this->_build_addsite_cmd($cfg);
			$ret = $this->_remote_cmd($cmd);
			if (!$ret['success']) {
				if (!$cfg['ipinfo']['namebased']) {
					dlog("deleting allocated IP `%s'", $ip);
					$this->_free_ip($ip);
				}

				return error('unable to add site, aborting!');
			}
			// everything worked out OK, update config with
			// potentially adjusted records
			$this->_cfg = $cfg;

			return true;
		}

		private function _get_src_platform()
		{
			return platform_version();
		}

		private function _get_dest_platform()
		{
			if ($this->_destPlatformVersion === null) {
				$platform = $this->_remote_cmd('%s/bin/cmd misc_platform_version',
					realpath(INCLUDE_PATH)
				);
				if (!$platform['success']) {
					fatal('unable to get dest platform version: %s', $platform['stderr']);
				}
				$this->_destPlatformVersion = (float)$platform['output'];
			}

			return $this->_destPlatformVersion;
		}

		private function _remote_cmd($cmd, ...$args)
		{
			if (count($args) > 0) {
				$cmd = vsprintf($cmd, $args);
			}
			$localcmd = 'ssh ' . $this->getTargetServer() . ' %s';
			if (is_debug()) {
				$this->log('REMOTE: %s', vsprintf($cmd, $args));
			}
			$status = Util_Process_Safe::exec($localcmd, $cmd);
			if (!$status['success']) {
				$output = Error_Reporter::truncate($status['output'], 128);
				$stderr = Error_Reporter::truncate($status['stderr'], 128);
				if (!$output) {
					$output = '(null)';
				}
				if (!$stderr) {
					$stderr = '(null)';
				}
				$this->log("WARN: %s: NON-ZERO RETURN: %d\noutput:\n%s\nstderr:\n%s",
					$cmd, $status['return'], $output, $stderr);

			}

			return $status;
		}

		private function _allocate_ip()
		{
			static $myip;
			if ($myip) {
				return $myip;
			}

			$myip = \Opcenter\Net\Ip4::allocate(false);

			return $myip;
		}

		private function _build_addsite_cmd($services)
		{
			$editor = new Util_Account_Editor(\Auth::profile()->getAccount());
			// assemble domain creation cmd from current config
			//$editor->importConfig();
			foreach ($services as $svc => $vars) {
				foreach ($vars as $k => $v) {
					$editor->setConfig($svc, $k, $v);
				}
			}
			$cmd = $editor->setMode('add')->getCommand();

			return $this->renameCommand($cmd);
		}

		/**
		 * Convert command to new platform style
		 *
		 * @param string $command
		 * @return string
		 */
		private function renameCommand(string $command): string
		{
			// @todo move to support class
			if (version_compare($this->_get_dest_platform(), '7.5', '<')) {
				return $command;
			}

			return preg_replace('!^.*?/(Add|Delete|Edit)VirtDomain!', INCLUDE_PATH . '/bin/$1Domain', $command, 1);
		}

		private function _free_ip($ip)
		{
			$client = Util_API::create_client(ADMIN_API_KEY);
			if (!$client->dns_ip_allocated($ip)) {
				return warn('ip address %s not allocated', $ip);
			}

			$domain = $this->_cfg['siteinfo']['domain'];
			if (!$client->dns_release_ip($ip, $domain)) {
				dlog('failed to release allocated ip %s', $ip);
			}

			return true;
		}

		private function _create_api_client()
		{
			if (!$this->_api_key_exists() || !$this->_api_key_valid()) {
				$this->_create_api_key();
			}

			$key = $this->_get_api_key();
			$server = $this->getTargetServer();
			$api = Util_API::create_client($key, $server);
			$this->_api = $api;

			return $api;
		}

		private function _api_key_exists()
		{
			$file = $this->_auth->domain_info_path('api.key');

			return file_exists($file);
		}

		private function _api_key_valid()
		{
			$key = $this->_get_api_key();
			$server = $this->getTargetServer();
			$api = Util_API::create_client($key, $server);
			try {
				$resp = $api->common_get_uptime();
			} catch (\SoapFault $e) {
				return false;
			}

			return (bool)$resp;
		}

		private function _get_api_key()
		{
			if (!$this->_api_key_exists()) {
				$key = $this->_create_api_key();
				$this->_set_api_key($key);
			}
			$path = $this->_auth->domain_info_path('api.key');

			return trim(file_get_contents($path));
		}

		private function _create_api_key()
		{
			$key = $this->_remote_cmd(INCLUDE_PATH . '/bin/cmd -d %s %s %s',
				$this->_cfg['siteinfo']['domain'],
				'auth_create_api_key',
				'Migration'
			);
			if (!$key['success']) {
				fatal('cannot generate key!');
			}
			$key = trim($key['output']);
			$this->_set_api_key($key);

			return $key;
		}

		private function _set_api_key($key)
		{
			$path = $this->_auth->domain_info_path('api.key');
			file_put_contents($path, $key);

			return true;
		}

		/**
		 * Sanity check, validate src domain matches dest
		 */
		private function sanity(): void
		{
			$args = ['common_get_service_value', 'siteinfo', 'domain'];
			if (($a = $this->_sync('local', ...$args)) !== ($b = $this->_sync('remote', ...$args))) {
				fatal("source domain `%s' does not match destination domain `%s'!!! aborting", $a, $b);
			}

		}

		private function _processStages()
		{
			foreach ($this->_syncStages as $stage => $arg) {
				$method = '_sync_' . $stage;
				call_user_func_array(array($this, $method), (array)$arg);
			}

			return true;
		}

		private function _sync_users()
		{
			$this->log('synchronizing users');
			$local = $this->_get_users('local');
			$remote = $this->_get_users('remote');
			$create = array_diff_key($local, $remote);
			$remove = array_diff_key($remote, $local);
			foreach (array_keys($remove) as $user) {
				if (!$this->_sync('remote', 'user_delete', $user)) {
					error("cannot delete user `%s'", $user);
				}
			}
			$quotas = $this->_afi->user_get_quota(array_keys($create));
			foreach ($create as $user => $gecos) {
				if (!isset($quotas[$user])) {
					error("no quota information found for `%s'", $user);
					continue;
				}
				$quota = $quotas[$user]['qhard'] / 1024;
				$svc = $this->_afi->user_enrollment($user);
				$pop3 = platform_is('7.5') ? $this->_afi->email_user_enabled($user, 'pop3') :
					$svc['imap'] ?? false;
				if (version_compare($this->_get_dest_platform(), '7.5', '>=')) {
					$svc['pop3'] = $pop3;
					$svc['smtp'] = $svc['smtp_relay'] ?? false;
				}

				if (!$this->_sync('remote', 'user_add',
					$user,
					\Opcenter\Auth\Password::generate(16),
					$gecos['gecos'],
					$quota,
					array_fill_keys($svc, 1)
				)
				) {
					error('cannot add user `' . $user . "'");
					continue;
				}
				$indexhtml = '/home/' . $user . '/public_html/index.html';
				if ($this->_sync('remote', 'file_exists', $indexhtml)) {
					$this->_sync('remote', 'file_delete', $indexhtml);
				}
				if ($this->_sync('local', 'ftp_user_jailed', $user)) {
					$jail = '';
					if ($this->_sync('local', 'ftp_has_configuration', $user)) {
						$jail = $this->ftp_get_option($user, 'local_root');
					}
					$this->_sync('remote', 'ftp_jail_user', $user, $jail);
				}
				$this->log("added user `$user'");
			}

			$this->log('copying user config');
			foreach (array_keys($local) as $user) {
				$prefs = $this->_sync('local', 'common_get_user_preferences', $user);
				if (!$this->_sync('remote', 'common_set_user_preferences', $user, $prefs)) {
					error("failed to set preferences for `%s'", $user);
				}
			}
			$remoteinfo = $this->_get_info_path_from_base($this->_sync('remote', 'common_get_base_path'));
			$localinfo = $this->_get_info_path_from_base($this->_sync('local', 'common_get_base_path'));
			$globalPref = Common_Module::GLOBAL_PREFERENCES_NAME;
			if (file_exists($localinfo . '/users/' . $globalPref)) {
				$ret = $this->_cmd('rsync -aHWx %s/users/' . $globalPref . ' root@%s:%s/users/' . $globalPref,
					$localinfo,
					$this->getTargetServer(),
					$remoteinfo
				);
				if (!$ret) {
					warn('failed to sync global preferences');
				}
			}

		}

		private function _get_users($which)
		{
			if ($which == 'local') {
				$afi = $this->_afi;
			} else {
				$afi = $this->_api;
			}
			$users = $afi->user_get_users();
			if (!is_array($users)) {
				fatal('unable to fetch users from API server - terminating, ' .
					'account invoice resides on multiple servers?');
			}

			return $users;
		}

		private function _get_info_path_from_base($path)
		{
			return str_replace('/fst', '/info', $path);
		}

		private function _cmd($cmd, ...$args)
		{
			$this->log('LOCAL: %s', vsprintf($cmd, $args));

			return Util_Process::exec($cmd, $args);
		}

		private function _sync_passwords()
		{
			$this->log('synchronizing passwords');
			$users = $this->_get_users('remote');
			$file = file_get_contents($this->_auth->domain_fs_path('/etc/shadow'));
			if (version_compare($this->_get_dest_platform(), '6', '>=')) {
				// prevent generating notifications on account admin pw change
				$REMOTESITE = 'site' . $this->_getSiteId('remote');
				$LOCALSITE = 'site' . $this->_getSiteId('local');
				$EDITOR = new Util_Account_Editor(null, $this->_auth);
				$EDITOR->setMode('edit')->setConfig(
					Auth_Module::getAuthService($this->_get_dest_platform()),
					Auth_Module::PWOVERRIDE_KEY,
					1
				);
				$cmd = str_replace($LOCALSITE, $REMOTESITE, $EDITOR->getCommand());
				$this->_remote_cmd($this->renameCommand($cmd));
			}

			foreach (explode("\n", $file) as $line) {
				$line = explode(':', $line);
				if (count($line) < 2) {
					continue;
				}
				$user = $line[0];
				$shadow = $line[1];

				// user not a real account
				if ($shadow == '!!') {
					continue;
				}
				if (!isset($users[$user])) {
					warn('user `' . $user . "' present in shadow but not get_users()?");
					continue;
				}
				if (!$this->_sync('remote', 'auth_change_cpassword', $shadow, $user)) {
					error("failed to update password for user `$user'");
				}
			}

			if (version_compare($this->_get_dest_platform(), '6', '>=')) {
				// toggle generations back on
				$EDITOR->setConfig(
					Auth_Module::getAuthService($this->_get_dest_platform()),
					Auth_Module::PWOVERRIDE_KEY,
					0
				);
				$cmd = str_replace($LOCALSITE, $REMOTESITE, $EDITOR->getCommand());
				$this->_remote_cmd($this->renameCommand($cmd));
			}

			return true;
		}

		/**
		 * Get account site id
		 *
		 * @param $which
		 * @return null|int
		 * @throws Exception
		 */
		private function _getSiteId($which): ?int
		{
			$path = $this->_sync($which, 'common_get_base_path');

			return $this->_getSiteIdFromPath($path);
		}

		private function _getSiteIdFromPath($path): ?int
		{
			$path = explode('/', $path);
			foreach ($path as $p) {
				if (strncmp($p, 'site', 4)) {
					continue;
				}
				$id = substr($p, 4);
				if (ctype_digit((string)$id)) {
					return (int)$id;
				}
			}

			return null;
		}

		private function _sync_sql_databases($which)
		{
			$this->log("syncing $which databases...");
			$api = $this->_api;
			$afi = $this->_afi;
			$prefix = $this->_cfg['mysql']['dbaseprefix'];
			foreach ($this->_get_sql_databases($which, 'local') as $db) {
				$dborig = $db;

				$dbexists = $which . '_database_exists';
				if (0 === strpos($dborig, $prefix)) {
					if ($api->$dbexists($db)) {
						$this->log("deleting $db");
						if (!$api->{$which . '_delete_database'}($db)) {
							warn("cannot delete database `$db'");
						}
					}
					$this->log("creating `$db'");
					if (!$api->{$which . '_create_database'}($db)) {
						error("cannot create database `$db'");
						continue;
					}
				} else {
					// db was created before the introduction of prefixes
					$this->log("NOTICE: adding non-standard DB $db");
					$this->_remote_cmd('/usr/local/sbin/add' . $which . 'db.sh %s %s',
						$this->_cfg['siteinfo']['domain'],
						$db
					);

				}

				$backup = $afi->{$which . '_get_backup_config'}($dborig);
				// db backup is not set
				if (is_array($backup)) {
					$ext = $backup['extension'];
					$span = $backup['span'];
					$hold = $backup['hold'];
					$email = $backup['email'];
					$this->log("adding backup task for $db");
					if (!$api->{$which . '_add_backup'}($dborig, $ext, $span, $hold, $email)
					) {
						$this->log("WARN: failed to create backup task for `$db'");

					}
				}
			}
		}

		private function _get_sql_databases($which, $location)
		{
			$dbs = $this->_sync($location, $which . '_list_databases');
			$dbt = array();
			// @BUG old dbs may still be listed in privilege table
			foreach ($dbs as $db) {
				if ($location === 'local') {
					if (!$this->_sync('local', $which . '_database_exists', $db)) {
						// @todo error?
						dlog("WARN: skipping invalid `%s' database `%s', reported in grant but not found in filesystem",
							$which,
							$db
						);
						continue;
					}
				}
				$dbt[] = $db;
			}

			return $dbt;
		}

		private function _sync_sql_users($which)
		{
			if ($which == 'mysql') {
				return $this->_sync_mysql_users();
			} else {
				return $this->_sync_pgsql_users();
			}

		}

		private function _sync_mysql_users()
		{
			$dbs = $this->_get_sql_databases('mysql', 'local');
			$local = $this->_sync('local', 'mysql_list_users');
			$prefix = $this->_cfg['mysql']['dbaseprefix'];
			foreach ($local as $user => $huser) {
				$origuser = $user;
				if (!strncmp($user, $prefix, strlen($prefix))) {
					// workaround in situation where user is <prefix><prefix>name
					// if we strip 1 prefix from the mysql user, sql_mysql_user_exists()
					// barfs and returns false even if user <prefix><prefix>name exists
					$compounded = $prefix . $prefix;
					if (strncmp($user, $compounded, strlen($compounded))) {
						$user = substr($user, strlen($prefix));
					}
				}
				foreach ($huser as $host => $info) {
					$this->log('adding ' . $user . '@' . $host);
					if ($user !== $this->_cfg['mysql']['dbaseadmin']) {
						// add prefix as workaround if user contains db prefix in username
						//$user = $this->_cfg['mysql']['dbaseprefix'] . $user;
						// pullback!!!
						if ($this->_sync('remote', 'mysql_user_exists', $user, $host)) {
							$this->_sync('remote', 'mysql_delete_user', $user, $host, true);
						}
						$this->_sync('remote', 'mysql_add_user', $user, $host, \Opcenter\Auth\Password::generate());

					} else if (!$this->_sync('remote', 'mysql_user_exists', $user, $host)) {
						$this->_sync('remote', 'mysql_add_user', $user, $host, \Opcenter\Auth\Password::generate());
					}
					if (!$info['password']) {
						$this->log('no database pass set for user %s@%s, generating one',
							$user,
							$host);
						// some older clients won't have this field set, regrettably
						for ($i = 0; $i < 16; $i++) {
							$info['password'] .= chr(mt_rand(48, 122));
						}
					} else {
						if (ctype_xdigit($info['password']) && \strlen($info['password']) == 16) {
							// old style password, see if we can retrieve the password set for the user
							// otherwise it will need to be reset
							$newpass = $this->_updateMySQLPasswordFormat($user, $host, $info['password']);
							if ($newpass) {
								$info['password'] = $newpass;
								info("found password for `%s'@`%s' in client my.cnf, updating password to new hash format",
									$user, $host);
							} else if (version_compare($this->_destPlatformVersion, 6, '<')) {
								warn("mysql user `%s'@`%s' uses old style password, which are no longer " .
									'supported on newer platforms. Reset password through Databases > MySQL Manager',
									$user, $host);
								continue;
							} else {
								warn("mysql user `%s'@`%s' uses an old style password, which is incompatible with mysql. Password " .
									'must be updated via Databases > MySQL Manager before mysql will function for this user',
									$user, $host);
							}
						}
					}

					$opts = array(
						'password'             => $info['password'],
						'max_user_connections' => min(10, $info['max_user_connections']),
						'max_updates'          => $info['max_updates'],
						'max_questions'        => $info['max_questions'],
						'use_ssl'              => (bool)$info['ssl_type'],
						'cipher_type'          => $info['ssl_type'],
						'ssl_ciper'            => $info['ssl_cipher'],
						'x509_issuer'          => $info['x509_issuer'],
						'x509_subject'         => $info['x509_subject'],
					);
					$this->_sync(
						'remote',
						'mysql_edit_user',
						$user,
						$host,
						$opts
					);
					foreach ($dbs as $db) {
						$priv = $this->_sync('local', 'mysql_get_privileges', $user, $host, $db);
						// user lacks privileges on db
						if (array_sum($priv) < 1) {
							continue;
						}
						$this->log("adding mysql grant for `%s'@`%s' on `%s'", $user, $host, $db);
						if (!$this->_sync('remote', 'mysql_set_privileges', $user, $host, $db, $priv)) {
							warn("failed to sync grant on `%s'@`%s' on `%s'", $user, $host, $db);
						}
					}
				}
			}

			return true;
		}

		private function _updateMySQLPasswordFormat($user, $host, $cpasswd)
		{
			if ($host !== 'localhost') {
				return null;
			}

			$confuser = $this->mysql_get_option('user');
			if (!$confuser) {
				return null;
			}

			$passwd = $this->mysql_get_option('password');
			if (!$passwd || $confuser !== $user) {
				return null;
			}

			return $passwd;
		}

		private function _sync_pgsql_users()
		{
			$this->log('syncing postgresql users...');
			$local = $this->_sync('local', 'pgsql_list_users');
			$prefix = $this->_cfg['mysql']['dbaseprefix'];
			foreach ($local as $user => $info) {
				if (substr($user, 0, strlen($prefix)) == $prefix) {
					$user = substr($user, strlen($prefix));
				}
				$this->log('adding ' . $user);
				if ($user != $this->_cfg['mysql']['dbaseadmin']) {
					if ($this->_sync('remote', 'pgsql_user_exists', $user)) {
						$this->_sync('remote', 'pgsql_delete_user', $user, true);
					}
					$this->_sync('remote', 'pgsql_add_user', $user, \Opcenter\Auth\Password::generate());

				} else if (!$this->_sync('remote', 'pgsql_user_exists', $user)) {
					$this->_sync('remote', 'pgsql_add_user', $user, \Opcenter\Auth\Password::generate());
				}
				if (!$this->_sync(
					'remote',
					'pgsql_edit_user',
					$user,
					$info['password'],
					$info['max_connections']
				)
				) {
					warn('pgsql user update failed for `' . $user . "'");
				}
			}
		}

		private function _sync_sql_schema($which)
		{
			foreach ($this->_get_sql_databases($which, 'local') as $db) {
				$this->log("syncing $which db `$db'");
				if (!$this->{'_sync_' . $which . '_schema'}($db)) {
					$this->log("FAIL: $which schema transfer for `$db'");
					// inconsistent state, don't chance it further
					throw new Exception("failed to transfer $db!");
				}
			}
		}

		private function _sync_addon_domains()
		{
			if (!$this->_cfg['aliases']['enabled']) {
				return true;
			}
			if ($this->_sync('local', 'aliases_changes_pending')) {
				$this->log('WARN: unsynchronized changes on account, forcing changes');
				if (!$this->_sync('local', 'aliases_synchronize_changes')) {
					throw new Exception('failed to synchronize changes');
				}
			}
			$this->log('syncing addon domains...');
			$domains = $this->_afi->aliases_list_shared_domains();
			// previously attached addon domains that are not explicitly defined in <info path>/domain_map
			$aliases = array_diff($this->_afi->aliases_list_aliases(), array_keys($domains));
			$aliases = array_fill_keys($aliases, '/var/www/html');
			if (count($aliases) > 0) {
				$this->log('WARN: unaffiliated aliased domains found (' . count($aliases) . ')');
			}
			foreach ($domains as $domain => $path) {
				$aliases[$domain] = $path;
			}
			$basepath = $this->_sync('remote', 'common_get_base_path');
			$remotepath = $this->_get_info_path_from_base($basepath);
			$this->_remote_cmd('touch ' . $remotepath);
			$this->_sync('remote', 'aliases_synchronize_changes');
			foreach ($aliases as $alias => $path) {
				if ($alias == $this->_cfg['siteinfo']['domain']) {
					continue;
				}
				if ($this->_sync('remote', 'aliases_domain_exists', $alias)) {
					$this->_sync('remote', 'aliases_remove_domain', $alias);
				}
				if ($this->_sync('remote', 'email_transport_exists', $alias)) {
					$this->_sync('remote', 'email_remove_virtual_transport', $alias, true);
				}
			}

			foreach ($aliases as $alias => $path) {
				$this->log('Adding domain ' . $alias . ' -> ' . $path);
				$this->_sync('remote', 'aliases_add_domain', $alias, $path);
				$index = $path . '/index.html';
				if ($this->_sync('remote', 'file_exists', $index)) {
					$this->_sync('remote', 'file_delete', $index);
				}
			}

			$this->_sync('remote', 'aliases_synchronize_changes');
		}

		private function _sync_subdomains()
		{
			$this->log('syncing subdomains');
			$local = $this->_sync('local', 'web_list_subdomains');
			$remote = $this->_sync('remote', 'web_list_subdomains');
			$create = array_diff_key($local, $remote);
			foreach ($create as $subdomain => $path) {
				// tidy up new account:
				// skip subdomains that are no longer valid
				if ($path) {
					if ($this->_sync('remote', 'web_subdomain_exists', $subdomain)) {
						$this->_sync('remote', 'web_remove_subdomain', $subdomain);
					}
					$this->_sync('remote', 'web_add_subdomain', $subdomain, $path);
					$index = $path . '/index.html';
					if ($this->_sync('remote', 'file_exists', $index)) {
						$this->_sync('remote', 'file_delete', $index);
					}
				}
			}
		}

		private function _sync_email_domains()
		{
			$this->log('syncing transports...');
			$local = $this->_sync('local', 'email_list_virtual_transports');
			$remote = $this->_sync('remote', 'email_list_virtual_transports');
			$add = array_diff($local, $remote);
			$del = array_diff($remote, $local);
			foreach ($add as $domain) {
				$host = $this->_sync('local', 'web_split_host', $domain);
				if ($host == false) {
					$host = array('domain' => $domain, 'subdomain' => '');
				}
				// @BUG
				// deleting a shared domain should
				// disassociate the domain from Mailbox Transports

				// a subdomain could have e-mail transports
				if (!$this->_sync('remote', 'aliases_domain_exists', $host['domain'])) {
					$this->log("WARN: domain `%s' cannot have transports", $host['domain']);
					continue;
				}
				$this->_sync('remote', 'email_add_virtual_transport', $host['domain'], $host['subdomain']);
			}

			foreach ($del as $domain) {
				$this->_sync('remote', 'email_remove_virtual_transport', $domain, true);
			}

			return true;
		}

		private function _sync_mailing_lists()
		{
			if (!$this->_sync('local', 'majordomo_enabled')) {
				return true;
			}
			$this->log('syncing mailing lists...');
			$local = $this->_get_mailing_lists('local');
			foreach ($local as $list) {
				if ($this->_sync('remote', 'majordomo_mailing_list_exists', $list)) {
					continue;
				}

				$domain = $this->_sync('local', 'majordomo_get_domain_from_list_name', $list);
				if (!$this->_sync('local', 'email_transport_exists', $domain)) {
					dlog("NOTICE: domain `%s' not configured to handle mail. Skipping mailing list sync `%s'",
						$domain, $list);
					continue;
				}

				/*
				 * this does not matter - configuration is synced during
				 * file xfer overwriting these temporary values
				 * we just need the aliases to be created
				 */
				$email = 'foo@bar.com';
				$pass = \Opcenter\Auth\Password::generate(16);
				$domain = $this->_afi->majordomo_get_domain_from_list_name($list);
				/*
				 * Another bugfix: client has mailing list named after admin user
				 * Oooohhh jesus!
				 */
				if ($this->_sync('remote', 'email_address_exists', $list, $domain)) {
					$this->_sync('remote', 'email_delete_mailbox', $list, $domain);
				}
				$this->_sync(
					'remote',
					'majordomo_create_mailing_list',
					$list,
					$pass,
					$email,
					$domain
				);
			}
		}

		private function _get_mailing_lists($which)
		{
			return $this->_sync($which, 'majordomo_list_mailing_lists');
		}

		private function buildFileList(): array
		{
			$files = $this->_xferfiles;
			if ($this->_sync('local', 'majordomo_enabled')) {
				$files[] = '/var/lib/majordomo';
			}

			if ($this->_sync('local', 'email_enabled')) {
				if (version_compare($this->_get_src_platform(), '8', '>=') ) {
					if ($this->_sync('local', 'spamfilter_get_provider') === 'spamassassin') {
						$files[] = '/etc/mail/spamassassin/local.cf';
					}
				} else {
					$files[] = '/etc/mail/spamassassin/local.cf';
				}
			}

			if ($this->_sync('local', 'ftp_enabled')) {
				$files[] = '/etc/vsftpd';
			}

			return $files;
		}

		private function _sync_files(array $files, $localbase = null, $remotebase = null)
		{
			$this->log('syncing files...');
			if ($this->_stage < 2) {
				// placeholder index
				$index = self::PLACEHOLDER_FILE;
				if ($this->_sync('remote', 'file_exists', $index)) {
					$this->_sync('remote', 'file_delete', $index);
				}
			}
			if (is_null($localbase)) {
				$localbase = rtrim($this->_sync('local', 'common_get_base_path'), '/');
			}
			// shadow layer faster
			$localbase = preg_replace('!/fst/!', '/shadow/', $localbase, 1);
			if (is_null($remotebase)) {
				$remotebase = rtrim($this->_sync('remote', 'common_get_base_path'), '/');
			}
			$localfiles = array_map(
				static function ($file) use ($localbase) {
					return $localbase . $file;
				}, $files);
			$remotefiles = array_map(
				static function ($file) use ($remotebase) {
					return $remotebase . $file;
				}, $files);

			$xtraflags = '';
			if (version_compare($this->_get_src_platform(), '4.5', '>=')) {
				$xtraflags = '-ignore_readdir_race -nowarn';
			}
			$errors = Error_Reporter::flush_buffer();
			// include /etc/httpd/conf unconditionally, mostly a fix for legacy servers
			// @XXX ANY PATH ADDITIONS MUST be within the parentheses
			$ret = $this->_cmd('find %s %s -xdev ' .
				'\\( -not -group root \\( \\( -path \'%s\' \\) \\( -type f -o -type l -o -type d -a -empty \\) \\) \\) ' .
				'-fprintf %s/filelist.txt %s || true',
				$localbase,
				$xtraflags,
				implode('\' -o -path \'', array_map(
					static function ($file) {
						return $file . '/*\' -o -path \'' . $file;
					},
					$localfiles)
				),
				$localbase,
				'"/%P\0"'
			);

			$ret['success'] = $this->handleFindNoSuchDirectoryErrors($errors);
			// we deal with plenty of paths that may or may not exist on the server, don't panic


			if (!$ret['success']) {
				$this->log('ERROR: finding files: ' . $ret['stderr']);

				return false;
			}

			$ret = $this->_cmd('rsync --stats -aHWxzA%s --delete --delete-excluded --usermap=%s --groupmap=%s --numeric-ids ' .
				'--files-from=%s/filelist.txt ' .
				'-0 %s root@%s:%s',
				SELINUX ? 'X' : '',
				implode(',', $this->buildUsermap()),
				implode(',', $this->buildGroupmap()),
				$localbase,
				$localbase,
				$this->getTargetServer(),
				$remotebase
			);

			// missing, ephemeral files can reflect in filelist.txt but vanish at time of sync
			// let's assume migration goes forward without a hitch
			if (!$ret['success']) {
				$this->log('WARN: xfer files: ' . coalesce($ret['error'], $ret['output']));
			} else {
				$this->log('filesystem xfer finished: ' . $ret['output']);
			}

			// @TODO file transfers for databases retain wrong permissions
			if (false && file_exists($localbase . '/filelist.txt')) {
				unlink($localbase . '/filelist.txt');
			}

			$this->translate($remotefiles);
		}

		private function buildGroupmap(): array
		{
			$oldpwd = $this->_sync('local', 'user_getpwnam', $this->_sync('local', 'common_whoami'));
			$newpwd = $this->_sync('remote', 'user_getpwnam', $this->_sync('remote', 'common_whoami'));
			$gids = [
				$oldpwd['gid'] . ':' . $newpwd['gid']
			];

			$localtc = $this->_sync('local', 'tomcat_system_user');
			$pwd = posix_getpwnam($localtc);
			$javauid = $pwd['uid'] ?? null;
			$remotetc = $this->_sync('remote', 'tomcat_system_user');
			if ($javauid && $remotetc) {
				// tomcat not installed on server
				$gids[] = $javauid . ':' . $remotetc;
			}

			return $gids;
		}

		private function buildUsermap(): array
		{
			$remote = $this->_sync('remote', 'user_get_users');
			$local = $this->_sync('local', 'user_get_users');
			$uids = [];
			$adminUID = $this->_sync('remote', 'user_get_uid_from_username', $this->_sync('remote', 'common_whoami'));
			foreach (array_intersect_key($local, $remote) as $same => $_) {
				$uids[] = $local[$same]['uid'] . ':' . $remote[$same]['uid'];
			}
			foreach (array_diff_key($local, $remote) as $missing => $_) {
				// doesn't exist on receiving side
				warn("User %(user)s does not exist on receiving side - defaulting to %(uid)d",
					['user' => $missing, 'uid' => $adminUID]);
				$uids[] = $local[$missing]['uid'] . ':' . $adminUID;
			}

			$localtc = $this->_sync('local', 'tomcat_system_user');
			$pwd = posix_getpwnam($localtc);
			$javauid = $pwd['uid'] ?? null;
			$remotetc = $this->_sync('remote', 'tomcat_system_user');
			if ($javauid && $remotetc) {
				// tomcat not installed on server
				$uids[] = $javauid . ':' . $remotetc;
			}
			return $uids;
		}

		private function handleFindNoSuchDirectoryErrors(array $buffer)
		{
			$newerrors = Error_Reporter::flush_buffer();
			$tmp = array();
			foreach ($newerrors as $e) {
				if (false !== strpos($e['message'], 'No such file or directory')) {
					continue;
				}
				$tmp[] = $e;
			}
			$buffer = array_merge($buffer, $tmp);
			if (!$tmp) {
				return true;
			}
			Error_Reporter::set_buffer($buffer);

			return false;
		}

		private function translate($files)
		{
			$localpath = $this->_sync('local', 'common_get_base_path');
			$remotepath = $this->_sync('remote', 'common_get_base_path');
			$errors = Error_Reporter::flush_buffer();
			$ret = $this->_remote_cmd('find %s -noleaf -type f -exec grep -I -m1 -q %s {} \; -fprint %s/changed.log -print0 | xargs -0 ' .
				'perl -n -p -i -e \'use re "eval";\' ' .
				'-e \'$oldsite="%s"; $newsite="%s";\' ' .
				'-e \'$len = length($newsite)-length($oldsite) ; ' .
				's!s:(\d+)(?{$l=$^N+$len}):"$oldsite!s:$l:"$newsite!g ; ' .
				's!$oldsite!$newsite!g\'',
				join(' ', $files), $localpath, $remotepath, $localpath, $remotepath
			);
			$this->handleFindNoSuchDirectoryErrors($errors);
			if (!$ret['success']) {
				$this->log('ERROR: translate path failed: ' . $ret['stderr']);

				return false;
			}

			return true;
		}

		private function _sync_mailboxes()
		{
			$this->log('syncing mailboxes');
			$local = $this->_sync('local', 'email_list_mailboxes');
			$remote = $this->_sync('remote', 'email_list_mailboxes');
			$uids = array();
			$transportcache = array();
			foreach ($remote as $mailbox) {
				$user = $mailbox['user'];
				$domain = $mailbox['domain'];
				if ($user === 'majordomo' && $this->_sync('local', 'majordomo_enabled') &&
					$this->_sync('local', 'majordomo_list_mailing_lists'))
				{
					continue;
				}
				// this really should be one nice function...
				if ($mailbox['type'] == Email_Module::MAILBOX_FORWARD) {
					$this->_sync('remote', 'email_remove_alias', $user, $domain);
				} else {
					$this->_sync('remote', 'email_remove_mailbox', $user, $domain);
				}
			}

			foreach ($local as $mailbox) {
				$emailuser = $mailbox['user'];
				$domain = $mailbox['domain'];
				$destination = $mailbox['destination'];
				$enabled = $mailbox['enabled'];

				if ($emailuser == '' && $mailbox['type'] == Email_Module::MAILBOX_FORWARD) {
					$tmp = substr($destination, 0, strpos($destination, '@'));
					if (!$this->_afi->user_exists($tmp)) {
						$tmp = $this->common_get_admin_username();
					}
					warn("converted forwarded catch-all on %s to local catch-all for `%s'",
						$domain, $tmp);
					$mailbox['type'] = Email_Module::MAILBOX_USER;
					$mailbox['uid'] = $this->user_get_uid_from_username($tmp);
					$destination = null;
				}

				if (!isset($transportcache[$domain])) {
					$transportcache[$domain] = $this->_sync('remote', 'email_transport_exists', $domain);
				}
				// @BUG domains purged from Addon Domains may still linger
				// as a transport, which triggers an error, check and skip
				if (!$transportcache[$domain]) {
					continue;
				}
				if ($emailuser === 'majordomo' && $this->_sync('local', 'majordomo_enabled') &&
					$this->_sync('local', 'majordomo_list_mailing_lists'))
				{
					continue;
				}
				if ($mailbox['type'] == Email_Module::MAILBOX_FORWARD) {
					if (!$this->_sync('remote', 'email_add_alias', $emailuser, $domain, $destination)) {
						$this->log('mailbox sync FAIL: %s@%s -> %s',
							$emailuser, $domain, $destination);
					}
				} else if ($mailbox['type'] == Email_Module::MAILBOX_USER) {
					$olduid = $mailbox['uid'];
					if (!isset($uids[$olduid])) {
						$user = $this->_sync('local', 'user_get_username_from_uid', $olduid);
						if ($user == false) {
							continue;
						}
						$uids[$olduid] = array(
							'user'   => $user,
							'newuid' => $this->_sync('remote', 'user_get_uid_from_username', $user)
						);
					}
					if (!isset($uids[$olduid])) {
						error("uid translate fail for `$olduid'");
						continue;
					}
					$posix_user = $uids[$olduid]['user'];
					$newuid = $uids[$olduid]['newuid'];
					$custom = substr($mailbox['destination'], strrpos(rtrim($mailbox['destination'], '/'), '/.'));
					// mailbox is /home/USER/Mail ... no custom mailbox
					if ($custom == 'Mail' || $custom == 'home.' . $posix_user . '.Mail.') {
						$custom = '';
					}
					// @XXX until email_list_mailboxes is fixed
					if (!$this->_sync(
						'remote',
						'email_add_mailbox',
						$emailuser,
						$domain,
						$newuid,
						'')
					) {
						$this->log("mailbox sync FAIL: $emailuser@$domain -> " . $posix_user);
					}
				} else {
					$this->log('skipping unknown mailbox type `' . $mailbox['type'] . "' for $emailuser@$domain");
					continue;
				}

				if (!$enabled) {
					$this->_sync('remote', 'email_disable_address', $emailuser, $domain);
				}

			}
		}

		private function _sync_crons()
		{
			if (!$this->_cfg['ssh']['enabled'] || !$this->_sync('local', 'crontab_enabled')) {
				return;
			}
			$this->log('syncing crons');

			if ($this->_stage < 1) {
				$this->log('NOTICE: stage is 0, skipping cron sync');

				return;
			}

			if (!$this->_sync('remote', 'crontab_enabled')) {
				$this->_sync('remote', 'crontab_toggle_status', 1);
			}

			foreach ($this->_sync('local', 'crontab_list_users') as $user) {
				$remotejobs = $this->_sync('remote', 'crontab_list_cronjobs', $user);
				foreach ($this->_sync('local', 'crontab_list_cronjobs', $user) as $task) {

					if (array_key_exists('disabled', $task) && $task['disabled']) {
						unset($task['disabled']);
						$this->_sync('remote', 'crontab_add_raw', join(' ', $task));
						continue;
					}

					if (false !== strpos($task['minute'], '=')) {
						$this->log("Skipping ENV variable, `%s'", join(' ', $task));
						continue;
					}

					foreach ($remotejobs as $remote) {
						// task matches
						if ($remote['cmd'] == $task['cmd'] &&
							$remote['minute'] == $task['minute'] &&
							$remote['hour'] == $task['hour'] &&
							$remote['day_of_month'] == $task['day_of_month'] &&
							$remote['month'] == $task['month'] &&
							$remote['day_of_week'] == $task['day_of_week']
						) {
							continue 2;
						}
					}

					if (!$this->_sync('remote', 'crontab_add_cronjob',
						$task['minute'],
						$task['hour'],
						$task['day_of_month'],
						$task['month'],
						$task['day_of_week'],
						$task['cmd'],
						$user)
					) {
						$this->log('WARN: failed to add `' . $user . "' command `" . $task['cmd'] . "'");
					}
					if ($task['minute'] === '@reboot') {
						$this->log('INFO: starting task with @reboot time: %s', $task['cmd']);
						$this->_sync('remote', 'pman_run', $task['cmd'], null, ['user' => $user]);
					}
				}
			}
			if (!$this->_isDebug()) {
				$this->_sync('local', 'crontab_toggle_status', 0);
				$this->_sync('remote', 'crontab_toggle_status', -1);
			}
		}

		private function _sync_vmount()
		{
			if ($this->_stage < 1) {
				$this->log('skipping vmount sync');

				return true;
			}
			$services = array('procfs', 'fcgi');
			foreach ($services as $svc) {
				if (!$this->_sync('local', 'misc_is_mounted', $svc)) {
					continue;
				} else {
					if ($this->_sync('remote', 'misc_is_mounted', $svc)) {
						continue;
					}
				}
				if (!$this->_sync('remote', 'misc_mount_service', $svc)) {
					error("service `%s' mount failed", $svc);
				}
			}

			return true;

		}

		private function _fix_apache_ownership()
		{
			$this->log('fixing apache/tomcat ownership');
			$fstbase = str_replace('/fst', '/shadow', $this->_sync('remote', 'common_get_base_path'));
			$cmd = 'ZZfix_apache_ownership.sh';
			if (version_compare($this->_get_dest_platform(), '7.5', '>=')) {
				$cmd = '98update_web_ownership';
			}
			$this->_remote_cmd("/etc/cron.daily/$cmd " . $fstbase);
		}

		private function _sync_http_custom_config()
		{
			$trans = array();
			$localdir = $this->_sync('local', 'web_site_config_dir');
			$remotedir = $this->_sync('remote', 'web_site_config_dir');
			// copy base config in siteXX/
			$conf = array('');
			if ($this->_sync('local', 'ssl_enabled')) {
				// and siteXX.ssl/ where appropriate
				$conf[] = '.ssl';
			}
			foreach ($conf as $file) {
				$file = $localdir . $file . '/custom';
				if (!file_exists($file)) {
					continue;
				}
				$this->log('NOTICE: copying custom http config');
				$remotefile = str_replace($localdir, $remotedir, $file);
				$ret = $this->_cmd('rsync -aHWx %s root@%s:%s',
					$file,
					$this->getTargetServer(),
					$remotefile
				);
				if (!$ret['success']) {
					$this->log('ERROR: http config xfer failed: ' . $ret['stderr']);

					return false;
				}
				$trans[] = $remotefile;
			}
			if (count($trans) > 0) {
				$this->translate($trans);
			}

			return true;
		}

		/**
		 * Copy over DNS configuration
		 */
		private function _sync_dns()
		{
			$this->log('syncing dns...');
			$domains = array_merge(
				[$this->_sync('local', 'common_get_service_value', 'siteinfo', 'domain')],
				(array)$this->_sync('local', 'aliases_list_aliases')
			);
			foreach ($domains as $domain) {
				$oldex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_ERROR);
				try {
					$local = (array)$this->_sync('local', 'dns_get_zone_information', $domain);
				} catch (\apnscpException $e) {
					warn("Record transfer failed for `%s': %s", $domain, $e->getMessage());
					continue;
				} finally {
					\Error_Reporter::exception_upgrade($oldex);
				}
				$local = array_filter($local, static function ($rr) {
					return $rr !== 'SOA' && $rr !== 'NS';
				}, ARRAY_FILTER_USE_KEY);
				$remotepresent = $this->_sync('remote', 'dns_get_zone_information', $domain);

				foreach ($local as $rr => $records) {
					$toAdd = array_udiff($records, array_get($remotepresent, $rr, []), static function ($a, $b) {
						foreach (['subdomain', 'parameter'] as $comp) {
							if ($ret = ($a[$comp] <=> $b[$comp])) {
								return $ret;
							}
						}

						return 0;
					});

					foreach ($toAdd as $record) {
						$this->log("Creating record `%s' (rr: %s, parameter: %s)",
							$record['name'],
							$rr,
							$record['parameter']
						);
						$this->_sync('remote', 'dns_add_record', $domain, $record['subdomain'], $rr,
							$record['parameter'], $record['ttl']);
					}
				}
			}

		}

		private function _sync_ssl()
		{
			$this->log('syncing ssl...');
			$cert = array_get($this->_sync('local', 'ssl_get_certificates'), 0, []);
			if (!$cert) {
				return true;
			}
			$cert['crt'] = $this->_sync('local', 'ssl_get_certificate', $cert['crt']);
			if (!empty($cert['chain'])) {
				$cert['chain'] = $this->_sync('local', 'ssl_get_certificate', $cert['chain']);
			} else {
				$cert['chain'] = null;
			}
			$cert['key'] = $this->_sync('local', 'ssl_get_private_key', $cert['key']);

			return $this->_sync('remote', 'ssl_install', $cert['key'], $cert['crt'], $cert['chain']);
		}

		private function _sync_letsencrypt_ssl()
		{
			if (!$this->_sync('remote', 'letsencrypt_permitted')) {
				return true;
			}

			if (!$this->_sync('local', 'letsencrypt_exists')) {
				// no LE present
				return true;
			}

			/**
			 * @xxx don't like this approach, modules should expose an _export()
			 *      method to prepare files for export instead of hardcoding the
			 *      path into this transfer process
			 */
			$localsite = $this->_getSiteId('local');
			$remotesite = $this->_getSiteId('remote');

			// N.B. trailing slash must be in local path to transfer to new dest path
			// otherwise rsync will place <local path> in <remote path>/<local path>
			$ret = $this->_cmd('rsync -aHWx %s/ root@%s:%s',
				$this->letsencrypt_storage_path('site' . $localsite),
				$this->getTargetServer(),
				$this->letsencrypt_storage_path('site' . $remotesite)
			);
			if (!$ret) {
				warn("failed to sync Let's Encrypt certificate");
			}

			return $ret;

		}

		private function _sync_update_stage()
		{
			$this->log('updating dns stage data');
			$domains = $this->_get_domains($this->_cfg);
			foreach(['ip', 'ip6'] as $family) {
				$ip = $this->_sync('local', "dns_get_public_${family}");
				$newip = $this->_sync('remote', "dns_get_public_${family}");
				if (!$ip || !$newip) {
					if ($ip !== $newip) {
						warn('%(family)s differs on new platform - skipping', compact($family));
					}
					continue;
				}
				$ttl = $this->_stage === 1 ? $this->_sync('local', 'dns_min_ttl') : $this->_sync('remote', 'dns_get_default', 'ttl');
				foreach ($domains as $d) {
					// subdomain + domain added as an Addon Domain, actual
					// DNS resides in domain
					if (!$this->dns_zone_exists($d)) {
						continue;
					}
					$this->set_migration_stage($d, $this->_stage);
					if ($this->_stage === 1) {
						$this->alter_ip($d, $ip, $ip, $ttl);
					} else if ($this->_stage === 2) {
						$this->log('IP change: %s %s => %s', $d, $ip, $newip);
						$this->alter_ip($d, $ip, $newip, $ttl);
						$this->update_uuid($d);
					}
				}
			}
		}

		private function _get_domains($cfg)
		{
			$domains = array($cfg['siteinfo']['domain']);
			$domains = array_merge($domains, $cfg['aliases']['aliases']);

			return array_unique($domains);
		}

		public function set_migration_stage($domain, $stage)
		{
			if (!$this->dns_configured()) {
				return warn("cannot set next migration stage `%d' - dns is not configured", $stage);
			}
			if ($this->dns_record_exists($domain, self::MIGRATION_RECORD_NAME, 'txt') &&
				!$this->dns_remove_record($domain, self::MIGRATION_RECORD_NAME, 'txt')
			) {
				$this->log('INFO: %s.%s record reported present, but failed to remove',
					self::MIGRATION_RECORD_NAME, $domain);
			}
			if ($stage < 2) {
				// stage 2 is finished
				return $this->dns_add_record($domain,
					self::MIGRATION_RECORD_NAME,
					'txt',
					(string)CLI_Transfer_Util::createMigrationRecord(),
					$this->_sync('local', 'dns_min_ttl')
				);
			}

			return true;
		}

		private function alter_ip($domain, $oldip, $newip, $ttl = 86400)
		{
			if ($this->_isDebug()) {
				return true;
			}
			if (!$this->_sync('remote', 'dns_domain_uses_nameservers', $domain)) {
				$this->log("WARN: domain `%s' does not use %s - DNS adjustments will not be reflected",
					$domain,
					join(', ', $this->_sync('remote', 'dns_get_hosting_nameservers', $domain))
				);
				$this->flagNonNameserverDomain($domain);
			}
			$rr = \Opcenter\Net\IpCommon::classFromAddress($oldip) === 'ip6' ? 'AAAA' : 'A';
			$recs = $this->_sync('remote', 'dns_get_records_by_rr', $rr, $domain);
			$replace = array('ttl' => $ttl, 'parameter' => $newip);
			foreach ($recs as $rec) {
				if ($rec['parameter'] === $oldip) {
					$host = rtrim($rec['name'], '.');
					if (substr($host, -strlen($domain)) === $domain) {
						$host = rtrim(substr($rec['name'], 0, -strlen($domain) - 1), '.');
					}
					if (!$this->_sync('remote', 'dns_modify_record', $domain, $host, $rr, $oldip, $replace)) {
						error('error: DNS edit failed for ' . $host . ".$domain");
					}
				}
			}
			// update SPF
			$spf = $this->_sync('remote', 'dns_get_records_by_rr', 'TXT', $domain);
			$ipquote = str_replace('.', '\\.', $oldip);
			foreach ($spf as $rec) {
				$count = 0;
				$param = $rec['parameter'];
				$newparam = [
					'parameter' => preg_replace('/' . $ipquote . '/', $newip, $param, -1, $count)
				];
				// no match, no update, refrain from sending unnecessary query to nsupdate
				if ($count < 1) {
					continue;
				}
				if (!$this->_sync('remote', 'dns_modify_record', $rec['domain'], $rec['subdomain'], 'TXT', $param, $newparam)) {
					error('SPF record update failed for ' . $domain);
				}
			}
		}

		private function flagNonNameserverDomain($domain)
		{
			if (in_array($domain, $this->_nslessdomains, true)) {
				return true;
			}
			$this->_nslessdomains[] = $domain;
		}

		private function update_uuid(string $domain): bool
		{
			$olduuid = $this->_sync('local', 'dns_uuid');
			$newuuid = $this->_sync('remote', 'dns_uuid');
			if ($olduuid === $newuuid || !$olduuid) {
				return true;
			}
			$olduuidname = $this->_sync('local', 'dns_uuid_name');
			if (!$this->_sync('remote', 'dns_record_exists', $domain, $olduuidname, 'TXT')) {
				return true;
			}
			$newuuidname = $this->_sync('remote', 'dns_uuid_name');

			return $this->_sync('remote', 'dns_modify_record', $domain, $olduuidname, 'TXT', $olduuid,
				['parameter' => $newuuid, 'name' => $newuuidname]);
		}

		private function _delete_api_key()
		{
			if (!$this->_api_key_exists()) {
				return false;
			}
			$key = $this->_get_api_key();
			$this->_sync('remote', 'auth_delete_api_key', $key);
			$path = $this->_auth->domain_info_path('api.key');
			unlink($path);
			$this->log('deleting migration key');

			return true;
		}

		public function addStage($stage, $args = array())
		{
			$resolved = '_sync_' . $stage;
			if (!method_exists($this, $resolved) || !is_callable(array($this, $resolved))) {
				fatal("unknown stage `%s'", $stage);
			}
			$this->_syncStages[$stage] = $args;
		}

		public function force()
		{
			$this->_force = true;
		}

		public function flip()
		{
			//$this->_dest
		}

		public function setStage($stage)
		{
			if (!is_int($stage) || $stage > 2 || $stage < 0) {
				return error("invalid stage spec `%s'", $stage);
			}
			$this->_stage = $stage;
		}

		public function skipCreateDomain()
		{
			$this->_createAccount = false;
		}

		public function notify()
		{
			if (!$this->finished()) {
				return error('migration not finished');
			}
			\apnscpSession::invalidate_by_site_id($this->_getSiteId('local'));
			$email = (array)$this->_cfg['siteinfo']['email'];
			$domain = $this->_domain;
			$server = $this->getTargetServer();
			$oldserver = $this->getSourceServer();
			$proxy = null;
			$blade = \BladeLite::factory('views/email');
			$body = $blade->make('migration.completed', [
				'panelurl'  => 'https://' . \Auth_Redirect::makeHostFromServer($server),
				'newserver' => $server,
				'oldserver' => $oldserver,
				'domain'    => $domain
			])->render();

			$opts = array(
				'html_charset' => 'utf-8',
				'text_charset' => 'utf-8'
			);

			$from = Crm_Module::FROM_NAME . ' <' . Crm_Module::FROM_ADDRESS . '>';
			$headers = array(
				'Sender' => $from,
				'From'   => $from
			);
			$mime = new Mail_Mime($opts);

			$mime->setHTMLBody($body);
			$mime->setTXTBody(strip_tags($body));

			$mime = new Mail_Mime(
				array(
					'text_charset' => 'utf-8'
				)
			);
			$mime->setHTMLBody($body);
			$mime->setTXTBody(strip_tags($body));

			$mime->addAttachment(join("\r\n", $this->getBuffer()),
				'text/plain',
				'migration-log.txt',
				false
			);

			$template = $mime->get();
			$baseHeaders = $mime->headers($headers);

			$tmpHeaders = array();

			foreach ($baseHeaders as $k => $v) {
				$tmpHeaders[] = $k . ': ' . $v;
			}
			$builtHeaders = join("\r\n", $tmpHeaders);
			$email[] = self::STATUS_EMAIL;
			foreach ($email as $addr) {
				Mail::send(
					$addr,
					'Migration Completed - ' . $domain,
					$template,
					$builtHeaders,
					'-f ' . Crm_Module::FROM_ADDRESS . " -F'" . Crm_Module::FROM_NAME . "'"
				);
			}

			return true;
		}

		public function finished()
		{
			return $this->_stage === 2;
		}

		public function getSourceServer()
		{
			return SERVER_NAME;
		}

		/**
		 * Get message buffer
		 *
		 * @return array
		 */
		public function getBuffer()
		{
			return $this->_buffer;
		}

		public function getDomainsWithExternalNameservers()
		{
			return $this->_nslessdomains;
		}

		public function getRemoteIP()
		{
			$ip = $this->_sync('remote', 'dns_get_public_ip');

			return $ip;
		}

		public function setTargetServer($server)
		{
			$file = $this->_auth->domain_info_path('.server.migration');

			return file_put_contents($file, $server);
		}

		private function _parse_platform($platform)
		{
			return parse_platform_from_text($platform);
		}

		private function _sync_mysql_schema($db)
		{
			// perform a preflight check
			dlog("performing corruption check on mysql db `%s'", $db);
			if (!$this->_sync('local', 'mysql_repair_database', $db)) {
				return error("database scan failed on `%s'", $db);
			}

			// when no absolute path specified, exports to /tmp
			$tmpnam = '/tmp/' . $db . '.sql';
			$abspath = $this->_sync('local', 'common_get_base_path') . $tmpnam;
			$this->_sync('local', 'file_delete', $tmpnam);
			$ret = $this->_sync('local', 'mysql_export', $db, $db . '.sql');
			if (!$ret) {
				$this->log("ERROR: `$db' dump failed");
				error("`$db' dump failed");
				if (file_exists($abspath)) {
					unlink($abspath);
				}

				return false;
			}
			$remotepath = $this->_sync('remote', 'common_get_base_path');
			$ret = $this->_cmd('rsync -HW %s root@%s:%s/tmp/%s',
				$abspath,
				$this->getTargetServer(),
				$remotepath,
				basename($abspath)
			);
			unlink($abspath);
			if (!$ret['success']) {
				$this->log("ERROR: `$db' sql xfer failed: " . $ret['stderr']);
				error("`$db' sql xfer failed: " . $ret['stderr']);

				return false;
			}

			$convert = false;
			$dbsafe = $db;
			if (false !== strpos($db, '-') && $this->_get_dest_platform() < 4) {
				// mysql 5.1 permits a wider range of db names
				// prefix db with #mysql50#
				$dbsafe = '\'#mysql50#' . $dbsafe . '\'';
			}
			$files = array(
				$remotepath . '/tmp/' . basename($abspath)
			);
			$this->translate($files);
			$ret = $this->_remote_cmd('mysql -o %s < %s/tmp/%s',
				$dbsafe,
				$remotepath,
				basename($abspath)
			);
			$this->_remote_cmd('rm -f %s/tmp/%s',
				$remotepath,
				basename($abspath)
			);
			if (!$ret['success']) {
				$this->log("ERROR: `$db' import failed: " . $ret['stderr']);

				return false;
			}

			return true;

		}

		private function _sync_pgsql_schema($db)
		{
			$tmpnam = '/tmp/' . $db . '.sql';
			$file = $this->_sync('local', 'common_get_base_path') . $tmpnam;
			$this->_sync('local', 'file_delete', $tmpnam);

			$ret = $this->_cmd('pg_dump %s > %s',
				$db,
				$file
			);

			if (!$ret['success']) {
				$this->log("ERROR: `$db' dump failed: " . $ret['stderr']);
				if (file_exists($file)) {
					unlink($file);
				}

				return false;
			}
			$remotepath = $this->_sync('remote', 'common_get_base_path');
			$ret = $this->_cmd('rsync -HW %s root@%s:%s/tmp/%s',
				$file,
				$this->getTargetServer(),
				$remotepath,
				basename($file)
			);
			unlink($file);
			if (!$ret['success']) {
				$this->log("ERROR: `$db' sql xfer failed: " . $ret['stderr']);

				return false;
			}

			$files = array(
				$remotepath . '/tmp/' . basename($file)
			);
			$this->translate($files);
			$ret = $this->_sync('remote', 'pgsql_import', $db, '/tmp/' . basename($file));
			$this->_remote_cmd('rm -f %s/tmp/%s',
				$remotepath,
				$db
			);
			if (!$ret) {
				$this->log("ERROR: `$db' import failed: " . $ret['stderr']);

				return false;
			}

			return true;
		}
	}
