<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */

	trait DependencyInjectionTrait
	{
		private $container;

		public function injectContainer($dep)
		{
			$this->container = $dep;
		}

		public function getInjectedContainer()
		{
			return $this->container;
		}
	}