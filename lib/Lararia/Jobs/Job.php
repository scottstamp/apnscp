<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2017
	 */

	declare(strict_types=1);

	namespace Lararia\Jobs;

	use Illuminate\Bus\Queueable;
	use Illuminate\Contracts\Queue\ShouldQueue;
	use Illuminate\Foundation\Bus\PendingDispatch;
	use Illuminate\Queue\SerializesModels;
	use Lararia\Contracts\Job as JobContract;
	use Lararia\Jobs\Notifications\JobFinished;
	use Lararia\Jobs\Traits\RawManipulation;
	use Lararia\Jobs\Traits\RunAs;


	/**
	 * @property  return
	 */
	abstract class Job implements JobContract
	{
		use Queueable, RawManipulation;

		const ICON_ALERT = '⚠';
		const ICON_SUCCESS = '✅';
		const ICON_FAILED = '❌';
		const ICON_INFO = 'ℹ️';
		const DEFAULT_QUEUE_NAME = 'default';
		const MAILABLE_TEMPLATE = '';

		/**
		 * @var array
		 */
		protected $logBuffer;

		/**
		 * Errors generated upon creation
		 *
		 * @var array
		 */
		protected $creationLogBuffer = [];

		/**
		 * @var mixed return value of job
		 */
		protected $return;

		// @var null|string email
		protected $email;

		// @var int
		protected $jobId;

		// @var array job identifiers
		protected $tags = [];

		/**
		 * Create new job
		 *
		 * @param Job|string $abstract
		 * @param mixed      ...$args
		 * @return static
		 */
		public static function create($abstract, ...$args)
		{
			\Lararia\Bootstrapper::minstrap();
			if ($abstract instanceof static) {
				return $abstract;
			}
			$job = new $abstract(...$args);
			if (!$job instanceof static) {
				fatal("job abstract `%s' must implement `%s'", $abstract, static::class);
			}

			$traits = class_uses_recursive($job);
			if (isset($traits[RunAs::class], $traits[SerializesModels::class])) {
				// __serialize/__unserialize blocks __wakeup from calling in RunAs, which in turn introduces ghosting
				fatal("Job cannot implement both %s and %s", RunAs::class, SerializesModels::class);
			}

			return $job;
		}

		/**
		 * Delay a job
		 *
		 * @param \DateTime|int|null $timespec
		 * @return $this
		 */
		public function delayedDispatch($timespec)
		{
			return $this->dispatch()->delay($timespec);
		}

		/**
		 * Run additional jobs dependent upon first job
		 *
		 * @param Job[] $chain
		 * @return PendingDispatch
		 */
		public function dispatch(array $chain = array()): PendingDispatch
		{
			if ($chain && !$chain[0] instanceof self) {
				fatal('chained jobs must implement `%s\'', self::class);
			}
			$this->creationLogBuffer = \Error_Reporter::flush_buffer();
			$queue = $this->getQueueName();

			return (new PendingDispatch($this))->onQueue($queue)->chain($chain);
		}

		/**
		 * Get default queue name
		 *
		 * @return string
		 */
		public function getQueueName(): string
		{
			return static::DEFAULT_QUEUE_NAME;
		}

		public function handle()
		{
			\Error_Reporter::flush_buffer();
			$e = null;
			$oldhandler = pcntl_signal_get_handler(SIGTERM);
			pcntl_signal(SIGTERM, static function () {
				app('queue')->after(static function () {
					info('Shutdown queued. Killing %d', posix_getpid());
					exit(0);
				});
			});
			$oldex = \Error_Reporter::exception_upgrade(\Error_Reporter::E_FATAL);
			try {
				// @TODO switch jobs to try { ... } catch { ... } to workaround monkey patch
				$this->return = \Error_Reporter::record_backtrace(function () {
					return $this->fire();
				}, \Error_Reporter::E_ERROR);
			} catch (\apnscpException $e) {
				error('Job failed - fatal error: %s', $e->getMessage());
				$this->return = false;
			} catch (\Throwable $e) {
				// log
				\Error_Reporter::exception_upgrade(0);
				// prevent sloppy cleanup from terminating job runner
				error($e->getMessage());
			} finally {
				\Error_Reporter::exception_upgrade($oldex);
				$this->recordLog();
				pcntl_signal(SIGTERM, $oldhandler);
			}
			$this->handleErrors($e);

			// Store result just in case
			if (!$this instanceof ShouldQueue) {
				// sync driver, notify JobFinished handler
				app('events')->dispatch(JobFinished::class, $this);
			} else {
				// save as extra field
				$this->setField('return', \serialize($this->return));
			}

			return $this->return;
		}

		protected function handleErrors(\Throwable $head = null): void
		{
			if (!$this->hasErrors()) {
				return;
			}
			$log = $this->getLog(\Error_Reporter::E_ERROR | \Error_Reporter::E_EXCEPTION);
			$last = array_pop($log);
			//\assert($e instanceof \Throwable, 'Error generated outside exception');
			if (null === $last['bt']) {
				report("State = error, bt = null\n%s\nTotal: %s", var_export($last, true), var_export($log, true));
			}
			throw (new JobException($last['message'], $last['severity'], $head))->setApiTrace($last['bt']);
		}

		/**
		 * Get job return value
		 *
		 * @return mixed
		 */
		public function getReturn() {
			if (null !== $this->return) {
				return $this->return;
			}
			if ($json = $this->getField('return')) {
				return \Util_PHP::unserialize($json, true);
			}
			return null;
		}

		protected function recordLog(): bool
		{
			$this->logBuffer = array_merge($this->logBuffer ?? [], \Error_Reporter::flush_buffer());

			return (bool)$this->setField('erlog', json_encode($this->logBuffer));
		}

		public function hasErrors(): bool
		{
			return (bool)($this->getStatus() & (\Error_Reporter::E_EXCEPTION|\Error_Reporter::E_ERROR));
		}

		public function getStatus(): int
		{
			$severities = array_column($this->getLog(), 'severity');
			if (!$severities) {
				return \Error_Reporter::E_OK;
			}

			return max($severities);
		}

		public function getLog(int $level = 0xFFFF): array
		{
			if (null === $this->logBuffer) {
				$this->logBuffer = (array)json_decode((string)$this->getField('erlog'), true);
			}

			return array_filter($this->logBuffer, static function ($error) use ($level) {
				return $error['severity'] & $level;
			});
		}

		/**
		 * Job identifiers
		 *
		 * @return array
		 */
		public function tags(): array
		{
			return $this->tags;
		}

		/**
		 * Set job identifier
		 *
		 * @param array $tags
		 */
		public function setTags(array $tags = []): self
		{
			$this->tags = $tags;
			return $this;
		}

		/**
		 * Escape string from markdown formatting
		 *
		 * @param string $string
		 * @param bool   $anchored text is at beginning of line
		 * @return string
		 */
		public function escapeMarkdown(string $string, bool $anchored = false): string
		{
			$input = ['\\', '`', '*', '_', '{', '}', '[', ']', '(', ')', '!'];
			$output = ['\\\\', '\`', '\*', '\_', '\{', '\}', '\[', '\]', '\(', '\)', '\!'];
			if ($anchored) {
				$input = array_merge($input, ['-', '.', '!', '#', '+']);
				$output = array_merge($output, ['\-', '\.', '\!', '\#', '\+']);
			}

			return str_replace(
				$input,
				$output,
				$string
			);
		}

		public function setLog(array $log): bool
		{
			if (null !== $this->logBuffer && $log !== $this->logBuffer) {
				return error('Log already set - called by %s', \Error_Reporter::get_caller());
			}
			$this->logBuffer = array_merge($this->creationLogBuffer, $log);

			return true;
		}

		/**
		 * Get pretty-print icon
		 *
		 * @return string
		 */
		public function getStatusIcon(): string
		{
			if ($this->hasErrors()) {
				return static::ICON_FAILED;
			}

			return static::ICON_SUCCESS;
		}

		public function getView()
		{
			return '';
		}

		public function getMailableClass()
		{
			return static::MAILABLE_TEMPLATE;
		}

		public function getEmail(): ?string
		{
			return $this->email;
		}

		public function setEmail(string $address): self
		{
			$this->email = $address;

			return $this;
		}

		public function getAdminCopyEmail(): ?string
		{
			$admin = CRM_COPY_ADMIN ?: null;

			return $admin === $this->getEmail() ? null : $admin;
		}

		/**
		 * Set queued job ID
		 *
		 * @param int $id
		 */
		public function setId(int $id): self {
			if (isset($this->jobId)) {
				fatal("Job ID already set");
			}

			$this->jobId = $id;

			return $this;
		}
	}