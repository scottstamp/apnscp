<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	/** @var Template_Engine $templateClass */
	$templateClass->create_category(
		'Account',                                // canonical name
		true,                                     // assertions (may be an array)
		'',                                       // 16x16 icon for the category
		'account'                                 // internal name
	)->hide();

	$templateClass->create_category(
		'DNS',
		true,
		'',
		'dns'
	);

	$templateClass->create_category(
		'System',
		true,
		'',
		'services'
	);

	$templateClass->create_category(
		'Mail',
		\Opcenter\Mail\Services\Rspamd::present(),
		'/images/template/app-index/mail.png',
		'mail'
	);

	$templateClass->create_category(
		'Dev',
		true,
		'/images/template/app-icons/dev.png',
		'dev'
	);

	$templateClass->create_category(
		'Help', // canonical name
		true,               // assertions (may be an array)
		'/images/template/app-icons/help.png',  // image for the category
		'help'
	);

	$templateClass->create_category(
		'Miscellaneous',
		true,
		null,
		'misc'
	)->hide();

	$templateClass->create_link(
		'Dashboard',
		'/apps/dashboard',
		true,
		null,
		null
	);

	$templateClass->create_link(
		'Nexus',
		'/apps/nexus',
		true,
		null,
		null
	);

	$templateClass->create_link(
		'Configuration',
		'/apps/scopes',
		AUTH_ADMIN_API,
		null,
		null
	);


	$templateClass->create_link(
		'rspamd',
		'/apps/rspamd',
		\Opcenter\Mail\Services\Rspamd::present(),
		null,
		'mail'
	);


	$templateClass->create_link(
		'License',
		'/apps/license',
		true,
		null,
		null
	);

	$templateClass->create_link(
		'Change Information',
		'/apps/changeinfo',
		true,
		null,
		'account'
	);

	$templateClass->create_link(
		'API Keys',
		'/apps/soapkeys',
		true,
		null,
		'dev'
	);

	$templateClass->create_link(
		'Platform Docs',
		'https://docs.apiscp.com/',
		true,
		null,
		'help'
	);

	$templateClass->create_link(
		'User Knowledgebase',
		MISC_KB_BASE,
		(bool)MISC_KB_BASE,
		null,
		'help'
	);


	$templateClass->create_link(
		'API Docs',
		'https://api.apiscp.com/',
		true,
		null,
		'help'
	);

	$templateClass->create_link(
		'Sitemap',
		'/apps/sitemap',
		true,
		null,
		'misc'
	);

	$templateClass->create_link(
		'Changelog',
		'/apps/changelog',
		true,
		null,
		'misc'
	);

	// general error handler
	$templateClass->create_link(
		'Error',
		'/apps/error',
		true,
		null,
		'misc'
	);

	$templateClass->create_link(
		'DNS Manager',
		'/apps/dns',
		true,
		null,
		'dns'
	);

	$templateClass->create_link(
		'Job Runner',
		['/horizon', '/apps/horizon'],
		\Lararia\JobDaemon::isStandalone(),
		null,
		'dev'
	);

	$templateClass->create_link(
		'Demo App',
		'/apps/template',
		true,
		null,
		'dev'
	);

	#/*********************/
	#/*   Link Creation   */
	#/*********************/
	#/*
	#*  Site Maintenance
	#*/

	/***************** begin links *****************/
	/////////////////////////////////
	//// Category: Account Management
	/////////////////////////////////

	$templateClass->create_link(
		'Settings',
		'/apps/changeinfo',
		true,
		null,
		'account'
	)->hide();

	$templateClass->create_link(
		'Server Information',
		'/apps/stats',
		true,
		null,
		'services'
	);

	/////////////////////////////
	////////// Category: CRM
	/////////////////////////////

	$templateClass->create_link(
		'Trouble Tickets',
		'/apps/troubleticket',
		\cmd('crm_configured'),
		null,
		'crm'
	);

	/***************** end resources links **************/

	/* let's only draw draw these objects for the view_shortcuts page */
	/*
	*  Modules
	*/

	#    $templateClass->create_module(
	#        "Getting Started",
	#        "shit",
	#        true,
	#        "/images/headers/gettingstarted.jpg"
	#    );
	#
	/*
	*  Site Info Table Information
	*/
	/*if (!stristr(HTML_Kit::page_url(),"dashboard"))
		return;*/

	$function = apnscpFunctionInterceptor::init();

	$templateClass->create_info(
		'Load Average',
		implode(', ', $function->common_get_load()),
		true,
		null,
		1,
		'A load average of ' . NPROC . '  represents 100% CPU utilization across all processors.  ' .
		'An average of 1 represents ' . sprintf('%2d%%', 1 / NPROC * 100) . ' utilization',
		'Load Averages'
	);

	$rebootdays = Template_Engine::call('common_get_uptime', array(false));
	$reboot = time() - $rebootdays;
	$templateClass->create_info(
		'Last Reboot',
		date('F j, Y', $reboot) . ' (' . round($rebootdays / 86400) . ' days)',
		true,
		null
	);

	$ver = Template_Engine::call('sql_mysql_version', array(true));
	$templateClass->create_info(
		'MySQL Version (MariaDB)',
		sprintf('%s (%d.%d.%d)', $ver, ...str_split(MySQL::initialize()->server_version, 2)),
		true,
		null
	);

	$templateClass->create_info(
		'PgSQL Version',
		$function->sql_pgsql_version(true),
		true,
		null,
		1,
		'Abbreviated term for PostgreSQL.'
	);


	$templateClass->create_info(
		'Perl Version',
		$function->perl_version(),
		true
	);

	$templateClass->create_info(
		'PHP Version',
		$function->php_version(),
		true
	);